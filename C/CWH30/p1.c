//Fri May 19 08:18:15 AM IST 2023
//CWH 30
// Exercise 4: printing star Pattern 

// well there are some problems that require you to print stars
// like this pattern down below
// this is triangular star pattern
// *
// **
// ***
// ****
// *****
// ******
// this is reverse triangular star pattern
// ******
// *****
// ****
// ***
// **
// *
// ofcourse you can do these using just the cout function repeatedly but what if there are 
// hundreds and thousands of lines in the pattern?
// or what if the pattern is very complex
// you have to take input from user, whether they want normal star pattern
// or reversed star pattern
// 0 for normal star pattern
// 1 for reversed star pattern

// here is my solution
// I've also added the functionality to select the number of lines you want in the pattern

#include <stdio.h>

int main(){

    int isRev=0;
    printf("If you want to print the pattern in reverse, enter any non-zero value else enter 0:");
    scanf("%d",&isRev);
    printf("Enter the number of lines you want in the star pattern:");
    int nol=0;
    scanf("%d",&nol);
    if (isRev){
        for (int i=nol;i>0;i--){
            for (int j=1;j<=i;j++){
                printf("*");
            }
            printf("\n");
        }
    }
    else{
        for (int i=1;i<=nol;i++){
            for (int j=1;j<=i;j++){
                printf("*");
            }
            printf("\n");
        }
    }

    return 0;
}
// I didn't expect it to work first try, lol