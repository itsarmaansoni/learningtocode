//Wed May 10 08:36:28 AM IST 2023
//CWH 23
//Arrays in C


// What is an Array?
// it is a collection of data items of the same type
// items are stored in contigiuous memory locations
// it can also store collection of derived data types, such as pointers, structures, etc.
// A one dimensional array is like a list
// a two dimensional array is like a table
// C language places no limits on the number of dimensions in arrays
// some texts refer to one dimensional arrays as vectors and two dimensional arrays as matrices
// but when the dimension is not known the term "array" is used.
#include <stdio.h>
int main(){
	int arrlen=10;
	int marks[arrlen];
	// marks[0]=23;
	// marks[1]=13;
	// marks[2]=35;
	// marks[3]=76;
	for (int i = 0; i < arrlen; i++)
	{
		printf("Enter the value for element %d:",i);
		scanf("%d",&marks[i]);
	}
	for (int i = 0; i < arrlen; i++)
	{
		printf("the value for element %d is:%d\n",i,marks[i]);
	}
	// you can also initialize an array on the spot
	int arr2[5]={23,54,65,23,75};
	// even if you don't mention it's length it will work
	// example
	int arr3[]= {23,54,65,23,75};
	// c compiler will automatically assign it length 5
	int arr4[][3]= {{1,2,3},{0,2,5}};
	// note: declaration of ‘arr4’ as multidimensional 
	// array must have bounds for all dimensions except the first
	
	return 0;
}
