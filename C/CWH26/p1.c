//Mon May 15 08:31:38 AM IST 2023
//CWH 26
// Pointers in C
#include <stdio.h>

int main(){

    int a = 45;
    printf("The value of a is %d\n",a);
    int* ptra=&a;  // int* is the data type of int pointer
    // ptra is the pointer name, it's storing the address of a, which is extracted through
    // & operator 
    printf("The value of ptra is %p\n",ptra); // the value you will have inside ptra will
    // be hexa decimal, that's why we use %x format specifier 
    // the value is the address of you
    // if you dereference that address, you will get the value stored at a
    printf("The value stored in ptra is %d\n",*ptra);
    // this value will be the value stored in a, which is 45

    // also, we must not forget that pointers are themselves a variable and they also
    // have an address
    printf("This is the address of ptra pointer is %p\n",&ptra);
    //Initialization of null pointer
    int* nptr = NULL;
    printf("The value of nptr is %p\n",nptr);
    
    return 0;
}