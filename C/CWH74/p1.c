//Thu Jul 20 07:51:08 AM IST 2023
//CWH 74
//Exercise: Area of a circle using function pointers
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
float euclideanDistance(int x1,int y1,int x2,int y2){
    int xDiff=x2-x1;
    int yDiff=y2-y1;
    float xDiffsq=pow(xDiff,2);
    float yDiffsq=pow(yDiff,2);
    float dis=sqrt(xDiffsq+yDiffsq);
    return dis;
}
float manhattanDistance(int x1,int y1,int x2,int y2){
    int xDiff=x2-x1;
    int yDiff=y2-y1;
    float absxDiff=abs(xDiff);
    float absyDiff=abs(yDiff);
    float dis=absxDiff+absyDiff;
    return dis;
}
float areaOfCircle(int x1,int y1,int x2,int y2/*,insert a function pointer here, that is pointing towards anyone of the distance calculating algorithm*/,float (*fPtr)(int,int,int,int)){
    //assume the distance calculated is the radius of the circle

    return pow(fPtr(x1,y1,x2,y2),2)*3.1415926535;
}
int main(){
    //take the values of x1,y1,x2,y2 from user
    //HERE IS MY SOLUTION!
    int x1,y1,x2,y2;
    printf("Enter the value of x1:");
    scanf("%d",&x1);
    printf("Enter the value of y1:");
    scanf("%d",&y1);
    printf("Enter the value of x2:");
    scanf("%d",&x2);
    printf("Enter the value of y2:");
    scanf("%d",&y2);
    float (*fPtr)(int,int,int,int);
    fPtr=&manhattanDistance;
    printf("Area of Circle is %f",areaOfCircle(x1,y1,x2,y2,fPtr));
    return 0;
}