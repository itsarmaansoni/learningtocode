//Fri May 26 09:51:03 AM IST 2023
//CWH 35
//String functions

//quiz revision: strings aren't data types
// they are just char arrays that are terminated via null character
#include <stdio.h>
#include <string.h>
int main(){

    char s1[] = "Harry";
    char s2[] = "Ravi";
    // puts(strcat(s1,s2));
    printf("The length of s1 is %ld\n",strlen(s1));
    printf("The length of s2 is %ld\n",strlen(s2));
    // there is also strrev,strcpy,strcmp you can look these functions up
    // you don't need to memorize them
    return 0;
}