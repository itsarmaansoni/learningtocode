//Fri Jun 30 08:42:13 AM IST 2023
//CWH 60
//Predefined macros and other pre-processors directives

#include <stdio.h>
int main(){
    // demonstration of pre-defined macros
    printf("File name is %s\n",__FILE__);
    printf("today's date is %s\n",__DATE__);
    printf("current line number is %d\n",__LINE__);
    printf("ANSI:%d\n",__STDC__);
    return 0;
}
