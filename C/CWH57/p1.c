//Tue Jun 27 08:31:23 AM IST 2023
//CWH 57
// Exercise : Matrix Multiplication

// write a program that takes in input the dimensions of two arrays
// and tells whether their multiplication is possible,
// if the multiplication is possible, the values are taken from the user
// and the matrices are displayed 
// then the values are multiplied and the final matrix is shown


//Here is my solution!
#include <stdio.h>
#include <stdlib.h>
int main(){
    //dimension entry
    int r1,c1,r2,c2;
    printf("Enter the number of rows for matrix 1:");
    scanf("%d",&r1);
    printf("Enter the number of columns for matrix 1:");
    scanf("%d",&c1);
    printf("Enter the number of rows for matrix 2:");
    scanf("%d",&r2);
    printf("Enter the number of columns for matrix 2:");
    scanf("%d",&c2);
    // printf("%d %d %d %d\n",r1,c1,r2,c2);
    //checking if matrix multiplication is possible, if not, return with code 1
    if (c1!=r2){
        printf("Multiplication is not possible!\n");
        return 1;
    }
    // matrix 1 memory allocation
    int** m1;
    m1 = (int**)malloc(sizeof(int*)*r1);
    for (int i=0;i<r1;i++){
        m1[i]=(int*)malloc(sizeof(int)*c1);
    }
    // taking values from user for matrix 1
    for (int i=0;i<r1;i++){
        for (int j=0;j<c1;j++){
            printf("Enter the value for matrix 1 [%d][%d]:",i,j);
            scanf("%d",&m1[i][j]);
            // printf("%d\t",m1[i][j]);
        }
        // printf("\n");
    }
    printf("\n");
    //matrix 2 memory allocation
    int** m2;
    m2 = (int**)malloc(sizeof(int*)*r2);
    for (int i=0;i<r2;i++){
        m2[i]=(int*)malloc(sizeof(int)*c2);
    }
    // taking values from user for matrix 2
    for (int i=0;i<r2;i++){
        for (int j=0;j<c2;j++){
            // m2[i][j]=i+j;
            printf("Enter the value for matrix 2 [%d][%d]:",i,j);
            scanf("%d",&m2[i][j]);
            // printf("%d\t",m2[i][j]);
        }
        // printf("\n");
    }
    //printing matrix 1 in tabular form
    printf("\nMatrix 1:\n");
    for (int i=0;i<r1;i++){
        for (int j=0;j<c1;j++){
            // printf("Enter the value for matrix 1 [%d][%d]:",i,j);
            // scanf("%d",&m1[i][j]);
            printf("%d\t",m1[i][j]);
        }
        printf("\n");
    }
    //printing matrix 2 in tabular form
    printf("\nMatrix 2:\n");
    for (int i=0;i<r2;i++){
        for (int j=0;j<c2;j++){
            // m2[i][j]=i+j;
            // printf("Enter the value for matrix 2 [%d][%d]:",i,j);
            // scanf("%d",&m2[i][j]);
            printf("%d\t",m2[i][j]);
        }
        printf("\n");
    }
    //Product matrix memory allocation
    int** pm = (int**)malloc(sizeof(int*)*r1);
    for (int i=0;i<r1;i++){
        pm[i] = (int*)malloc(sizeof(int)*c2);
    }
    //Product algorithm and initialization of product matrix
    for (int r=0;r<r1;r++){
        for (int c=0;c<c2;c++){
            int sum = 0;
            for (int k=0;k<c1;k++){
                sum += m1[r][k] * m2[k][c];
            }
            pm[r][c]=sum;
        }
    }
    //Printing product matrix 
    printf("\nProduct Matrix:\n");
    for (int i=0;i<r1;i++){
        for (int j=0;j<c2;j++){
            printf("%d\t",pm[i][j]);
        }
        printf("\n");
    }
    return 0;
}