//Fri May  5 08:41:20 AM IST 2023
//CWH 20
//solutions of multiplication table program


// this is CWH's solution to the multiplication
// problem, it's pretty similar to my solution
// but it's here anyway

#include <stdio.h>
int main(){
	printf("Enter the number you want the multiplication table of :");
	int num;
	scanf("%d",&num);
	for (int i=1;i<=10;i++){
		printf("%d x %d = %d\n",num,i,(num*i));
	}
	return 0;
}
