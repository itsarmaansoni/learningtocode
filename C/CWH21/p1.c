//Mon May  8 08:07:18 AM IST 2023
//CWH 21
// Recursion in C programming language



// recursive functions are functions
// that call themselves and keep calling 
// themselves until a base case is met

// let's look at some code

#include <stdio.h>
int factorial(int num){
	if (num==1 || num==0)
	return 1;
	else
	return num*factorial(num-1);
}
int fibonacci(int term){
	if (term<=2)
	return 1;
	else 
	return fibonacci(term-1)+fibonacci(term-2);
}
int main(){
    int usernum;
	printf("Enter a number to find the factorial of : ");
	scanf("%d",&usernum);
	printf("The factorial of the given number is : %d\n",factorial(usernum));
	// using a factorial might not be the most efficient approach 
	// but it makes it easier to write logic for some problems
	//
	
	// here is fibonacci term finder using recursion
	int term;
    printf("Enter the term you want to find :");
    scanf("%d",&term);
    printf("The value at term %d is %d",term,fibonacci(term));
    return 0;
}

