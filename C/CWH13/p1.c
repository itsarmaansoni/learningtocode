// Do while loop in C
// 26 April 2023
// CWH 13


// do while syntax
// do {
// 	loop body
// } while (loop condition)

// actual example in code

#include <stdio.h>
int main(){
	int i=0;
	do {
		i=i+1;
		printf("%d\n",i);
	} while (i<=10);
	return 0;
}
// as you can see it doesn't matter what the loop condition is, the loop body will be executed atleast once


