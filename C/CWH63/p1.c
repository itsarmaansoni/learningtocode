//Wed Jul  5 11:01:29 AM IST 2023
//CWH 63
//Exercise: Check Palindrome in C
//


// question: we have to check if a number is palindrome or not
// a number is palindrome if when reversed is equal to the original number
// example 636, 5585855
// you have to write a isPalindrome() function which takes in the number as an integer argument
// and returns 1 or 0 for true and false respectively, true if the number is palindrome 
// and false if the number is not palindrome
// the main function is already written 
//
//Here is my solution!!
#include <stdio.h>
int isPalindrome(int number){
	int num = number;
	int newnum=0;
	while (num!=0){
		int d=num%10;
		newnum=d+newnum*10;
		num=num/10;
	}
	if (newnum==number){
		return 1;
	}

	return 0;
}
int main(){
	int number;
	printf("Enter a number:");
	scanf("%d", &number);
	if (isPalindrome(number)){
		printf("Number is Palindrome\n");
	}
	else
	{
		printf("Number is not Palindrome\n");
	}
	return 0;
}
