//Mon Jun  5 08:22:41 AM IST 2023
//CWH 41
//Exercise: HTML parser

#include <stdio.h>
#include <string.h>
/*
you have to write the parser function such that
it removes tags and trailing and leading spaces from the strings
so for input string "<h1> This is a heading </h1>" 
the output will be:
This is a heading
*/
// here is MY SOLUTION!
// my first instinct is to make a space remover function, which removes leading and trailing
// spaces
void space_remover(char str[]){
    int len = strlen(str);
    //removes trailing spaces
    for (int i=len-1;i>=0;i--){
        if (str[i]==' ')
        continue;
        else{
        str[i+1]='\0';
        break;
        }
    }
    len = strlen(str);
    //removes leading spaces
    int firstnonspace=0;
    for(int i=0;i<len;i++){
        if (str[i]==' ')
        continue;
        else{
            firstnonspace=i;
            break;
        }
    }
    char nstr[len-firstnonspace+1];
    for (int i=firstnonspace,j=0;i<=len;j++,i++){
        nstr[j]=str[i];
    }
    strcpy(str,nstr);
}
//9889 622 777 mr satish singh // Orient service center // call 10:00 AM


// I failed to solve this exercise,
// not that I can't solve it, it's just that 
// the rules were very vague, the question was very vague
// what if there are multiple tags?
// what if tags don't end?
// if I had to deal with every case, this program will be very complex,
// I will look at the solution now,
// well, I was able to make the space_remover function tho, which is nice
// YEAH, after watching the code with harry's video
// a learnt that a lot of things were assumed like
// there won't be multiple opening and closing tags, all tags will end
// only two tags will be present, they don't have to have the same content inside
// but still I accept my defeat, I was thinking about a way more complex solution,
// no wonder I wasn't able to implement it
// don't get me wrong, I would have implemented it if I spent more time on it,
// it's not that I didn't know how to solve this problem, it's just that
// the question was vague
// ONE MORE THING, the code just assumed that there won't be anything
// outside the tags
// it will parse everything as long as it's not inside the tag, 
// this problem assumed a lot of things, this makes me sad
void parser(char str[]){
    int len=strlen(str);

}
int main(){

    char str[] = "         <h1> This is a heading </h1>              ";
    space_remover(str);

    return 0;
}