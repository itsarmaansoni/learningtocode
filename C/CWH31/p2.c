// Mon May 22 09:36:43 AM IST 2023
// CWH 31
// solution of quick quiz
#include <stdio.h>

int changeValues(int* x, int* y){
    int sum = *x + *y;
    int sub = *x - *y;
    *x = sum;
    *y = sub;
}
int main(){
    int a=5;
    int b=4;
    printf("The value of a and b before changing are: %d and %d\n",a,b);
    changeValues(&a,&b);
    printf("The value of a and b after changing are: %d and %d\n",a,b);
    return 0;
}