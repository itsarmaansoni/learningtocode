//Mon May 22 08:56:35 AM IST 2023
//CWH 31
// Call by Value & Call by Reference in C 
#include <stdio.h>

void changeValue(int* x){
    *x=45;
}
int main(){

    int a=34,b=56;
    printf("The value of a is %d\n",a);
    changeValue(&a);
    printf("The value of a is %d\n",a);

    return 0;
}

// quick quiz
// make a function such that, it takes value of a and b
// and makes the value of a equal to a+b
// and makes the value of b equal to a-b 
// using call by reference, 
// my solution is in p2.c