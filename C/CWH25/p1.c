// Fri May 12 08:45:41 AM IST 2023
// CWH 25
//  Exercise on Recurssion
//  we have to basically figure out which one
//  recursive approach or iterative approach is more efficient for
//  calculating fibonacci elements
// Here is my SOLUTION!
#include <stdio.h>
// Fibonacci using recurssion
int fibo_rec(int term)
{
    if (term == 1 || term == 2)
        return (term - 1);
    else
        return fibo_rec(term - 1) + fibo_rec(term - 2);
}
// Fibonacci using iteration
int fibo_itr(int term)
{
    int a = 0;
    int b = 1;
    if (term == 1 || term == 2)
        return (term - 1);
    else
        for (int i=1;i<term;i++){
            int temp=b;
            b=a+b;
            a=temp;
        }
        return a;
}
int main()
{
    printf("Enter the term you want to calculate:");
    int term;
    scanf("%d",&term);
    printf("Answer using Iterative approach is:%d\n",fibo_itr(term));
    printf("Answer using recursive approach is:%d\n",fibo_rec(term));
    return 0;
}
/*
a=0
b=1


term=1
temp=1
a=1
b=1

term=2
temp=1
a=1
b=2

term=3
temp=2
a=2
b=3


term=4
temp=3
a=3
b=5
*/
// I don't even know how I was able to make out the logic for iterative approach lol


// RECURRSION IS APPRANTLY SLOWER when the term we want to calculate is over 40
// it really starts showing
// so yeah, this was my solution to the exercise