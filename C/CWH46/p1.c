//Mon Jun 12 08:34:58 AM IST 2023
//CWH 46
// CODE WITH HARRY's solution to TRAVEL AGENCY MANAGER EXERCISE

// the code wasn't posted on his site
// but his code was inferior because it didn't use loops to solve the problem
// I used loops

// here is my solution again

//Thu Jun  8 09:21:20 AM IST 2023
//CWH 44
// Exercise : Travel Agency Manager


//QUESTION
/*
You manage a travel agency and you want your n drivers to input their following details:
1. Name
2. Driving License No
3. Route 
4. Kms
Your program should be able to take n as input(or you can take n=3 for simplicity) and 
your drivers will start inputting their details one by one.

Your program should print details of the drivers in a beautiful fashion.
User structures.
*/
#include <stdio.h>

// here is my solution
typedef struct DriverInfo{
    char name[50];
    char license_no[50];
    char route[100];
    int kms;
} DInfo;
int main()
{
    DInfo dInfoarr[3];
    for (int i=0;i<3;i++){
        printf("\n ENTER THE DETAILS FOR DRIVER no. %d\n\n",i+1);
        printf("Enter the name:");
        scanf("%[^\n]%*c", dInfoarr[i].name);
        printf("Enter the license_no:");
        scanf("%[^\n]%*c", dInfoarr[i].license_no);
        printf("Enter the Route:");
        scanf("%[^\n]%*c", dInfoarr[i].route);
        printf("Enter the distance travelled:");
        scanf("%d",&dInfoarr[i].kms);
        scanf("%*c");
    }
    for (int i=0;i<3;i++){
        printf("\n Details for Driver %d \n",i+1);
        printf("Name:%s\n",dInfoarr[i].name);
        printf("License plate number:%s\n",dInfoarr[i].license_no);
        printf("Route:%s\n",dInfoarr[i].route);
        printf("Distance Travelled: %d\n",dInfoarr[i].kms);
    }
    return 0;
}


// The exercise was fairly simple, it's just that dealing with strings
// always causes trouble, I will take the time to learn strings properly