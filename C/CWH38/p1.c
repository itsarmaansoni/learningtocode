//Wed May 31 08:06:24 PM IST 2023
//CWH 38
//Typedef in C

// typedef is a keyword used to give alternative names to datatypes

// typedef <previous name> <alias name> 

#include <stdio.h>
struct Student{
    int id;
    int marks;
    char fav_char;
    char name[34];
} ;

typedef struct Employee
{
    int id;
    char[34] name;
    int salary;
} emp;

int main(){
    typedef unsigned long ul;
    ul l1,l2,l3;
    typedef struct Student st;
    st harry,anmol,armaan;
    emp rakesh,mukesh;
    
    return 0;
}