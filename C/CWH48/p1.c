//Wed Jun 14 08:26:46 AM IST 2023
//CWH 48
// Exercise 8: Employee Manager

// Dynamic Memory Allocation
    // ABC Pvt Ltd. manages employee records of other companies.
    // Employee Id can be of any length and it can contain any character
    // For 3 employees, you have to take 'length of employee id' as input in a length integer variable.
    // Then, you have to take employee id as an input and display it on screen. 
    // Store the employee id in a character array which is allocated dynamically.
    // You have to create only one character array dynamically
// EXAMPLE:
    // Employee 1:
    // Enter no of characters in your eId
    // 45
    // Dynamically allocate the character array.
    // take input from user 

    // Employee 2:
    // Enter no of characters in your eId
    // 4
    // Dynamically allocate the character array.
    // take input from user 

    // Employee 3:
    // Enter no of characters in your eId
    // 9
    // Dynamically allocate the character array.
    // take input from user 


// HERE is my solution
// I'll try to write as little code as I can

#include <stdio.h>
#include <stdlib.h>

int main(){
    printf("Enter the number of Employees:");
    int noe;
    scanf("%d",&noe);
    char * empId=(char*)malloc(10*sizeof(char));
    for (int i=1;i<=noe;i++){
        printf("Employee Details number %d:\n",i);
        printf("Enter the length of employeeId :");
        int empIdLen;
        scanf("%d",&empIdLen);
        getchar();
        empId = (char*)realloc(empId, empIdLen*sizeof(char));
        printf("Enter the Employee Id:");
        scanf("%[^\n]%*c",empId);
        printf("Employee Id is :%s\n",empId);
    }
    return 0;
}