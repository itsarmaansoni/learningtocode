// Switch case control statements in C
// 24 April, 2023
// CWH11
//
//
// switch case are an alternative to if else ladder statements, but it all comes down to what you want to use


// syntax or example: 
// int a = 2;
// switch (a){
// 	case 2:
// 	printf("Value is 2");
// 	break;
// 	case 3:
// 	printf("Value is 3");
// 	break;
// 	default:
// 	printf("unspecified case")
// 	break; 
// }

// switch expresssion can either be an int or a char
// case value must be an int or char as well
// case must come inside switch block
// break statement isn't a must, but if not used, it might lead to executing other blocks
// use break; as necessary

// flowchart for switch case statement is attached as an image


#include <stdio.h>
int main(){
	printf("Enter your age:");
	int age;
	scanf("%d", &age);
	switch (age){
		case 3:
		printf("Age is 3\n");
		break;
		case 13:
		printf("Age is 13\n");
		break;
		case 23:
		printf("Age is 23\n");
		break;
		default:
		printf("Age is not 3, 13, or 23\n");
		break; // there is no need to use break; in default, but it's just good practice 
	}
	// there can also be nested-switch, you get the point

	return 0;
}
