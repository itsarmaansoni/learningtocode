// for loops in C
// 28th April, 2023
// CWH15


// the for loop is another kind of loop in C
// it is used to transverse data structures like array and linked lists 
// it has a little different syntax than do while and while loop
// sytax 
// for (expression 1;expression 2; expression 3){
// 	loop body;
// }
// expression 1: initialization expression
// expression 2: loop condition
// expression 3: updation expression

#include <stdio.h>
int main(){
	for (int i=1;i<=10;i++){ // a for loop definition can have multiple initialization expressions
	// multiple loop conditions and multiple updation expressions, but only the last loop condition will be checked
		printf("%d\n",i);
	}
	return 0;
} 
