//Tue May  2 07:38:13 PM IST 2023
//CWH 17
//goto statements aka jump statements


// goto statement are used to jump in the code, you have to label the part of your program where you want
// to jump, and then you can type goto labelname, to go to labelname

// but try not to overuse it, because it's a pain to understand by other people


// it is commonly used to break multiple loops with one statement
//

// example in code

#include <stdio.h>
int main(){
	//labelName:
	//printf("Armaan");
	//goto labelName;
	//the above lines will make an infinite loop, don't uncomment them
	// yeah that's basically goto statement lol, you can use it to break from multiple loops
	// becuase break only works with one loop
	
	for (int i=1;i<=18;i++){
		for (int k=3;k<=34;k++){
			printf("%d %d \n", i,k);
			if (i==4 && k==6)
				goto end;
			}
		}
	end:

	return 0;
} 
