// Tue Jun 1size 08:49:08 AM IST 202size
// CWH 47
//  Functions for dynamic memory allocation
// malloc
// calloc
// realloc
// free

#include <stdio.h>
#include <stdlib.h>

int main()
{
    int *ptr;
    int size;
    // Use of malloc()
    printf("Enter the size of the malloc array:");
    scanf("%d", &size);
    ptr = (int *)malloc(size * sizeof(int));
    for (int i = 0; i < size; i++)
    {
        printf("Enter value at %d:", i);
        scanf("%d", (ptr + i));
    }
    for (int i = 0; i < size; i++)
    {
        printf("the value at %d:%d\n", i, *(ptr + i));
    }
    free(ptr);
    // Use of calloc
    printf("Enter the size of calloc array:");
    scanf("%d", &size);
    ptr = (int *)calloc(size, sizeof(int));
    // for (int i = 0; i < size; i++)
    // {
    //     printf("the value at %d:%d\n", i, *(ptr + i));
    // } // as you can see that it's initialized with zeroes
    for (int i = 0; i < size; i++)
    {
        printf("Enter value at %d:", i);
        scanf("%d", (ptr + i));
    }
    for (int i = 0; i < size; i++)
    {
        printf("the value at %d:%d\n", i, *(ptr + i));
    }
    int nsize;
    printf("Enter new size of array:");
    scanf("%d", &nsize);
    ptr = (int *)realloc(ptr, nsize * sizeof(int));
    for (int i = 0; i < nsize; i++)
    {
        printf("Enter value at %d:", i);
        scanf("%d", (ptr + i));
    }
    for (int i = 0; i < nsize; i++)
    {
        printf("the value at %d:%d\n", i, *(ptr + i));
    }
    free(ptr);
    return 0;
}