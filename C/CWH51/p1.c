// Mon Jun 19 08:47:02 AM IST 2023
// CWH 51
// Exercise 9:Coding Rock Paper and Scissors in C

// Create Rock, Paper & Scissors Game
//  Player 1: rock
//  Player 2 (computer): scissors -->player 1 gets 1 point

// rock vs scissors - rock wins
// paper vs scissors - scissors wins
// paper vs rock - paper wins
// Write a C program to allow user to play this game three times with computer. Log the scores of computer and the player. Display the name of the winner at the end
// Notes: You have to display name of the player during the game. Take users name as an input from the user.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int generateRandomNumber(int n, int R)
{
    // srand takes seed as an input and is defined inside stdlib.h
    //  time() is defined in time.h and gives the current time which is used as seed
    //  so that the psuedorandom number is generated via time
    //  I will make some changes in the current function, I will also take one more int
    //  input for seed, this will be the number of rounds played, this will add more randomness
    //  I will multiply the current time and the rounds played number and feed it to the generateRandomNumer fucntion
    srand(time(NULL) * R);
    return rand() % n;
}

int main()
{
    // Okay  this is my solution to the CODE WITH HARRY's ROCK PAPER SCISSORS EXERCISE

    // first I will make score variable for both bot and user
    int scoreBot, scoreUser;
    // initialize them with 0
    scoreBot = 0;            printf("tied!\n");

    scoreUser = 0;
    // taking username from user
    char *username;
    int usernamelen = 100;
    printf("Welcome to the GAME OF ROCK PAPER AND SCISSORS!\n");
    // printf("Enter your player name's length:");
    // scanf("%d",&usernamelen);
    // getchar();
    username = (char *)malloc(sizeof(char) * (usernamelen + 1));
    printf("Enter your player name:");
    scanf("%[^\n]%*c", username);
    printf("\n");
    printf("Player %s initialized!!!", username);
    printf("\n\n Starting ROCK PAPER SCISSORS!!!!!\n\n");
    int rounds = 1;
    printf("To choose Rock type:\t\tr\nTo choose Scissors type:\ts\nTo choose Paper type:\t\tp\n");
    int choiceBotInt;
    char choiceUser, choiceBot;
    while (rounds <= 3)
    {
        choiceBotInt = generateRandomNumber(3, rounds);
        // printf("%d\n",choiceBotInt);
        printf("\n%s's turn :", username);
        scanf("%c", &choiceUser);
        getchar();
        if (choiceBotInt == 0)
            choiceBot = 'p';
        else if (choiceBotInt == 1)
            choiceBot = 's';
        else
            choiceBot = 'r';
        printf("Computer:%c\n", choiceBot);
        int flag = 0;
        // 1= user won
        // 2= bot won
        // 0 = tie
        if (choiceBot == 'r')
        {
            if (choiceUser == 'p')
                flag = 1;
            else if (choiceUser == 's')
                flag = 2;
            else
                flag = 0;
        }
        else if (choiceBot == 'p')
        {
            if (choiceUser == 'r')
                flag = 2;
            else if (choiceUser == 's')
                flag = 1;
            else
                flag = 0;
        }
        else
        { // s
            if (choiceUser == 'r')
                flag = 1;
            else if (choiceUser == 'p')
                flag = 2;
            else
                flag = 0;
        }
        rounds++;
        if (flag == 1)
        {
            printf("%s WON!!!\n", username);
            scoreUser++;
        }
        else if (flag == 2)
        {
            printf("YOU LOST!! computer wins!\n");
            scoreBot++;
        }
        else
        {
            printf("Tied!\n");
            scoreUser++;
            scoreBot++;
        }
    }
    printf("\nFinal Score Chart\n");
    printf("Computer:%d\n",scoreBot);
    printf("%s:%d\n",username,scoreUser);
    if(scoreBot>scoreUser){
        printf("OVERALL COMPUTER WON!!! LOSER %s\n",username);
    }
    else if (scoreUser>scoreBot){
        printf("OVERALL %s WON!!! LOSER COMPUTER\n",username);
    }
    else{
        printf("OVERALL TIED!!!! JHANTUUUUU!\n");
    }
    return 0;
}
