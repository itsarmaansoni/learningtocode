//Thu May 25 08:14:16 AM IST 2023
//CWH 34
// Strings in C
#include <stdio.h>

void printStr(char str[]){
    int i=0;
    while (str[i]!='\0'){
        printf("%c",str[i]);
        i++;
    }
}
int main(){

    // char str[]={'H','a','r','r','y'};
    // this is not a valid string yet, we must add '\0'
    // at the end of this array
    char str[]= {'H','a','r','r','y','\0'};
    char str2[] = "Harry"; // this method is easier and automatically puts a null character at the end of the string
    printStr(str);
    printStr(str2);
    char str3[35];
    gets(str3);
    puts(str3);
    printf("%s",str3);
    return 0;
}