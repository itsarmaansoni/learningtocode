//Thu May 25 08:14:16 AM IST 2023
//CWH 34
// Strings in C

// there are no strings in C,
// we have to manually use the character array
// strings are represented by character arrays which end with the null 
// character '\0'

#include <stdio.h>

int main(){

    char st[] = {'h','a','r','r','y','\0','g','j','o'};
    char name[50] = "Armaan";
    for (int i=0;i<100;i++){
        // if (st[i]=='\0')
        // break;
        printf("%c",st[i]);
    }
    printf("\n");
    for (int i=0;i<50;i++){
        // if (name[i]=='\0')
        // break;
        printf("%c",name[i]);
    }
    printf("\n");

    return 0;
}
// we must always have an array of length one greater than 
// that of the string that we want to store in it,
// it's because we also have to store the '\0' null character
// to store the string "harry" which is of length 5, you need a char
// array of minimum length 6
