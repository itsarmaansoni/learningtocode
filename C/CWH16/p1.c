//Mon May  1 09:54:14 AM IST 2023
//CWH16
//break and continue statements in C

#include <stdio.h>
int main(){
	// what is a break statement?
	// what if you enter a loop
	// and you want to get out of the loop before it finishes iterating over all the possible values
	// you use the break statement;
	// the continue statement on the other hand just skips the current iteration
	

	printf("demonstrating break at iteration 4\n");
	for (int i=1;i<=10;i++){
		if (i==4){
			break;
			// this breaks the program at the iteration 4, and it doesn't execute
			// the rest of of the loop body after this, so numbers will
			// only be printed upto 3 and we will be out of loop
		}
 		printf("%d\n",i);
	}

	printf("demonstrating continue at iteration 4\n");
	for (int i=1;i<=10;i++){
		if(i==4){
			continue;
			// this skips iteration number 4, the rest of the loop body isn't executed
			// and we go to updation statement of the loop and then the loop condition is checked
			// and then we are back to loop body, if loop condition is true
		}
		printf("%d\n",i);
	}
}
