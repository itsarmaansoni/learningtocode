// Tue May  9 09:03:40 AM IST 2023
// CWH 22
// Units and Conversions : EXERCISE
//  this is an exercise from code with harry
//  and this will contain my solution to this problem

/*
well basically
here are the conversion factors
kms to miles
cms to inches
pounds to kgs
inches to meters
*/
// write a program that asks the user which one of these options they want
// when they select an option
// they have to enter a value to convert using that option
// the converted value should be displayed nicely

#include <stdio.h>
int main()
{
    int confac;
    double inp, out;
    printf("Here is a list of conversion factors:\n");
    printf("1.) Kilometers to Miles\n");
    printf("2.) Centimeters to inches\n");
    printf("3.) Pounds to Kilograms\n");
    printf("4.) Inches to meters\n");
    while (1)
    {
        printf("Enter 0 to exit or Select a value from 1 to 4:");
        scanf("%d", &confac);
        switch (confac)
        {
        case 0:
            printf("Exiting\n");
            return 0;
        case 1:
            printf("Please Enter the value in Kilometers:");
            scanf("%lf", &inp);
            printf("The converted value is:%lf Miles\n",(inp*0.621371));
            break;
        case 2:
            printf("Please Enter the value in Centimeters:");
            scanf("%lf", &inp);
            printf("The converted value is:%lf Inches\n",(inp*0.393701));
            break;
        case 3:
            printf("Please Enter the value in Pounds:");
            scanf("%lf", &inp);
            printf("The converted value is:%lf Kilograms\n",(inp*0.453592));
            break;
        case 4:
            printf("Please Enter the value in Inches:");
            scanf("%lf", &inp);
            printf("The converted value is:%lf meters\n",(inp*0.0254));
            break;
        default:
            printf("Invalid choice!\n");
        }
    }
    return 0;
}