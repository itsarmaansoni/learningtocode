// Tue May 16 08:07:27 AM IST 2023
// CWH 27
//  Arrays and Pointer Arithmetics in C
#include <stdio.h>

int main()
{

    int a = 34;
    int *ptra = &a;
    printf("%d\n", ptra);
    ptra++;
    printf("%d\n", ptra);
    // as you can see it increases by 4, because int is size 4 in my architecture
    int arr[] = {1, 2, 3, 4, 5, 6, 7, 345};
    printf("Value at position 3 of the array is %d\n", &arr[0]);
    printf("The address of first element of the array is %d\n", &arr[0]);
    printf("The address of first element of the array is %d\n", arr);
    printf("The address of second element of the array is %d\n", &arr[1]);
    printf("The address of second element of the array is %d\n", (arr + 1));
    // one thing to note: value of arr is a constant
    // arr ++ is illegal 
    // you can make your own pointer which is not constant
    int b=3;
    int* bptr=&b;
    bptr++; // totally valid
    printf("The value of first element of the array is %d\n", *&arr[0]); // could have just used arr[0]
    printf("The value of first element of the array is %d\n", *arr);
    printf("The value of second element of the array is %d\n", *&arr[1]); // could have just used arr[1]
    printf("The value of second element of the array is %d\n", *(arr + 1));

    return 0;
}