
// Format specifiers,constants and escape sequences in C
// 20th April, 2023
// CWH9


//Constants in C
// a constant is a value that cannot be changed in the program
// for example 14.53,'c',"code with harry"
// There are two ways to define a constant in C programming
// const keyword
// #define preprocessor
//

#include <stdio.h>


#define PI 3.141
int main(){
	const float b = 234.98;
//	b=234.3; // this statement will throw an error because we are trying to change a read-only variable
	// b which is initialized with keyword 'const'
	printf("b = %f\n",b);
	printf("PI = %f\n", PI);
//	PI=4.34; // this line will also throw an error
	return 0;
}















