
// Format specifiers and escape sequences in C
// 20th April, 2023
// CWH9
//
//
// escape sequence in C
// escape sequence in C programming is a sequence of characters, it doesn't represent itself when used
// in a string or character literal
// it is composed of 2 or more characters starting with backslash '\' for example '\n' represents newline
// \t represents tab (horizontal) \v represents vertical tab \a represents alarm/beep
// \" double quote \' single quote \0 null \? question mark \\ backslash
#include <stdio.h>
int main(){
	printf("\n");
	printf("\tthis was a tab before this line\n");
	printf("\vthis is was a vertical tab before this line\n");
	printf("this is a double quote \" this is a single quote \' this is a question mark\?\n");
	printf("this is a null character \0\n");
	printf("this is a backslash \\\n");
	return 0;
}
// well comments are a part of this chapter too:
// so these are comments, this particular comment is a single line comment
/* and this is a 
 * multi line comment in
 * c programming language, and I can't think of stuff to type now, so yeah,
 *
 */
