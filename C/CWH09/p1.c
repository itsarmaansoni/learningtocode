// Format specifiers and escape sequences in C
// 20th April, 2023
// CWH9


// What is a format specifier?
// format specifier is a way to tell the compiler the type of data that is going to be input or printed in the 
// output
// for example printf("This is a good boy %a.bf", var); will print var
// with b decimal points in a 'a' character space
// let's learn this with code
 
#include <stdio.h>
int main(){
	int a = 8;
	float b = 6.421;
	printf("The value of a is :%d\n", a); // %d is the format specifier for int
	printf("The value of b is :%f\n", b); // %f is the format specifier for float
	printf("The value of b in 2 decimal point precision is :%.2f\n", b); 
	printf("The value of b in 4 decimal point precision and 7 offset is :%7.4f\n", b);
	int c,d,e;
	c=1;
	d=34;
	e=987;
	printf("The value of c with 3 offset is:%3.0d\n",c);
	printf("The value of d with 3 offset is:%3.0d\n",d);
	printf("The value of e with 3 offset is:%3.0d\n",e);
	// it is worth noting that whatever comes after the point in the format specifier for int that is %d
	// so for example %6.4d means that 6 will be the offset and there will be trailing zeroes added 
	// to the number in order to make it of length 4, if it's already length 4 then no zeroes will be added
	// in the %f, for example %6.4f would mean that 6 offset and 4 decimal accuracy

	printf("The value of c with 3 offset is:%0.3d\n",c);
	printf("The value of d with 3 offset is:%0.3d\n",d);
	printf("The value of e with 3 offset is:%0.3d\n",e);
	// what if we give negative value for offset?
	// let's try it out with some float values and int values
	int gh;
	float hg;
	gh=23;
	hg=6.4;
	printf("The value of gh with negative offset -8 is:%-8.d:\n", gh);
	printf("The value of hg with negative offset -8 is:%-8.f:\n", hg);

	// it is worth noting that not every possible format specifier can be explained in this
	// one chapter, but we can always experiment with different format specifiers and different 
	// values in them and see how it affects the output and input to learn more about them,
	// it's also tied with experience, the more experience you have, the better you utilize and know
	// about format specifiers
	// here are some important format specifiers
	// %c for printing character
	// %d for printing integer
	// %f for printing floating point number
	// %l for printing long integer
	// %lf for printing double
	// %Lf for printing long double
	// these are ought to be remembered 
	return 0;
}
