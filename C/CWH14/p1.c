// while loops in C
// 27th April, 2023
// CWH14


#include <stdio.h>
int main(){
	// while loop syntax
	// while (loop condition){
	// 	loop body;
	// } 
	// here is an example of a while loop that prints numbers from 1 to 30
	
	int i=1;
	while (i<=30){
		printf("%d\n", i);
		i++;
	}
	return 0;
}
