//Tue May 23 08:09:12 AM IST 2023
//CWH 32
// passing arrays as arguments in C
#include <stdio.h>

    /*always remember, whenever we pass an array into a function
    the ptr of the array is passed, so if we change the array in the function
    the actual array will be changed*/
float average(int array[]){
    float sum=0.0;
    float len=6.0;
    for (int i=0;i<len;i++){
        sum+=array[i];
    }
    float aver=sum/len;
    return aver;
}
float averageptr(int* array){
    float sum=0.0;
    float len=6.0;
    for (int i=0;i<len;i++){
        sum+=array[i];
    }
    float aver=sum/len;
    return aver;
}
float averageptr2(int* array){
    float sum=0.0;
    float len=6.0;
    for (int i=0;i<len;i++){
        sum+=*(array+i);
    }
    float aver=sum/len;
    return aver;
}
int main(){

    int arr[] = {1,2,5,7,2,6};
    float avg=average(arr);
    printf("%f\n",avg);
    float avg2=averageptr(arr);
    printf("%f\n",avg2);
    float avg3=averageptr2(arr);
    printf("%f\n",avg3);
    
    return 0;
}
// I couldn't type properly today because of a finger injury
// so i will also paste code with harry's program in p2.c
