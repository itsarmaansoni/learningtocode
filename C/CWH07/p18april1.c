//Understanding operators in C programming language
//



#include <stdio.h>
int main()
{
	int a,b;
	a=32;
	b=7;
	//ARITHMETIC OPERATORS
	printf("a+b = %d\n", a+b);
	printf("a-b = %d\n", a-b);
	printf("a*b = %d\n", a*b);
	printf("a/b = %d\n", a/b);
	printf("a modulo b = %d\n", a%b);

	


	// what if the operands were floating point? or either one of them was floating point
	int c;
	float d;
	c=32;
	d=7.4;
	printf("value of c is %d\n",c);
	printf("value of d is %f\n",d);
	printf("c+d = %f\n",c+d); // we are using %f because whenever there is an operation between an integer and a float, the output is in float, we can't use %d
	printf("c-d = %f\n",c-d); // if we try to use %d it wll give weird values
	printf("c*d = %f\n",c*d);
	printf("c/d = %f\n",c/d);


	//RELATIONAL OPERATORS
	// they return 1 or 0 which mean true or false respectively 
	// in c any value that is not 0 is considered true and 0 is considered false if we do bool operations
	
	int f,g;
	f=15;
	g=19;
	printf("f>g= %d\n",f>g);
	printf("f<g= %d\n",f<g);
	printf("f<=g= %d\n",f<=g);
	printf("f>=g= %d\n",f>=g);
	
	//Logical operators
	// logical operators and comparision operators are closely related because logical operators also return 1 & 0 for true and false, 
	// they only work with 1 and 0 values as well, i.e. true and false
	// && operator is "and operator"
	// || operator is "or operator"
	// ! operator is "logical not operator"
	// here is the truth table of the && operator 
	// && returns 1 if both values are 1 and returns 0 in all other cases, i.e (0,1),(1,0),(0,0), only gives 1 when (1,1)
	// here is the truth table of the || operator
	// || returns 1 if any one or both inputs are 1, i.e it will return 1 in these cases (1,1),(0,1),(1,0) and return false when (0,0) 
	// ! operator flips the input, if the input is 1, it will return 0 and if the input is 0, it will return 1
	
	
	int v1=1,v2=0; //you can try and change these values to see how it affects the output
	printf("v1 && v2 is %d\n", v1&&v2);	
	printf("v1 || v2 is %d\n", v1||v2);	
	printf("!v1 is %d\n", !v1);	

	// it should be noted that any value other than 0 is also considered true, that is 22 is true and so is 98, only 0 is false
	




	//BITWISE OPERATOR
	// they work on bits
	// example 2 & 3, & is the and bitwise operator 
	// 2 in binary is 10
	// 3 in binary is 11
	// so that 2 & 3 will be (10)&(11) 
	// 0 & 1 will be 0 and 1 & 1 will be 1 
	// so the value will be 10, so it will give the value 2 
	
	int j,k;
	j=2;
	k=3;
	printf("j&k=%d",j&k);


	// here is the truth table of all bitwise operators
	// a	b	a&b	a|b	a^b
	// 0	0	0	0	0
	// 0	1	0	1	1
	// 1	1	1	1	0
	// 1	0	0	1	1
	//& is read "and" | is read "or" and ^ is read "xor" or "exclusive or" 
	// there are more bitwise operators that are ~ which is one's complement operator and << which is left shift operator and >> which is right shift operator
	
	//ASSIGNMENT OPERATOR
	// = simple assignment operator, we use it all the time when initializing variables
	// += Add and assignment operator. it adds the right operand t the left operand and assign the result to the left operand
	// -= subtract and assignment operator. it subtracts the right operand from the left operand and the result is assigned to the left operand
	// *= multiply and assignment operator
	// /= divide and assignment operator
	
	//Misc operators
	//sizeof() returns the size of a variable sizeof(a), where a is integer
	//& returns the address of a variable &a returns the actual address of the variable a
	//* pointer to a varible *a
	//?: conditional expression if condition is true? then value X: otherwise value y 
	int lkj=34;
	float jkl=234.234;
	printf("\n");
	printf("%d",sizeof(int));
	printf("\n");
	printf("%d",sizeof(double));
	//operator precedence
	//* / %    (left to right)
	//+ -      (left to right) 
	//<< >>    ( left to right)
	//< <= > >=   (left to right)

	return 0;
}
