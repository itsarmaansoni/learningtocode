//Fri Jul 14 09:19:27 AM IST 2023
//CWH 70
//Exercise: Command Line Calculator in C
#include<stdio.h>
#include <stdlib.h>
#include <string.h>
int main(int argc,char* argv[])
{
    // You have to create a command line utility to add/subtract/divide/multiply two numbers
    // First command line argument of your c program must be the operation.
    // The next arguments being the two numbers. For example:
    // >>Command.c add 45 4
    // >>49

    // HERE IS MY SOLUTION!
    // printf("number of arguments are :%d\n",argc);
    // if (argc>=2){
    //     for (int i=0;i<argc;i++){
    //         printf("argument number %d is %s\n",i,argv[i]);
    //     }
    // }
    if (argc!=4){
        printf("Invalid Number of Arguments\n");
        return 1;
    }
    else {
        if(strcmp(argv[1],"add")==0){
            int x = atoi(argv[2]);
            int y = atoi(argv[3]);
            printf("Sum of %d and %d is %d\n",x,y,x+y);
        }
        else if(strcmp(argv[1],"subtract")==0){
            int x = atoi(argv[2]);
            int y = atoi(argv[3]);
            printf("subtraction of %d and %d is %d\n",x,y,x-y);
        }
        else if(strcmp(argv[1],"divide")==0){
            int x = atoi(argv[2]);
            int y = atoi(argv[3]);
            printf("division of %d and %d is %d\n",x,y,x/y);
        }
        else if(strcmp(argv[1],"multiply")==0){
            int x = atoi(argv[2]);
            int y = atoi(argv[3]);
            printf("multiplication of %d and %d is %d\n",x,y,x*y);
        }
        else {
            printf("Invalid operation!\n");
            return 2;
        }
    }
    return 0;
}
