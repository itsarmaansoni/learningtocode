// exercise by code with harry
// task: print a multiplication table of a number entered by the user 
// in pretty form 
// example:
// Enter the number you want the multiplication table of : 6
// Table of 6:
// 6 x 1 = 6
// 6 x 2 = 12
// .....
// 6 x 10 = 60


// my solution to the problem
#include <stdio.h>
int main(){
	printf("Enter the number you want the multiplication table of :");
	int n;
	scanf("%d",&n);
	for (int i=1;i<=10;i++){
		printf("%d x %d = %d\n",n,i,(n*i));
	}
	return 0;
}
