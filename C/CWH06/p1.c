
// this program is to demonstrate how you can find the size of each data type in c


#include <stdio.h>
int main(){
	printf("%lu", sizeof(int)); // please note that the size of data type is dependent upon architecture
	// there are some basic functions that you should know in order to use C at the basic level
	// these are the printf and scanf, they are used to take data from the user and to print data in the cosole for the user to see
	// here is how they work
	int number;
	printf("Enter a number:"); // this printf() function takes the string "Enter a number:" and prints it to console
	scanf("%d", &number); // scanf function will now take one int value from the user, it will take int value because %d is used, we will learn about these in detail
	// in chapters ahead
	//
	//
	// some basics about operators,
	// you can use + - / * and % operators in c
	// for example 5+3 will evaluate 8
	printf("%d", 5+3); // will print 8
	// don't worry, we will learn about this in so much more detail in chapters ahead
	return 0;
}
