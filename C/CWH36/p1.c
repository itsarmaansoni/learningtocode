//Mon May 29 10:25:14 AM IST 2023
//CWH 36
// Exercise: Array Reversal 


#include <stdio.h>

// write a function void revarr(int[] arr) such that
// it takes an int array and reverses it
void swap(int* ptra, int* ptrb){
    int temp = *ptra;
    *ptra = *ptrb;
    *ptrb = temp;
}
void revArr(int arr[],int len){
    for (int i = 0; i<len/2; i++){
        swap(&arr[i],&arr[len-i-1]);
    }
}
int main(){
    int arrLen=7;
    int arrayToBeReversed[] = {34,65,76,345,65,34,876};
    printf("\n\nOriginal Array is :\n\n");

    for (int i=0;i<arrLen;i++){
        printf("array element %d: %d\n",i,arrayToBeReversed[i]);
    }
    revArr(arrayToBeReversed,arrLen);
    printf("\n\nReversed Array is :\n\n");
     for (int i=0;i<arrLen;i++){
        printf("array element %d: %d\n",i,arrayToBeReversed[i]);
    }

    return 0;
}


/*
my logic :
I want to check whether my logic works for all array lengths
let's make a sample array
of an odd length
in this case 5
2
3
4
6
7

my algorithm will swap like this:
arr[i] will be swapped with arr[len(arr)-i-1]
soo

*/