//Thu May  4 08:43:10 AM IST 2023
//CWH 19
//functions in C

// functions are used to divide the program into small parts
// functions alow for reusability of code
// they make the program modular
// also called procedure or subroutine


// basic syntax of a C program
// return_type function_name(data_type parameter1, data_type parameter2.... parameterx)
// {
// 	//function code
// 	return value_of_return_type
// }

// a function is declared in order for the compiler to know it's existance
// a function is defined to get some task done
// a function is called in order to be used
//
// a function definition is where you write the code for the function
// a function declaration is where you just write the basics about the function
// like it's return value and parameters, and function name 
//
//
// Types of functions:
// 1.) Library functions ( functions that are defined in the system header files that we import )
// 2.) User-Defined functions ( functions that are writen by the user )
#include <stdio.h>
int sum(int a, int b){
	
	return a+b;
}
int main(){
	int a,b,c;
	a = 9;
	b = 87;
	c = sum (a,b);
	printf("The sum is %d\n",c);
	return 0;
}
