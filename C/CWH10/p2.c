
//If-else control statements in C
//21 April, 2023
//CWH10


// solution to the exercise of maths and science pass gifts


// if someone passed both maths and science they get 45$
// if someone passed only science they get 10$
// if someone passed only maths they get 10$
// else they get nothing
//


#include <stdio.h>
int main(){
	int maths, science;
	printf("Enter 1 if you passed science and 0 if you failed:");
	scanf("%d", &science);
	printf("Enter 1 if you passed maths and 0 if you failed:");
	scanf("%d", &maths);

	if (maths&&science)
		printf("you got 45$\n");
//	else if (maths)
//		printf("you got 10$\n");
//	else if (science)
//		printf("you got 10$\n");
	else if (maths||science) // this was a better condition, that eliminated the need of two conditions above
		printf("you got 10$\n");
	else
		printf("you got 0$\n");
	return 0;
}
