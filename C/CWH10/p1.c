//If-else control statements in C
//21 April, 2023
//CWH10

// C if else statements
// it is used to perform operation based on some conditions

#include <stdio.h>
int main(){
	
	int age;
	printf("Enter your age:");
	scanf("%d",&age);
	if (age<18){
		printf("You are not invited to my party\n");
	}
	else if(age==18){
		printf("You barely made it to my party\n");
	}
	else {
		printf("You are invited to my party\n");
	}
	return 0;
}
// CWH gave an exercise, and my solution is in p2.c

