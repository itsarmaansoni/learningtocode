# Tue May 30 08:38:34 AM IST 2023
# CWH 37
# Finally Keyword in Python

def func1():
    try:
        l = [1,5,4,7]
        i = int(input("Enter the index:"))
        print(l[i])
        return 1
    except:
        print("Some error occurred")
        return 0
    finally:
        print("I am always executed!")

    #stuff written inside finally block is always executed, no matter what
x=func1()
print(x)