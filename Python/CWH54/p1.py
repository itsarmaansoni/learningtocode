# Thu Jun 22 08:41:00 AM IST 2023
# CWH 54
# is vs "==" in python


a = None
b = None

print(a is b) # exact location of object in memory
print(a is None) # exact location of object in memory
print(a == b) # value