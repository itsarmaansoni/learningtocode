# Wed Jul 26 09:18:00 AM IST 2023
# CWH 83
# Exercise : Shoutouts to Everyone!

# Here is my Solution!
import gtts
import os
l = ["Rahul","Nishant","Harry","Armaan","Anmol"]
myText=""
for name in l:
    myText+="shoutout to "+name+" "
language = "en"
myObj = gtts.gTTS(text=myText,lang=language,slow=False)
myObj.save("shoutout.mp3")
os.system("mpv shoutout.mp3")
