# Fri May 26 09:51:03 AM IST 2023
# CWH 35
# for loop with else in Python

for i in range (5):
    print(i)
else:
    print("Sorry 5 not in range(5)")

for i in range (5):
    print(i)
    if i==3:
        break
else:
    print("will the else block execute?")
# no it doesn't, else block only executes if the loop condition is false.
