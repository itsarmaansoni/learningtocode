# Thu May 18 08:33:24 AM IST 2023
# CWH 29
# Docstrings and PEP-8 in Python

# Docstrings are the string literals that appear right after the 
# definition of a function,method, class or module 

def square(n):
    '''this function takes a number n and returns the square of the number'''
    return n**2

print(square(4))
print(square.__doc__)
# note , docstring has to be written just under the function declaration,
# even if there is one line of code between function declaration and docstring, then it won't
# be a docstring anymore, it will just be a comment

def cube(n):
    n2=n*n*n
    '''this is not a docstring anymore, because there is a line of code above this'''
    return n2
print(cube.__doc__)
print(cube(4))

# PEP-8 are basically guidelines on how to write better code
# the zen of python is an easter egg, which you can find by typing import this
# in a python program