# Mon May 29 10:25:14 AM IST 2023
# CWH 36
# error handling in Python
try:
    a = int(input("Enter a number to print the multiplication table of :"))
    print(f"The multiplication table of {a} is :")
    for k in range(1,11):
        print(f"{a} x {k} = {a*k}")
except Exception as e:
    print(e)
    print("sorry some errors occurred")

try:
    num = int(input("Enter a number:"))
except ValueError as e:
    print(e)
    print("This is a value error")
except IndexError as e:
    print(e)
    print("this is index error")
print("Some lines of code")
print("End of program")

# you can have multiple except blocks to handle different types of errors