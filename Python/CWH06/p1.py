#understanding variables and Data Types

a = 1  #this is saying store the value 1, which is an integer in a variable named a, in the memory, python is dynamically typed so no data type needed to be mentioned
print(a) #printing the value of a

b = "Harry" # here python understands that the data type is string
print (b) # this prints harry
#print (Harry)
# why can't we just say b = Harry, without the quotes? because that will give us an error, what if there is a variable named harry? that's why we enclose strings in ""
c=True
d=None 
# here a,b,c & d are different kinds of variables, we don't need to explicitly mention the data type in python
# a is int
# b is String
# c is Bool
# d is None type


#why do we need different types of data types?
# so that we can store values of different kind in a our memory
# it also specifies the type of value a variable can hold
# like we can't do a + b because a is an int and b is a string

#we can print the datatype of a variable using the type() function
print(type(a))
print(type(b))
print(type(c))
print(type(d))

#we also have complex numbers in python!
cn=complex(8,2)
print(type(cn))
#first part is imaginary that is 8i and the second part is real that is 2 making 8i+2


# we also have sequenced data types like list tuples dictionaries 

list1=[8,2.3,[-4,5], ["apple", "banana"]] #a list is a data type that can store together any type of data type, int string and even list themselves!

tuple1=(8,2.3,[-4,5], ["apple", "banana"])
print(type(list1))
print(type(tuple1))
#lists are mutable, i.e that they can be changed, tuples are immutable, i.e they cannot be changed
dict1={"name":"Armaan","age":19,"canVote":True}
print(type(dict1))
#a dictionary is a map data type, with key-value pairs
#Everything in python is an object
