# Mon Jun 12 08:34:58 AM IST 2023
# CWH 46
# OS module

# OS module helps with performing operations with your OS
import os
if (not os.path.exists("data")):
    os.mkdir("data")
for day in range(1,101):
    if (len(str(day))==1):
        os.mkdir(f"data/day00{str(day)}")
    elif (len(str(day))==2):
        os.mkdir(f"data/day0{str(day)}")
    else:
        os.mkdir(f"data/day{str(day)}")
folders = os.listdir("data")
os.chdir("data")
for folder in folders:
    os.chdir(folder)
    os.open("main.py",'x')
    os.chdir("..")

