# solution to calculator exercise in Python by code with harry
# 19 April 2023
# CWH8


a = 50
b = 3


print("The value of",a,"+",b,"is",a+b)
print("The value of",a,"-",b,"is",a-b)
print("The value of",a,"*",b,"is",a*b)
print("The value of",a,"/",b,"is",a/b)

#its pretty simple, in my own solution I used if,elif and else statements, which was not necessary to make this work, + I used user input values, + I gave them choice of the operator, my program was better than this, but this is good for beginners
