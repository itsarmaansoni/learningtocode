# Fri Jun  2 08:42:19 AM IST 2023
# CWH 40
# EXERCISE : Write a python program to translate a message into secret code language. Use the rules below to translate normal English into secret code language

# Coding:
# if the word contains atleast 3 characters, remove the first letter and append it at the end
#   now append three random characters at the starting and the end
# else:
#   simply reverse the string

# Decoding:
# if the word contains less than 3 characters, reverse it
# else:
#   remove 3 random characters from start and end. Now remove the last letter and append it to the beginning

# Your program should ask whether you want to code or decode


#OKAY SO HERE IS ME TRYING TO SOLVE IT

# I want to make two functions, named encrypt and decrypt, even tho
# it's not an encryption, I still think it's a cool name

# encrypt() and decrypt() take a string and return a string 
import random


def rand3lettercombogen():
    alphas="abcdefghijklmnopqrstuvwxyz"
    a = random.randrange(0,26)
    b = random.randrange(0,26)
    c = random.randrange(0,26)
    comb=""+alphas[a]+alphas[b]+alphas[c]
    return comb
def jumbler(word):
    ran3pref=rand3lettercombogen()
    ran3post=rand3lettercombogen()
    word = word + word[0]
    word = word[1:]
    word = ran3pref+word+ran3post
    return word

def wordform(wo):
    nw=""
    if len(wo)>2:
        nw=jumbler(wo)
    else:
        for k in wo:
            nw=k+nw
    return nw

def encrypt(string):
    string=string+" "
    word = ""
    ns=""
    for ch in string:
        if (ch==' '):
            ns=ns+wordform(word)+" "
            word=""
        else:
            word=word+ch
    return ns
def dejumbler(wor):
    if len(wor)==6:
        return ""
    wor=wor[3:-3]
    wor=wor[len(wor)-1]+wor
    wor=wor[:-1]
    return wor
def dewordform(word):
    nw=""
    if len(word)>2:
        nw=dejumbler(word)
    else:
        for k in word:
            nw=k+nw
    return nw
def decrypt(string):
    string=string+" "
    word = ""
    ns=""
    for ch in string:
        if (ch==' '):
            ns=ns+dewordform(word)+" "
            word=""
        else:
            word=word+ch
    return ns

# print(encrypt("code with harry is the best coding channel ever!"))
# print(decrypt("gmjodecflj xynithwxko qcearryhhib si ctahethab zntestbvwg fmoodingcgrf mzuhannelczmq tmsver!eofa "))
while True:
    choice = int(input("for encryption enter 1 for decryption enter 2, 0 to quit:"))
    if(choice==0):
        exit()
    elif(choice==1):
        encstr=input("Enter the string to encrypt:")
        encstr=encrypt(encstr)
        print("the encrypted string is :"+encstr)
    elif(choice==2):
        decstr=input("Enter the string to decrypt:")
        decstr=decrypt(decstr)
        print("the decrypted string is :"+decstr)
    else:
        print("INVALID CHOICE!!!!")