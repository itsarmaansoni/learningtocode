# Tue May  2 07:38:13 PM IST 2023
# CWH 17
# for loop in python
# for loops can iterate over a sequence of iterable objects
# list, tuples, and strings are example of some iterable objects
name = "Armaan"
for i in name:
    print(i)
# what the above program does is that the for loop iterates over the string object name, 
# i is the character at that iteration
# for list it will be elements in the list
# what if we don't wanna iterate over a range?
# what if we want the loop to run a specfic number of times?
# that's when range() function comes into play
for k in range(5):
    print(k)
# this means it will iterate over 0,1,2,3,4
# and print them

for k in range(3,11):
    print(k)
# this will iterate from 3 till 10 which is (11-1)
# and print them
# learn more about range function from python docs
