# Mon May 15 08:31:38 AM IST 2023
# CWH 26
# code with harry's solution to the greeting problem exercise 2
import time 
tsu= int(time.strftime('%H'))
if (tsu>=5 and tsu<=11):
    print("Good morning Sir!")
elif (tsu>=12 and tsu<=16):
    print("Good afternoon Sir!")
elif (tsu>=17 and tsu<=23):
    print("Good evening Sir!")
else:
    print("Good Night Sir!")