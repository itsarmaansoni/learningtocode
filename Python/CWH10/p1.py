# Input function in python
# april 21, 2023
# cwh10


#the input() function in python is used to take user input
#syntax variable = input()
# the input function takes a string as input 
a = input("Enter your name:") # input function can also take a string to print before the input
# in this case it's "Enter your name:"
print("My name is",a)
# if you are working with number input, it is very important to typecast the inputs properly before using
b = int(input("Enter number 1:"))
c = int(input("Enter number 2:"))
print("The sum of these two numbers is:",(b+c))
#as you can see that you can directly typecast the input strings
# an exercise is to check what happens if with all 
# the arithmatic operators used on string numbers that
# are not typecasted 

g=input("Enter g:")
h=input("Enter h:")
print(g+h)
#print(g-h) # minus operation not supported for strings
#print(g*h) multiply not supported for strings 
#print(g/h) divide not supported for strings


# here are the normal operations
# after type casting
g=int(g)
h=int(h)
print(g+h)
print(g-h) 
print(g*h)
print(g/h)
