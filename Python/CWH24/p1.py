# Thu May 11 08:56:39 AM IST 2023
# CWH 24
# Tuples in Python

tup=(1,5)
print(type(tup),tup)
tup2=(1,)
print(type(tup2),tup2)
# when you are making a tuple with one element,
# adding a comma is necessary, or it will 
# make the data type that of the element
# tuples are immutable
# tuples are basically constant lists
# meaning they can't be changed
# other than that tuples are basically lists




