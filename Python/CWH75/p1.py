# Fri Jul 21 02:52:55 PM IST 2023
# CWH 75
# Code With Harry's Solution to cluttered folder cleaner exercise
import os

files = os.listdir("clutteredFolder")
i = 1
for file in files:
  if file.endswith(".png"):
    print(file)
    os.rename(f"clutteredFolder/{file}", f"clutteredFolder/{i}.png")
    i = i + 1