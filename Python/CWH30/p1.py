# Fri May 19 08:18:15 AM IST 2023
# CWH 30
# Recursion function in Python 
# function that calls itself is called recursive function
def factorial(num):
    if (num==1 or num==0): # base case or condition
        return 1
    else :
        return num*factorial(num-1)
print("the factorial of 7 is",factorial(7))
print("the factorial of 5 is",factorial(5))
print("the factorial of 3 is",factorial(3))
print("the factorial of 1 is",factorial(1))
print("the factorial of 0 is",factorial(0))
print("the factorial of 12 is",factorial(12))

# quick quiz: write a function to calculate the terms of fibonacci series using recursion 
def fibonacci(term):
    if (term<=2):
        return term-1
    else:
        return fibonacci(term-1)+fibonacci(term-2)
print("the term 1 of fibonacci series is",fibonacci(1))
print("the term 2 of fibonacci series is",fibonacci(2))
print("the term 3 of fibonacci series is",fibonacci(3))
print("the term 4 of fibonacci series is",fibonacci(4))
print("the term 5 of fibonacci series is",fibonacci(5))
print("the term 6 of fibonacci series is",fibonacci(6))
print("the term 7 of fibonacci series is",fibonacci(7))



