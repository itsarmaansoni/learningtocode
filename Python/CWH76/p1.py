# Mon Jul 24 08:46:52 AM IST 2023
# CWH 76
# Exercise: merge the pdf

#HERE is my Solution!!

# I couldn't solve it, this is one of those things that you just have to look up
# because this isn't about logic, this is learning how to use a new tool
import os
from pypdf import PdfReader
from pypdf import PdfWriter


os.chdir("/home/life/Downloads")
fileList = os.listdir()
pdfList=list()
for file in fileList:
    if (file.endswith(".pdf")):
        pdfList.append(file)
# print(pdfList)

pdf1=PdfReader("/home/life/Downloads/"+pdfList[0])
pdf2=PdfReader("/home/life/Downloads/"+pdfList[1])
pdf3=PdfWriter("/home/life/Downloads/Dummy.pdf")
pdf3.append_pages_from_reader(pdf1)
pdf3.append_pages_from_reader(pdf2)
pdf3.write("/home/life/Downloads/Dummy.pdf")

