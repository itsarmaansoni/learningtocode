# Fri Jul 14 09:19:27 AM IST 2023
# CWH 70
# Class Methods as Alternative Constructors
class Employee:
    def __init__(self,name,salary):
        self.name = name
        self.salary = salary
    @classmethod
    def fromStr(cls,string):
        return cls(string.split("-")[0],string.split("-")[1])
e1 = Employee("Harry",12000)
print(e1.name)
print(e1.salary)
string = "Armaan-38988"
e2 = Employee(string.split("-")[0],string.split("-")[1])
print(e2.name)
print(e2.salary)
# but there is a better way than doing this
# using class methods for construction of objects
string = "Rohan-83829"
e3 = Employee.fromStr(string)
print(e3.name)
print(e3.salary)
