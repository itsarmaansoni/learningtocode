# Mon May  8 08:07:18 AM IST 2023
# CWH 21
# function arguments in python

def average(a,b):
    print("the average is",(a+b)/2)
average(4,3) 

def average2(c=9,d=3): #default arguments
    print("the average is",(c+d)/2)
average2()

def average3(*numbers):
    # // numbers is actually a tuple here
    sum=0
    for num in numbers:
        sum=sum+num
    print("the average of numbers entered is",(sum/len(numbers)))
average3(234,23,234,23,2,26,98)
