# Mon Jul 31 09:50:01 AM IST 2023
# CWH 91
# Generators in Python
def my_generator():
    for i in range(50000000):
      # Complex computations
      yield i

gen = my_generator()
# print(next(gen))
# print(next(gen))
# print(next(gen))

for j in  gen:
  print(j)