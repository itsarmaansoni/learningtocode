# Thu May  4 08:43:10 AM IST 2023
# CWH 19
# Break and Continue in Python

# what is a break statement?
# what if you enter a loop
# and you want to get out of the loop before it finishes iterating over all the possible values
# you use the break statement;
# the continue statement on the other hand just skips the current iteration

print("This is the demonstration of break statement in iteration 4")
for k in range (0,10):
    if (k==4):
        break
    print(k)
print("This is the demonstration of continue statement in iteration 4")
for k in range (0,10):
    if (k==4):
        continue
    print(k)

