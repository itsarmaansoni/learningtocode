# Wed Jun  7 09:00:01 AM IST 2023
# CWH 43
# Virtual environments in python
'''
to create a virtual Environment 
$ python -m venv virutal_environment_name
this will create a virutal_environment_name folder in the current directory 
after that you can activate that environment by typing
$ source virtual_environment_name/bin/activate
you can deactivate the environment by 
typing 
$ deactivate

while your environment is activated you can install multiple packages 
with version of your choice, that your project is compatible with 

to create a requirements.txt file for your env
pip freeze > requirements.txt

to install all the required packages
pip install -r requirements.txt

'''

