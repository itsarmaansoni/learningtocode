# Tue May  9 09:03:40 AM IST 2023
# CWH 22
# Lists in python

# Lists are iterable datatype in python


marks = [3,5,6]
print(marks)
print(type(marks))
marks[0]=34
print(marks)
print(marks[0])
# lists are mutable 
marks.append(45)
print(marks)
# lists can hold anytype of data and multiple types of data 
# for example 
marks.append(True)
marks.append(34.34)
marks.append("Armaan")
marks.append([34,54,76])
marks.append({3:34})
marks.append((3,5,6))
print(marks) 
# as you can see it can hold anything 
# you can iterate over lists with for loop 
# because they are iterable objects 
for k in marks:
    print(k)
# first item has index 0 and second item has index 1 and so on
# so a list of size 5 will have index from 0 to 4
# if we pass a negative index in a list 
# we have to first convert it to a positive index 
# for example 
print(len(marks))
# marks[-3] will be len(marks)-3 = 10 - 3 = 7 so it's 
# equal to marks[7] which will be
print(marks[7])
print(marks[-3])
# as you can see they are the same 

# this can be used to check if an element is in the list 

if 45 in marks:
    print("yes")
else :
    print("No")

if "Ha" in "Harry":
    print("ok")
if "ha" in "Harry":
    print("ok")
if 'r' in "Harry":
    print("yes")


# list comprehension
lst = [ i for i in range(34)]
lst2 = [i*i for i in range(20)]
lst3 = [i*i for i in range(13) if i%2==0]
print(lst)
print(lst2)
print(lst3)