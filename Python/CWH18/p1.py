# Wed May  3 08:54:43 AM IST 2023
# CWH 18
# while loops in python
# do while loops in python
i=0
while (i<=3):
    print(i)
    i+=1
f=1


# there are no do while loops in python
# however you can simulate a do while loop in python
# basically the funda is that your loop block should execute atleast once
# you achieve it by doing this
# while True:
#   //loop body
#   if (!(loop condition)):
#       break

