# Tue Jun 27 08:31:23 AM IST 2023
# CWH 57
# classes and objects in python

class Person:
    name = "Harry"
    occupation = "Software Developer"
    networth = 10000
    def info(self):
        print(f"{self.name} is a {self.occupation}")

a = Person()
a.name = "Shubham"
print(a.name)
print(a.occupation)
print(a.networth)
a.info()
b = Person()
b.name = "Armaan"
b.occupation = "Student"
b.networth = 0
b.info()