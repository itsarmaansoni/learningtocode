# Wed May 10 08:36:28 AM IST 2023
# CWH 23
# list methods in python 
l = [1,2,5,5,4,2,23]
print(l)
l.append(32)
print(l)
l.sort()
print(l)
l.sort(reverse=True) 
# reverse parameter when set to True will sort the list in descending order
print(l)
l.reverse()
# this just reverses the list firts element becomes last second becomes second last and so on
print(l)
print(l.index(2))
# returns the index of the first occurance of 2 in the list
print(l.count(5))
# this counts the number of occurances of 5 in the list

m = l 
# a beginner might think that m is now a copied list and is same as l 
# which can be changed without affecting the original list l, but that's not true
# m is just a reference to l, changing m will change l
#  here is a demonstration 
m[0]=1
print(m)
print(l)
# as you can see both lists changed
# to avoid this, we use the copy function when making copies of a list
m = l.copy()
# now you can freely edit m and l and they wouldn't interfere with one another 

print(l)
l.insert(1,899)
# this above function insert takes two args, index and value, it inserts the 
# value at that index and shifts other values in the list accordingly
print(l)


newlist=[34,71,334]
l.extend(newlist)
# what this does is basically append the elements of newlist at the end of l 
print(l)
