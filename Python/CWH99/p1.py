# Tue Aug  2 09:57:21 PM IST 2023
# CWH 99
# Code with Harry's solution to drink water reminder exercise

import os
import time 

REPEAT_INTERVAL = 3600 # Repeat frequency in seconds

while True:
  command = "osascript -e \'say \"Hey Harry drink water\"\'; osascript -e \'display alert \"Hey Harry, Drink water\"\'"
  os.system(command)
  time.sleep(REPEAT_INTERVAL)