# String methods in pyton
# 25th April, 2023 ( I was supposed to do this on 26th, but I feel like studying today, so yeah)
# CWH 13


a = "Harry"
print (len(a))
print (a.upper())
#strings are immutable objects, so this means that whatever output you get from these functions are new strings
print (a.lower())
b="!!!Armaan!!!!!!!!!!!!"
print(b.rstrip("!"))
#only strips trailing, not leading
print(b.replace("Ar","Ro"))
places="Delhi Gurugram Mumbai"
print(places.split(" ")) #splits string with seprator provided and returns a list of those 
blogHeading = "this is a blog heading"
print(blogHeading.capitalize())
#capitalize converts first character to uppercase and all other characters to lowercase
print(blogHeading.center(50))
# there are a lot of string methods, it's impossible to mug them up, but it's good to keep them in mind,
# so that you can use them instead of having to write code for everything from scratch
