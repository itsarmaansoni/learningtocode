# Tue Aug  1 12:23:12 PM IST 2023
# CWH 94
# Exercise : Drink water reminder


# this is gonna be ez, I am just going to use the os module and use the notification
# api that I am currently using which is notify-send
# in Gentoo
# here is my solution
import os
import time
# I want it to remind me every 2 hours from it's start
# or... I could use fixed times like 9 am 11 am then 1 pm
# let's see
# I think I am gonna use a while loop

#okay, so, I want to launch this program in startup 
# also, this will save the launch time and will generate a list of times during the day in which 
# I need to drink water
launchHour=time.strftime("%H")
launchMin=time.strftime("%M")
notifList=[str(t)+" "+launchMin for t in range(int(launchHour),24)]
# print(notifList)
while True:
    time.sleep(3)
    if (time.strftime("%H %M") in notifList):
        # print(time.strftime("%H %M %S"))
        os.system('notify-send --icon=/home/life/Downloads/waterIcon.png "Armaan, it\'s time to drink some water"')
        time.sleep(60)