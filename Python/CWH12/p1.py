# String Slicing and other operations on strings
# 25, April, 2023
# CWH12


names = "Harry,Shubham,Mayank"
print(names[0:5])
# the above statement will slice the string names from 0 to 4(5-1)
print(len(names)) # len functions gives the length of a string
fruit="Mango"
print(fruit,"is a",len(fruit),"letter word")


place= "Gurugram"
print(place[0:4]) # slicing from 0 to (4-1) which is 3
print(place[:4]) # automatically asumes a zero if not given, so it gives you the first four letters of string
print(place[4:8]) # slicing from 4 to (8-1) which is 7
print(place[4:]) # if no end specified, it will slice till the end from the given index
print(place[4:len(place)]) #slicing from 4 index to len(place)-1, which is (8-1) which is 7, so the same as above


# what if we give negative values
print(place[0:-4]) # when we give negative value, it will evaluate it by subtracting it from len of string
# so, if we give -4, it will do len(place)-4=8-4=4, so it will put a 4 there, and so the slicing will be from 
# 0 to 4-1 which is 3, so the slicing will be from 0 to 3 index
# putting -4 is like putting len(place)-4
