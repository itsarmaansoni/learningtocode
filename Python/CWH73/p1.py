# Wed Jul 19 09:54:45 AM IST 2023
# CWH 73
# Magic/Dunder methods in Python

from emp import Employee

e = Employee("Harry")
print(str(e))
print(repr(e))
# print(e.name)
# print(len(e))
e()