# Wed Jul 26 09:17:56 AM IST 2023
# CWH 82
# Code With Harry's Solution to Merge the PDF Exercise

from pypdf import PdfWriter
import os

merger = PdfWriter()
files = [file for file in os.listdir() if file.endswith(".pdf")]

for pdf in files:
    merger.append(pdf)

merger.write("merged-pdf.pdf")
merger.close()