# Wed May 17 08:32:30 AM IST 2023
# CWH 28
# F-strings in Python
# this is a relatively new feature in python

# F-string is used for string formatting 

letter = "Hey my name is {} and I am from {}"
country = "India"
name = "Armaan"
print(letter.format(name,country))

letter = "Hey my name is {1} and I am from {0}"
print(letter.format(name,country))

# F-strings come to rescue now

print(f"Hey my name is {name} and I am from {country}") 
# f""  is automatically recognised as fstring

txt = "For only a low low price of {price:.2f} dollars!"
print(txt.format(price=49.98689))

# if you literally want to to print curly brackets in f strings
# then you have to put them inside double brackets
print(f"Hey my name is {{name}} and I am from {{country}}")