# print("Hey I am a good boy and this
# viewer is also a good boy/girl")

#as you can see the above code gives error
#we can mitigate that by using escape sequence char '\n'

print ("Hey I am a good boy and this\nviewer is also a good boy/girl")



#about comments
#this is a single line comment

# for multiline comments you can either use
# '#' symbol for each new line
# or you can use the multiline string


''' this is a 
multi line string and this is ignored by the compiler
as you can see this can also be used to write multi line comments'''


""" this also works as multi line string
as you can see that you can use triple quotes 
and they work same as ''' """
 
#what if you literally want to print a new line character which is an escape sequence?
#in that case you can escape the escape sequence by using a backslash

#for example 
# I want to print this exactly "hello this is the newline character \n I am armaan"
print ("hello this is the newline character \n I am armaan")
#as you can understand, the above code will not print what we want to print
# so we will use backslash to escape the escape sequence
print ("hello this is the newline character \\n I am armaan")

#as you can see that it literally prints the newline character


#how about printing the double quote literally? as you know
# that the double quote is used to define the start and end of string
# so how do we print it?
# we can either use '' to enclose the string to print double quote
# or we can use \"

# print (" I want to print double quotes "hey this is armaan" ")# this line will throw an error 
print (' I want to print double quotes "hey this is armaan" ')
print (" I want to print double quotes \"hey this is armaan\" ")
# as you can see we did the job of literally printing double quotes
# we can do something similar for single quote 




#ABOUT THE PRINT STATEMENT

# print can handle multiple arguments
print("Hey", 6,7,sep="~", end ="\n")
print("armaan")
# sep is what comes between two arguments
# end is what comes after all arguments are done printing
# so the output will be something like:
# Hey~6~7
# armaan


#default sep=" " (single space)
#default end="\n" (new line)

# there is one more parameter of the print function that is "file" we will not discuss about that now

#these parameters are optional 


