# Thu Jul  6 10:06:33 AM IST 2023
# CWH 64
# Exercise : Library Management program

class Library:
    def __init__(self,libraryName):
        self.name=libraryName
        self.books=[]
        self.numberOfBooks=0
    def addBook(self,nameOfBook:str):
        self.books.append(nameOfBook)
        self.numberOfBooks+=1
    def removeLastBook(self):
        self.books.pop()
        self.numberOfBooks-=1
    def printInfo(self):
        print("Name of Library:",self.name)
        print("Number of Books:",self.numberOfBooks)
        print("List of Books:",self.books)

library1=Library("Universal Library")
library2=Library("Kanpur Central Library")

library1.addBook("The Tempest")
library1.addBook("Merchant of Venice")
library1.addBook("Nootan Physics 12th")
library1.addBook("Mein Kamph")

library1.printInfo()


# // Answer to Harry's Question: No the books are not stored when the program halts
# for that we'd need to implement file handling so we can read book list from file
