# Fri Jul 28 02:22:30 PM IST 2023
# CWH 90
# Exercise : news API

#my api key 129003830dc34c598b32ba1a2ef6b403

#https://newsapi.org/v2/everything?q=tesla&from=2023-06-28&sortBy=publishedAt&apiKey=129003830dc34c598b32ba1a2ef6b403
# so what I am understanding is that we need to place the keyword 
# in front of ?q=

import requests 
import json
query=input("what type of news are you interested in?\n")
url = f"https://newsapi.org/v2/everything?q={query}&from=2023-06-28&sortBy=publishedAt&apiKey=129003830dc34c598b32ba1a2ef6b403"
r = requests.get(url)

# print(r.text)
news = json.loads(r.text)
# print(news)
for article in news["articles"]:
    print(article["title"])
    print("----------------------------------")
# for article in news["articles"]:
#     print(article)