# Mon May 22 08:56:35 AM IST 2023
# CWH 31
# Sets in Python

#Sets ensure that the items added are unique

s={2,4,2,6}
print(s)
print(type(s))
# Set is a collection of well defined objects
# Sets do not promise order
# anything can be at any index
# Sets cannot be changed once created

# quick quiz : make an empty set
harry={}
print(harry)
print(type(harry))
# ohhh, as you can see, it makes an empty dictionary, what do we do now?
har=set() # that's how you do it
print(har)
print(type(har))


# how to access items of a set? using a for loop 
colors = {"red", "green", "blue", "indigo","yellow", "red","pink"}
for color in colors:
    print(color)

