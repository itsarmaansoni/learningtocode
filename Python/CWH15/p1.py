# Exercise number 2 : Good morning sir
# 28 April 2023
# CWH15


# time module

import time


timestamp=time.strftime('%H %M %S')
#print(timestamp)


# the question of the exercise is to make a bot that greets you according to the time of the day
# HERE IS MY SOLUTION TO THE EXERCISE!
# for example, it will say good morning when it's morning and so on
# you have to use good morning
# good afternoon 
# and good evening

tsu=int(time.strftime('%H'))
#print(tsu)
# okay soo now we have the hour
# morning - 5 AM to 12 PM
# afternoon - 12 PM to 5 PM
# evening - 5 Pm to 12 AM
# night - 12AM to 5 AM 



# what will be the value in %H 
# morning 5 to 12
# afternoon 12 to 17
# evening 17 to 23
# night 23 to 4

#print(tsu)

# well anything above 5 is automatically morning right...
if (tsu>=5 and tsu<=11):
    print("Good morning Armaan!")
elif (tsu>=12 and tsu<=16):
    print("Good afternoon Armaan!")
elif (tsu>=17 and tsu<=23):
    print("Good evening Armaan!")
else:
    print("Good Night Armaan!")





