# type casting in python
# 20 april, 2023
# cwh9


# we use typecasting method for each type
# for int we use int()
# for float we use float()

a = "1"
b = "2"
print(a + b) # as you can see that a and b are strings
# but we want to add them as integers
# we can typecast them

print(int(a) + int(b))

#that's all there is to typecasting

