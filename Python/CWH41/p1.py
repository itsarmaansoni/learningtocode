# Mon Jun  5 08:22:41 AM IST 2023
# CWH 41
# if-else shorthand python


a = 330
b = 3303
print("A") if a>b else print("=") if a == b else print("B") 


c = 330 if a>b else 230
print(c)