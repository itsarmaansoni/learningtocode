#learning about operators in python
#18 april, 2023
#cwh7


#let's learn about some operators in python
print(5+6)
print(15-6)
print(15*6)
print(23/5)
print(23//5) # the floor division operator, this only tells you the quotient 
print(23%5) # modulo operator, tells you the remainder
print(4**2) # exponent operator, so this basically 4 squared

