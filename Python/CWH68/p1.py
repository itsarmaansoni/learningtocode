# Wed Jul 12 09:33:57 AM IST 2023
# CWH 68
# Exercise : Clean the Clutter 

#question attached in screenshot

#here is my solution
import os
import random
import string
def directoryClutterer(directoryPath:str,extention:str,numberOfFiles:int):
    '''takes in a directory path and a file extention(e.g. ".png") and numberOfFiles and clutters the directory 
    with given number of files that are randomly named with that extention'''
    if(os.path.isdir(directoryPath)):
        # print(os.getcwd())
        os.chdir(directoryPath)
        for i in range(numberOfFiles):
            f = open(getRandName()+extention, "x")
def directoryCleaner(directoryPath:str,extention:str):
    '''takes in a directory path and a file extention(e.g. ".png") and renames all the files of 
    that extension in that directory so it makes a natural number sequence'''
    if(os.path.isdir(directoryPath)):
        # print(os.getcwd())
        os.chdir(directoryPath)
        fileList = os.listdir(os.getcwd())
        counter=0
        for file in fileList:
            if(file.endswith(extention)):
                counter+=1
                os.rename(file,str(counter)+extention)
                # print(file)
def directoryDebloater(directoryPath:str,extention:str):
    '''deletes the files of the given extention in the given directory'''
    if(os.path.isdir(directoryPath)):
        # print(os.getcwd())
        os.chdir(directoryPath)
        fileList = os.listdir(os.getcwd())
        for file in fileList:
            if(file.endswith(extention)):
                os.remove(file)
        # print(os.getcwd())
def getRandName():
    randName=''
    randLen=random.randint(5,11)
    for i in range(randLen):
        randName=randName+(random.choice(string.ascii_letters+string.digits))
    # print(randLen)
    # print(randName)
    return randName
# getRandName()
# directoryClutterer("/home/life/Documents/learningtocode/Python/CWH68/",".pdf",6)
# directoryClutterer("/home/life/Documents/learningtocode/Python/CWH68/",".gif",6)
# directoryCleaner("/home/life/Documents/learningtocode/Python/CWH68/",".gif")
# directoryDebloater("/home/life/Documents/learningtocode/Python/CWH68/",".pdf")


#NOTE: these are very powerful functions and can cause system wreckage, please use carefully
