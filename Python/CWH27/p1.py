# Tue May 16 08:07:27 AM IST 2023
# CWH 27
# exercise 3: kaun banega crorepati 
# here is my solution!
questions=["what is the color of an Apple?"
           ,"which animal has the longest neck among these?"
           ,"In which city is the Taj Mahal?"
           ,"which bird breed is the smallest in size?",
           "which is the largest sea creature?",
           "how many teeth does a healhty adult human have?"]
options_list=[["blue","green","red","orange"],
        ["giraffe","elephant","cow","horse"],
        ["Kolkata","Agra","Kanpur","Chennai"],
        ["Humming Bird","Sparrow","Ostrich","crow"],
        ["Blue Whale","Octopus","Sea Horse","Dolphin"],
        ["45","34","33","32"]]
answerkey=[2,0,1,0,0,3]
prize_money=[1000,5000,20000,50000,100000,300000]
c=0
moneywon=0
for question in questions:
    print()
    print("---------------------------------------")
    print("prize money for this question:₹"+str(prize_money[c])+"/-")
    print("question "+str(c+1)+")",question)
    op=1
    for options in options_list[c]:
        print(str(op)+")",options)
        op+=1
    ans=int(input("Enter your choice 1-4:"))
    if ((ans-1)==answerkey[c] and ans>=1 and ans<=4):
        print()
        print("Correct Answer!")
        moneywon+=prize_money[c]
    else :
        print("Wrong Answer!")
        break
    c+=1
print("you won a total sum of ₹"+str(moneywon)+"/-" )