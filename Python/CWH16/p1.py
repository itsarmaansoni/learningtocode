# Mon 1st May, 2023
# CWH16
# Match cases


# The match case statement allows users 
# to implement code snippets exactly to 
# switch cases. It can be used to pass the 
# variable to compare and the case statements 
# can be used to define the commands to 
# execute when one case matches.

command = 'Hello, World!'
match command:
    case 'Hello, World!':
        print('Hello to you too!')
    case 'Goodbye, World!':
        print('See you later')
    case other:
        print('No match found')

