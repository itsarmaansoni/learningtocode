# Thu Jul 27 09:13:05 AM IST 2023
# CWH 86
# Walrus Operator (New addition to Python, since 3.8)

# walrus operator :=

# new to Python 3.8
# assignment expression aka walrus operator
# assigns values to variables as part of a larger expression

# happy = False
# print(happy)

# print(happy := True)

# foods = list()
# while True:
#   food = input("What food do you like?: ")
#   if food == "quit":
#       break
#   foods.append(food)
foods = list()
while (food := input("What food do you like?: ")) != "quit":
    foods.append(food)
print(foods)