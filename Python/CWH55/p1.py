# Fri Jun 23 08:39:23 AM IST 2023
# CWH 55
# Exercise: Snake Water Gun

# Snake, Water and Gun is a variation of the children's game
# "rock-paper-scissors" where players use hand gestures to represent
# a snake, water, or a gun. The gun beats the snake, the water beats 
# the gun, and the snake beats the water. Write a python program to create 
# a Snake Water Gun game in Python using if-else statements. Do not create 
# any fancy GUI. Use proper functions to check for win.

# Here is my SOLUTION!

# import random
# cho = ("snake","water","gun")

# print("Select Gun, Water or Snake")
# for i in range(3):
#     choiceUser="inv"
#     rawChoiceUser = input("User:")
#     if (rawChoiceUser[0].lower=='s'):
#         choiceUser="snake"
#     elif (rawChoiceUser[0].lower=='w'):
#         choiceUser="water"
#     elif (rawChoiceUser[0].lower=='g'):
#         choiceUser="gun"
#     choiceBot = random.choice(cho)
#     print("Bot:"+choiceBot)
#     if (choiceBot==choiceUser):
#         print("TIE!")
#     else:
#         if (choiceUser=="snake"):
#             if (choiceBot=="water"):
#                 print("USER WON!")
#             elif(choiceBot=="gun"):
#                 print("BOT WON!")

#         elif (choiceUser=="water"):
#             if (choiceBot=="snake"):
#                 print("BOT WON!")
#             elif(choiceBot=="gun"):
#                 print("USER WON!")
#         elif (choiceUser=="gun"):
#             if (choiceBot=="snake"):
#                 print("USER WON!")
#             elif(choiceBot=="water"):
#                 print("BOT WON!")

# that was not only too long, it didn't work
# so I am trying again

#winlose matrix/double dimentional list
#     User  S W G
#       B S 0 1 2
#       O W 2 0 1
#       T G 2 1 0
import random
winlose =           [[0,1,2],
                     [2,0,1],
                     [2,1,0],]
for i in range(3):
    choiceBot = random.choice((0,1,2))
    choiceUser = int(input("Enter 0 for snake 1 for water and 2 for gun:"))
    print("Bot:"+str(choiceBot))
    if(winlose[choiceUser][choiceBot] == 0):
        print("TIE!")
    elif(winlose[choiceUser][choiceBot] == 1):
        print("USER WON!")
    elif(winlose[choiceUser][choiceBot] == 2):
        print("BOT WON!")

#I don't think it works, I am too lazy to rewrite it
# I know I can solve this problem, this is nothing
# I just want to eat my breakfast now