# Wed May 24 09:43:51 AM IST 2023
# CWH 33
# Dictionaries in Python
info = {'name':'Karan', 'age':19, 'eligible':True}
print(info)
print(info['name'])
print(info.get('eligible'))
print(info.values())
print(info.keys())
print(info.items())

print(info.items())
for key, value in info.items():
  print(f"The value corresponding to the key {key} is {value}") 
