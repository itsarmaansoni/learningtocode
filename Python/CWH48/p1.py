# Wed Jun 14 08:26:46 AM IST 2023
# CWH 48
# local and global variables in Python
x = 10  # global variable


def my_function():
  global x
  x = 5  # this will change the value of the global variable x
  y = 5  # local variable


my_function()
print(x)  # prints 5
# print(y) # this will cause an error because y is a local variable and is not accessible outside of the function