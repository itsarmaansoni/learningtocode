# Mon Jul 31 09:50:05 AM IST 2023
# CWH 93
# Code with Harry's Solution News API problem
import requests
import json

query = input("What type of news are you interested in? ")
url = f"https://newsapi.org/v2/everything?q={query}&from=2023-06-30&sortBy=publishedAt&apiKey=129003830dc34c598b32ba1a2ef6b403"
r = requests.get(url)
news = json.loads(r.text)
# print(news, type(news))
for article in news["articles"]:
  print(article["title"])
  print(article["description"])
  print("--------------------------------------")