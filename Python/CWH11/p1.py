# Strings in python
# 24 april, 2023
# cwh11


# anything inside double quotes in python is a string
# or anything inside single quotes
name = "harry"
friend = "Rohan"
anotherFriend = 'Lovish'
hereisamultilinestring = ''' here is a multiline 
string'''
hereisonemoremultilinestring = """ here is one more multiline string 
hshshhshsh lkjlklkjsdf 
lkjsdflkjsdf """ 

print(name)
print(friend)
print(anotherFriend)
print(hereisamultilinestring)
print(hereisonemoremultilinestring)


print("Hello",name)
print("Hello"+name)

print(name[0]) # prints the zeroth character of the string name, string is an iterable object in python
# given that they are iterable object
# we can use for loops on em 

for character in name:
    print(character)
#as you can see that it prints each chracter of the string seperately
