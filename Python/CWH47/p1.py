# Tue Jun 13 08:49:08 AM IST 2023
# CWH 47
# Code with harry's solution to secret message exercise
st = input("Enter message")
words = st.split(" ")
coding = input("1 for Coding or 0 for Decoding")
coding = True if (coding=="1") else False
print(coding)
if(coding):
  nwords = []
  for word in words:
    if(len(word)>=3):
      r1 = "dsf"
      r2 = "jkr"
      stnew = r1+ word[1:] + word[0] + r2
      nwords.append(stnew)
    else:
      nwords.append(word[::-1])
  print(" ".join(nwords))

else:
  nwords = []
  for word in words:
    if(len(word)>=3): 
      stnew = word[3:-3]
      stnew = stnew[-1] + stnew[:-1]
      nwords.append(stnew)
    else:
      nwords.append(word[::-1])
  print(" ".join(nwords))
  
  
# what I can learn from this is how small the code is, my code is very huge
# Harry used a lot of string and list functions to make the job easier,
# while I tried to write a lot of things from scratch because I didn't know it
# was possible to do something with a built in function
# this is good code


# but this has a very bad CLI interface, it looks awful to operate
# while my code has a clean CLI
# also, this is prone to errors,
# a lot of out of bound errors, in my code those are handled but not here
