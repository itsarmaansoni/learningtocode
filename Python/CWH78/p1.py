# Mon Jul 24 09:23:03 AM IST 2023
# CWH 78
# Single Inheritance in Python

class Animal:
    def __init__(self, name, species):
        self.name = name
        self.species = species
        
    def make_sound(self):
        print("Sound made by the animal")

class Dog(Animal):
    def __init__(self, name, breed):
        Animal.__init__(self, name, species="Dog")
        self.breed = breed
        
    def make_sound(self):
        print("Bark!")

d = Dog("Dog", "Doggerman")
d.make_sound()

a = Animal("Dog", "Dog")
a.make_sound()

# Quick Quiz: Implement a Cat class by using the animal class. Add some methods specific to cat

class Cat(Animal):
    def __init__(self,name,breed):
        super().__init__(name,species="Cat")
        self.breed=breed
    def make_sound(self):
        print("meoww!!!")
    def play(self):
        print(f"{self.name} is playing!!")
        
