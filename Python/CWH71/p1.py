# Mon Jul 17 09:00:34 AM IST 2023
# CWH 71
# dir, __dict__ and help method in python
class Person:
  def __init__(self, name, age):
      self.name = name
      self.age = age
      self.version = 1

    
p = Person("John", 30)
print(p.__dict__)

print(help(Person))