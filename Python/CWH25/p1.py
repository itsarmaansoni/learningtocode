# Fri May 12 08:45:41 AM IST 2023
# CWH 25
# methods for Tuples

# well basically
# there are no methods for tuples
# you can't change a tuple
# you have to make a temporary list out of tuple
# change that list
# and copy the value of that list to a new tuple
# that's the only way
# the list() and tuple() fucntions help us convert tuple to list and vice versa
countries = ("India", "Russia", "Iran", "United States")
print(countries)
tempcountries= list(countries)
# tempcountries is now a list we can change 
tempcountries.append("China")
tempcountries.pop(2)
tempcountries.sort()
countries=tuple(tempcountries)
print(countries)

# there are ofcourse a few methods for tuples as well 
# but they don't change the tuple 

# for example count() method, which counts the occurance of an element in tuple 