// Fri Jun  2 08:42:19 AM IST 2023
// CWH 40
//  Multilevel Inheritance in C++

#include <iostream>

using namespace std;
class Student
{
private:
protected:
    int roll_number;

public:
    void set_roll_number(int);
    void get_roll_number(void);
};
void Student ::set_roll_number(int n)
{
    roll_number = n;
}
void Student ::get_roll_number()
{
    cout << "The roll number is :" << roll_number << endl;
}
class Exam : public Student
{
protected:
    float physics;
    float maths;

public:
    void set_marks(float, float);
    void get_marks(void);
};
void Exam ::set_marks(float p, float m)
{
    physics = p;
    maths = m;
}
void Exam ::get_marks(void)
{
    cout << "The marks in maths are :" << maths << endl;
    cout << "The marks in physics are :" << physics << endl;
}
class Results : public Exam
{
    float percentage;

public:
    void display()
    {
        get_roll_number();
        get_marks();
        cout << "Your percentage is :" << ((maths + physics) / 2) << "%" << endl;
    }
};
int main()
{
    // if we are inheriting B from A and C from B: [A---->B---->C]
    // 1.) A is the base class for B and B is the base class for C
    // 2.) A---->B---->C is called the inheritance path
    Results harry;
    harry.set_roll_number(34);
    harry.set_marks(23.3, 64.4);
    harry.display();
    return 0;
}