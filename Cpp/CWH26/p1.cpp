//Mon May 15 08:31:38 AM IST 2023
//CWH 26
// Friend Functions in C++
#include <iostream>

using namespace std;

class Complex
{
private:
    int a,b;
    // the line below means that sumComplex function is a friend function
    // and it is allowed to access private members
    friend Complex sumComplex(Complex cnum1,Complex cnum2);
public:
    void setNum(int n1, int n2){
        a=n1;
        b=n2;
    }
    void printNum(void){
        cout<<"The value of complex number is "<<a<<"+"<<b<<"i"<<endl;
    }
};
Complex sumComplex(Complex n1,Complex n2){
    Complex ret;
    ret.setNum((n1.a+n2.a),(n1.b+n2.b));
    return ret;
}
int main(){

    Complex c1,c2,sumComp;
    c1.setNum(5,8);
    c2.setNum(3,6);
    c1.printNum();
    c2.printNum();
    sumComp=sumComplex(c1,c2);
    sumComp.printNum();
    // friend functions allow us to access private data of objects if they are defined 
    // in the class of that object
    // note that friend functions aren't member functions
    // you cannot access them with class objects
    
    return 0;
}
/*
Properties of Friend functions:
1.)Not in the scope of the class
2.)Since it is not in the scope of the class it cannot be called from the object of that class
 c1.sumComplex is invalid
3.) can be invoked without the help of any object
4.) usually contains objects as arguments
5.) can be declared in either public or private section of the class
*/