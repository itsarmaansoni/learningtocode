//Wed May 31 08:06:24 PM IST 2023
//CWH 38
//Single Inheritance deep dive
#include <iostream>

using namespace std;

class Base{
    int data1;
    public:
        int data2;
        int getData1();
        int getData2();
        void setData();
};
void Base :: setData(void){
    data1 = 10;
    data2 = 20;
}
int Base :: getData1(){
    return data1;
}
int Base :: getData2(){
    return data2;
}
class Derived : public Base{
    int data3;
    public:
        void process();
        void display();
};
void Derived :: process(){
    data3= data2 * getData1();
}
void Derived :: display(){
    cout<<"Data 1 is equal to : "<<getData1()<<endl;
    cout<<"Data 2 is equal to : "<<data2<<endl;
    cout<<"Data 3 is equal to : "<<data3<<endl;
}
int main(){
    Derived der;
    der.setData();
    der.process();
    der.display();

    return 0;
}