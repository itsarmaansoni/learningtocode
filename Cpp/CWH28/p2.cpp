// Wed May 17 08:48:50 AM IST 2023
// CWH 28
//more on Friend functions, a little complex example
#include <iostream>

using namespace std;
class C2;
class C1
{
private:
    int Val;
public:
    void inData(int a){
        Val=a;
    }
    void display(){
        cout<<"The value is "<< Val << endl;
    }
    friend void swapVal(C1 * c1obj, C2 * c2obj);
};

class C2
{
private:
    int Val;
public:
    void inData(int a){
        Val=a;
    }
    void display(){
        cout<<"The value is "<< Val << endl;
    }
    friend void swapVal(C1 * c1obj, C2 * c2obj);
};
void swapVal(C1 * c1obj, C2 * c2obj){
    int* ptrc1;
    int* ptrc2;
    ptrc1 = &(*c1obj).Val;
    ptrc2 = &(*c2obj).Val;
    int temp = *ptrc1;
    *ptrc1=*ptrc2;
    *ptrc2=temp;
}
int main(){
    C1 ob1;
    C2 ob2;
    ob1.inData(8);
    ob1.display();
    ob2.inData(18);
    ob2.display();

    swapVal(&ob1,&ob2);

    ob1.display();
    ob2.display();

    //main code

    return 0;
}
// ookay so there is a problem, 
// I watched harry's tutorial and he used reference variables as the 
// parameters for the swapVal funtion, but I used pointers,
// the problem is, when I tried to use reference variables, it didn't work,
// but when I rewrote the logic with pointer variables, it worked
// so what I am gonna do is write one more program and make it using reference variables
// just to be sure