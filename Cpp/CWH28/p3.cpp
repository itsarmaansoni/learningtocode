// Wed May 17 09:10:55 AM IST 2023
// CWH 28
// more on friend fucntions, trying with reference variable
#include <iostream>

using namespace std;
class C2;
class C1
{
private:
    int Val;
public:
    void inData(int a){
        Val=a;
    }
    void display(){
        cout<<"The value is "<< Val << endl;
    }
    friend void swapVal(C1 & a, C2 & b);
};

class C2
{
private:
    int Val;
public:
    void inData(int a){
        Val=a;
    }
    void display(){
        cout<<"The value is "<< Val << endl;
    }
    friend void swapVal(C1 & a, C2 & b);
};
void swapVal(C1 & a, C2 & b){
    int temp=a.Val;
    a.Val=b.Val;
    b.Val=temp;
}
int main(){
    C1 ob1;
    C2 ob2;
    ob1.inData(8);
    ob1.display();
    ob2.inData(18);
    ob2.display();

    swapVal(ob1,ob2);

    ob1.display();
    ob2.display();

    //main code

    return 0;
}

// can't believe it actually worked first try, 
// I must be doing something really stupid in the previous program