//Wed May 17 08:32:30 AM IST 2023
//CWH 28
// more on Friend functions
#include <iostream>

using namespace std;
class Y;
class X
{
private:
    int data;
public:
    void setValue(int value){
        data = value;
    }
    friend void add (X o1, Y o2);
};
class Y
{
private:
    int data;
public:
    void setValue(int value){
        data = value;
    }
    friend void add (X o1, Y o2);
};
void add( X o1, Y o2){
    cout<<"Summing data of X and Y gives "<< (o1.data + o2.data)<<endl;
}
int main(){

    X o1;
    Y o2;
    o1.setValue(4);
    o2.setValue(6);
    add(o1,o2);

    return 0;
}