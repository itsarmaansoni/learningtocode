// Mon Jun 19 08:47:02 AM IST 2023
// CWH 51
// Pointers to Objects and Arrow Operators
#include <iostream>
using namespace std;

class Complex
{
    int real, imaginary;

public:
    void getData()
    {
        cout << "The real part is " << real << endl;
        cout << "The imaginary part is " << imaginary << endl;
    }

    void setData(int a, int b)
    {
        real = a;
        imaginary = b;
    }
};
int main()
{
    Complex *ptr = new Complex;
    (*ptr).setData(1,54);
    (*ptr).getData();

    //arrow operator
    ptr->setData(1, 54);
    ptr->getData();

    // Array of Objects
    Complex *ptr1 = new Complex[3];
    ptr1->setData(1, 4);
    ptr1->getData();
    (ptr1+1)->setData(1,5);
    (ptr1+1)->getData();
    (ptr1+2)->setData(2,9);
    (ptr1+2)->getData();
    return 0;
}
