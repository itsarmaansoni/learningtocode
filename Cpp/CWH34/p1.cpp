// Thu May 25 08:14:16 AM IST 2023
// CWH 34
//  Copy Constructor in C++
#include <iostream>

using namespace std;

class Number
{
private:
    int a;

public:
    Number(){
        a=0;
    }
    Number(int num)
    {
        a = num;
    }
    Number (Number &obj){ // this is a copy constructor
        a = obj.a;
        cout<<"Copy Constructor invoked"<<endl;
    }
    void display()
    {
        cout << "The number is :" << a << endl;
    }
};
int main()
{

    Number x, y, z(45);
    x.display();
    y.display();
    z.display();

    // Copy Constructor is a type of constructor that makes the copy of another object

    Number z1(z); // Copy constructor invoked
    z1.display();
    // even if you remove your copy constructor, copying constructor will work
    // because C++ compiler gives a copy constructor to all classes by default
    // THIS IS VERY IMPORTANT!
    Number z2;
    z2=z; // Copy Constructor will NOT be invoked
    z2.display();
    Number z3 = z; // Copy Constructor will be invoked
    z3.display();


    return 0;
}