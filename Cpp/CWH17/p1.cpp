//Tue May  2 08:03:35 AM IST 2023
//CWH 17
//Inline functions, Default Arguments & Constant Arguments in C++

#include <iostream>

using namespace std;

int product(int a , int b ){
	return a * b;
}
inline int productIN(int a , int b ){
	return a * b;
}
int dummyfunctiontodemonstratedefaultarguments(int a, int b=234){
	return 0;
} // in this functions b is a default argument, if not passed it will consider the value 234 as default
// default arguments must always be placed at the end of function declaration

int dummyfunctiontodemonstrateconstarguments(int a, const int p){
	return 0;
} // the value of const int p cannot be changed throughout the program after it is passed to the function,
// which means it is a constant argument
int main(){
	int c,d;
	cout<<"Enter the value of C and D:";
	cin>>c>>d;
	cout<<"The product of C and D is "<<product(c,d)<<endl;
	cout<<"The product of C and D using inline function is "<<productIN(c,d)<<endl;
	// what inline function is basically doing is putting c*d in place of productIN(c,d)
	// which makes it more efficient than a normal function because, in a normal function,
	// actual parameters are passed, then they are copied to formal parameters, operation is performed
	// value is returned, the cout uses it's value, but we could have saved all this 
	// by just typing c*d, that's what inline function does, they are efficient, and their name perfectly
	// describes them
	// remember to only use inline function when function code is small enough that replacing 
	// the function call with contents of the code is actually helpful and not counter productive
	// don't use inline functions for recursive functions,
	// it's generally not recommended to use inline functions if they have
	// recursion, loops, switch case or static variables
	

	// arguments are explained using fucntions above main function


	return 0;
}

