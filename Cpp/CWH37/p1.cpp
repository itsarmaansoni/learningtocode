//Tue May 30 08:38:34 AM IST 2023
//CWH 37
//Inheritance syntax and Visibility mode in C++

#include <iostream>

using namespace std;

class Employee // Base Class
{
private:
public:
    int id;
    float salary;
    Employee(int inpId){
        id = inpId;
        salary = 34.0;
    }
    Employee(){
        salary = 34.0;
    }
};

// Derived class syntax
// class {{derived-class-name}} : visibility mode {{base-class-name}} {
// members/methods/etc..
// }


// creating a programmer class derived from Employee base class

class Programmer : public Employee {

// as you can see that we didn't define the visibility mode
// it is private by default
// this is equivalent to class Programmer : private Employee
// there are two visibility modes, public and private
// public visibility mode : Public members of the base class becomes public members of the derived class 
// private visiblity mode : public members of the base class becomes private members of the derived class
// what about private members??? THEY CANNOT BE INHERITED!!!!!
    public:
        int languageCode = 9;
        Programmer(int inpId){
            id = inpId;
        }
        void getData(){
            cout<<id<<endl;
        }
};
int main(){

    Employee harry(1),rohan(2);
    cout<<harry.salary<<endl;
    cout<<rohan.salary<<endl;
    Programmer akash(5);
    cout<<akash.languageCode<<endl;
    akash.getData();
    cout<<akash.salary<<endl; 
    // only works if you inherit with public visibility mode 


    //whenever you make a derived class the default constructor of the base class
    // is automatically called, so make sure to define the default constructor
    return 0;
}