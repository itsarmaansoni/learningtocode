// Structures, Unions and Enums in C++
// 27th April, 2023
// CWH14


#include <iostream>

using namespace std;

struct employee{
	int eID;
	char favChar;
	float salary;
}; // Structure is a user defined data type which can hold different types of datatypes

typedef struct student{
	int age;
	int id;
	char section;
	float marks;
} st; // we can also typedef a structure, what it does is that it allows us to make struct variables without
// typing out the entire thing 'struct student harry' can now be written as 'st harry'


union money{
	int rice;
	char car;
	float pounds;
}; // unions are like structures but they are more memory efficient, you can only use one of the datatypes 
// that are defined within it, while structures can store all the datatypes at once and we can access them 
// without any problems, all the data types within a union share memory, and that memory is equal to the biggest
// data type within that union
//
// a structure with one char one int and one float will reserve this much bytes = 4 + 4 + 1 = 9
// but a union with one char one int and one float will reserve this much bytes = 4 = 4 // because 4 is the 
// largest data type in that union 
int main(){
	struct employee harry;
	st shubham;
	shubham.age=24;
	harry.eID=43;
	harry.favChar='u';
	harry.salary=23234.54;
	cout<<"employee ID of Harry is "<<harry.eID<<endl;
	cout<<"Harry's favorite character is "<<harry.favChar<<endl;
	cout<<"Harry's salary is "<<harry.salary<<endl;
	cout<<"Shubham's age is "<<shubham.age<<endl;
	// we can also make other employees using the structure above
	struct employee Rohan;
	struct employee Armaan;
	// you get the point
	// 
	//
	//
	// demonstrating how unions work;
	union money mon1;
	mon1.rice=34;
	mon1.car='c'; // this overrides the previous value
	// now if we try to access the value of m1.rice we will get a garbage value
	cout<<"mon1.rice is "<<mon1.rice<<endl;
	cout<<"mon1.car is "<<mon1.car<<endl; //we will retrieve the correct value foor m1.car because it was 
	// stored later
	

	//enums
	// enumeration data types
	enum Meal {breakfast,lunch,dinner};
	Meal m1= breakfast;
	Meal m34= lunch;
	cout<<"meal m1 is "<<m1<<endl;
	cout<<"meal m34 is "<< m34<<endl;
	return 0;
}
