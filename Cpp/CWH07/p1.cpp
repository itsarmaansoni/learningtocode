// Reference Variables & type casting in C++
// date : 18th april
// CWH7
#include <iostream>
using namespace std;
int c=45; //global variable c
int main(){
	int a, b, c; //local variable c
	cout<<"Enter the value of a :";
	cin>>a;
	cout<<"Enter the value of b :";
	cin>>b;
	c=a + b; //this initializes the local variable c
	cout<<"The sum of a and b is:"<<c<<endl; // this prints the local variable c
	// but what if we want to use the global variable c?
	// we can use the "scope resolution operator"
	// scope resolution operator "::"
	cout<<"the global c is "<<::c<<endl;
	float d=34.4;
	long double e =34.4;
	cout<<"The value of d is "<<d<<endl<<"The value of e is "<<e<<endl;
	//one important thing to note is that 34.4 by itself is a double, if you pass the value 34.4 in any function it will be taken as a double
	//to use this value as a float you will have to specify by adding an 'f' that is 34.4f is now a float value
	// we could have used 
	//
	//
	//
	



	//REFERENCE VARIABLE
	//when we want two variables to point to one value we use reference variables
	float vari =2343.234;
	cout<<vari<<endl;
	float & y = vari; // which means y is a reference variable and is pointing to vari
	cout<<y<<endl; // prints the value of vari



	//type casting
	//changing one type of variable to another type
	//we do it with type cast operator (type_that_you_want)variable;
	int ad = 45;
	cout<<"The value of a is "<<(float)ad;
	float bd= 234.23434;
	cout<<"The value of bd is"<<(int)bd;

	return 0;
}
