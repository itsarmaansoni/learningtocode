// Fri May 12 08:45:41 AM IST 2023
// CWH 25
//  Array of Objects 
#include <iostream>

using namespace std;

class Employee
{
private:
    int id;
    int salary;

public:
    void setId(void)
    {
        cout << "Enter the Id of Employee:";
        cin >> id;
        salary = 122;
    }
    void getId(void)
    {
        cout << "The employee Id is:" << id << endl;
    }
};
int main()
{

    // Employee Harry,Rohan,Lovish,Shruti;
    // Harry.setId();
    // Harry.getId();
    // But what if you have a lot of Employees?
    // you can make an employee array!
    Employee fb[5];
    for (int i = 0; i < 5; i++)
    {
        fb[i].setId();
        fb[i].getId();
    }

    return 0;
}