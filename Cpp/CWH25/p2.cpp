// Fri May 12 08:45:41 AM IST 2023
// CWH 25
//using objects as function parameters
#include <iostream>

using namespace std;

class complex
{
private:
    int a,b;
public:
    void setData(int v1,int v2){
        a = v1;
        b = v2;
    }
    void setDataBySum(complex o1, complex o2){
        a = o1.a + o2.a;
        b = o1.b + o2.b;
    }
    void getData(){
        cout<<"The value of complex number is :"<<a<<" + "<<b<<"i"<<endl;
    }
};
int main(){

    complex c1,c2,c3;
    c1.setData(3,5);
    c2.setData(5,7);
    c3.setDataBySum(c1,c2);
    c3.getData();
    return 0;
}