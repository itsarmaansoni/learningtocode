//Tue May 23 08:09:12 AM IST 2023
//CWH 32
// Constructors with default arguments in C++
#include <iostream>

using namespace std;

class Simple
{
private:
    int data1;
    int data2;
public:
    Simple(int a, int b=9){
        data1=a;
        data2=b;
    }
    void printData(){
        cout<<"The Value of data1 is "<<data1<<endl;
        cout<<"The Value of data2 is "<<data2<<endl;
    }
};
int main(){

    Simple obj(1,4);
    obj.printData();
    Simple obj2(1);
    obj2.printData();

    return 0;
}