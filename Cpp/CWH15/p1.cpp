// functions and function prototypes in C++
// 28th April, 2023
// CWH15

#include <iostream>
using namespace std;

int sum(int a, int b){ // function definition
	int c=a+b;
	return c;
	}
// function prototype / function declaration
//type function-name (arguments);
int sub(int a, int b); // this is neccessary if you want to use the function before you have defined it

int main(){
	int num1,num2;
	cout<<"Enter num 1:";
	cin>>num1;
	cout<<"Enter num 2:";
	cin>>num2;
	cout<<"The sum is "<<sum(num1,num2)<<endl; // num1 and num2 are actual parameters
	return 0;
}
int sub(int a, int b){ // function definition
	// a and b are formal parameters
	int c=a-b;
	return c;
}

