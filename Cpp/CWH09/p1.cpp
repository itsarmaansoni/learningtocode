
// if-else,switch case  and control structures in C++
// 20th April, 2023
// CWH9


//control structures are used to give code a flow
//types of control structures
//1.) sequence structure
//2.) selection structure
//3.) loop structure
//
//
//sequence structure: entry in a program > perform action 1 > perform action 2 > perform action 3 >..> exit
//
//
//selection structure: in a selection structure there is a condition in the beginning, if that
//condition is fulfilled then the structure goes to the truth condition else it goes to the false condition
//and exits
//
//
//Loop structure : you have a loop condition, if it's met then the loop body executes, otherwise we exit 
//from the loop
//
//
// ways to model these control structures in a C++ program
// 1.) if-else
// 2.) nested if-else
// 3.) switch case
//


#include <iostream>

using namespace std;

int main(){
//	cout<<"This is tutorial 9";	
	int age;
	cout<<"Enter your age:";
	cin>>age;
	if (age<18){
		cout<<"You cannot come to my party"<<endl;
	}
	else if (age == 18){
		cout<<"You are a kid and you will get a kid pass to the party"<<endl;
	}
	else {
		cout<<"You can come to the party"<<endl;
	}

	return 0;
}




// learn about switch case in p2.cpp







