
// if-else,switch case  and control structures in C++
// 20th April, 2023
// CWH9


//switch case 

#include <iostream>


using namespace std;


int main(){
	int age;
	cout<<"Enter your age:";
	cin>>age;
	switch (age){
		case 18:
			cout<<"You are 18"<<endl;
			break;    // using the break; statement is very important otherwise 
		//	our program will jump to other cases and not exit till another break; 
		//	and execute all case blocks
		case 22:
			cout<<"You are 22"<<endl;
			break;
		case 2:
			cout<<"You are 2"<<endl;
			break;

		default:
			cout<<"No special cases";
			break;
	}
	return 0;
}


