//call by value and call by reference in Cpp
//1st May 2023
//CWH 16

#include <iostream>

using namespace std;

int sum (int a, int b){
	int c = a+b;	
	return c;
}
void swap (int a, int b){
	int temp=a;
	a=b;
	b=temp;
}
void actualswap (int* a, int* b){
	int temp=*a;
	*a=*b;
	*b=temp;
}
void referenceswap(int & a, int & b){
	int temp = a;
	a = b;
	b = temp;
}
int & referencereturn(int & d){
	return d;
} 
int main(){
	
	int a,b;
	a = 4;
	b = 5;
	cout<<"sum of a and b is "<<sum(a,b)<<endl;
	cout<<"The value of and a and b respectively before swap is "<<a<<" and "<<b<<endl;
	swap(a,b);
	cout<<"The value of and a and b respectively after swap is "<<a<<" and "<<b<<endl;
	// as you can see they didn't swap in the output, what is going wrong?
	// the problem is that we called this program by value, and not by reference
	// when we pass a and b in the swap function, those are actual arguments, and they get copied
	// to the formal arguments a and b of the swap function, the swaping takes place for
	// the formal arguments and not the actual arguments
	// now we will make an actualswap fucntion
	
	cout<<"The value of and a and b respectively before actualswap is "<<a<<" and "<<b<<endl;
	actualswap(&a,&b);
	cout<<"The value of and a and b respectively after actualswap is "<<a<<" and "<<b<<endl;
	// as you can see that actual swap works!
	// how does this work?
	// well, the function actualswap takes two int pointer arguments, for simplicity we will just call them
	// pointer arguments, those pointer arguments want an address, so when we called the function
	// actualswap, we gave them the address of both a and b, the address was now copied to the 
	// formal parameters i.e pointer variables of actualswap function, from there
	// the actual values of a and b were swapped by dereferencing the pointers provided, 
	// that's how the actualswap works.
	// actualswap function is an example of call by reference using pointer variables
	// and swap function was an example of call by value
	//
	// now we will see some C++ specific stuff
	// call by reference using reference variables in C++
	// demonstrated by referenceswap function
	

	cout<<endl;
	cout<<"The value of a and b respectively before reference swap is "<<a<<" and "<<b<<endl;
	referenceswap(a,b); // this is another way of call by reference by reference variables
	// reference variables are a Cpp specific thing, I guess
	cout<<"The value of a and b respectively after reference swap is "<<a<<" and "<<b<<endl;



	// there is one more concept to learn, return by reference
	// functions can also return references to variables, not only values but references
	// or addresses 
	int d = 7;
	referencereturn(d)=9;
	cout<<"the value of d is "<<d<<endl;
	// as you can see it changes the value of d, because it returned the reference to d

	return 0;
}
