//Fri Jun 16 08:56:31 AM IST 2023
//CWH 50
// Revisiting Pointers
#include <iostream>

using namespace std;


int main(){

    //basic Example
    int a = 45;
    int* ptr = &a;
    cout<<"The value of a is "<<*(ptr)<<endl;

    // new keyword (THIS IS A NEW CONCEPT)

    int* p = new int(50);
    // this makes a new pointer variable p and at the address of p it stores a new integer
    // value of 50, no need for pointing to an existing variable
    cout<<"The value at address p is "<<*(p)<<endl;

    // you can also make contigous blocks of memory 
    // aka arrays
    int* arr = new int[3];
    arr[0]=10;
    arr[1]=20;
    arr[2]=30;
    //delete keyword/operator
    // delete[] arr; // for a dynamically allocated array we use delete with []
    // for a pointer that is pointing to a single block we use delete ptr_name;
    cout<<"The value of arr[0] is "<<arr[0]<<endl;
    cout<<"The value of arr[1] is "<<arr[1]<<endl;
    cout<<"The value of arr[2] is "<<arr[2]<<endl;


    return 0;
}