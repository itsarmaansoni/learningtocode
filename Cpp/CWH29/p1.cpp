//Thu May 18 08:33:24 AM IST 2023
//CWH 29
// Constructors and distructors in C++
#include <iostream>

using namespace std;

class Complex
{
private:
    int a,b;
public:
    // creating a constructor
    // constructor is a special member function, with the same name as of the class
    // used to initialize the objects of its class
    // it is automatically invoked whenever an object is created
    Complex(void); // constructor declaration 
    void printData(void){
        cout<<"The Complex number is "<<a<<"+"<<b<<"i"<<endl;
    }
};

Complex :: Complex(void){ // this is a default constructor as it takes no parameter
    a=0;
    b=0;
}

int main(){

    Complex c;
    c.printData();

    return 0;
}
//characteristics of contructor
// 1.) it should be declared in the public section of the class
// 2.) they are automatically invoked whenever the object is created
// 3.) anytihng written inside the constructor is automatically run 
// 4.) have the same name as class
// 5.) they do not have return types, not even void
// 6.) they can be default or parameterised constructor
// 7.) we cannot refer to their addresses