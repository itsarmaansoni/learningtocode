// Mon May 22 08:56:35 AM IST 2023
// CWH 31
//  Constructor Overloading in C++
#include <iostream>

using namespace std;

class Complex
{
private:
    int a, b;

public:
    Complex()
    {
        a = 0;
        b = 0;
    } // this is the default contructor 
    Complex(int x, int y)
    {
        a = x;
        b = y;
    } // this is a parameterized contructor
    Complex(int x)
    {
        a = x;
        b = 0;
    } // this is a parameterized contructor
    
    void printData(void)
    {
        cout << "The Complex number is " << a << "+" << b << "i" << endl;
    }
};
int main()
{

    Complex c1(4, 7);
    c1.printData();
    Complex c2;
    c2.printData();
    Complex c3(3);
    c3.printData();
    return 0;
}