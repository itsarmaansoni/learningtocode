//Array and pointer arithmetic in C++
//25th April, 2023 (this was supposed to be done on 26th but I am feeling like studying)
//CWH13


// What is an array?
// An array is a collection of items of similar type
// stored in  contiguous memory locations
//
// sometimes a simple variable is not able to hold all the data 
//
// let's say that we have 2400 students, and we want to store their marks, making 2400 variables will not be feasible, that's where arrays come into the picture
// we can simply create an array of size 2400 
// 
//
// if a is an array, the first element is 0
// so to access it we might do something like a[0] 

// writing a program to demonstrate
// arrays

#include <iostream>
using namespace std;
int main(){
	int marks[4]={23,35,62,62}; // declaring an array of size 4 and also initializing it,
	// also, declaring the size is optional, the c++ compiler is smart
	// and it can figure out that the array should of length 4 even if you do
	// int marks[]={23,35,62,62} <--- this is valid, as you can see that I didn't define the length of array
	
	// you can also declare an array and assign the elements later
	int mathmarks[4];
	mathmarks[0]=32;
	mathmarks[1]=65;
	mathmarks[2]=78;
	mathmarks[3]=87;
	cout<<marks[0]<<endl;
	cout<<marks[1]<<endl;
	cout<<marks[2]<<endl;
	cout<<marks[3]<<endl;



	// you can also change the value of any element of the array
	mathmarks[0] = 34;



	return 0;
}


