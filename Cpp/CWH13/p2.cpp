//Array and pointer arithmetic in C++
//25th April, 2023 (this was supposed to be done on 26th but I am feeling like studying)
//CWH13


// Array and loops
//

#include <iostream>
using namespace std;
int main(){
	int marks[4];
	for (int i=0;i<4;i++){
		cout<<"Enter the element at index "<<i<<":";
		cin>>marks[i];
	}
	for (int i=0;i<4;i++){
		cout<<"The value at index "<<i<<" is "<<marks[i]<<endl;
	}



	return 0;
}
// There is an exercise from CWH, do the same stuff with while and do while loop, so I tried to solve in 
// solution.cpp
