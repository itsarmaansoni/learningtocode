//Array and pointer arithmetic in C++
//25th April, 2023 (this was supposed to be done on 26th but I am feeling like studying)
//CWH13

//arrays and pointer arithmetics
//how arrays and pointers tie


#include <iostream>
using namespace std;
int main(){
	int marks[4]={23,35,62,62};
	cout<<marks<<endl; // marks just stores the address of the first element in the array
	cout<<*marks<<endl; // when you dereference it, you get the value of first element in array
	cout<<*(marks+1)<<endl;// in pointer arithmetics when you add 1 to a pointer, it
	// gives the address just ahead of it, in this case it will the second element in the array, so when you
	// dereference it, you get the value of element 2
	//
	//
	//you can also store the marks pointer in another pointer variable as well
	int * ptr = marks;
	cout<<ptr<<endl;
	cout<<*ptr<<endl;
	cout<<*(ptr+1)<<endl;
	// all of this gives the same output as the three statements above these
	return 0;
}
