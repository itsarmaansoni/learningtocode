
//Array and pointer arithmetic in C++
//25th April, 2023 (this was supposed to be done on 26th but I am feeling like studying)
//CWH13


// solution to quick quiz from CWH


#include <iostream>
using namespace std;
int main(){
	int marks[4];
	int i=0;
	while (i<4){
		cout<<"Enter the value of element at index "<<i<<":";
		cin>>marks[i];
		i++;
	}
	i=0;
	while (i<4){
		cout<<"The value at index "<<i<<" is "<<marks[i]<<endl;
		i++;
	}
	int marks2[4];
	int i2=0;
	do {
		cout<<"Enter the value of element at index "<<i2<<":";
		cin>>marks2[i2];
		i2++;
	} while (i2<4);

	i2=0;
	do {
		cout<<"The value of index "<<i2<<" is "<<marks2[i2]<<endl;
		i2++;
	} while (i2<4);
	return 0;
}
