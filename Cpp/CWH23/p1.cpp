// Wed May 10 08:36:28 AM IST 2023
// CWH 23
// Objects Memory allocation
//  and using arrays in classes
#include <iostream>
using namespace std;
class Shop
{
    int itemId[100];
    int itemPrice[100];
    int counter;

public:
    void getPrice(void);
    void initCounter(void) { counter = 0; }
    void setPrice(void);
};
void Shop ::setPrice(void)
{
    cout << "Enter Id of your Item:";
    cin >> itemId[counter];
    cout << "Enter Price of your Item:";
    cin >> itemPrice[counter];
    counter++;
}
void Shop ::getPrice(void)
{
    for (int i = 0; i < counter; i++)
    {
        cout << "The Price of item with Id " << itemId[i] << " is " << itemPrice[i] << endl;
    }
}

int main()
{
    Shop dukaan;
    dukaan.initCounter();
    dukaan.setPrice();
    dukaan.setPrice();
    dukaan.setPrice();
    dukaan.setPrice();
    dukaan.getPrice();
    return 0;
}
