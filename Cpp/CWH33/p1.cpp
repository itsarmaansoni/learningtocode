// Wed May 24 09:43:51 AM IST 2023
// CWH 33
//  Dynamic Initialization of Constructors
#include <iostream>

using namespace std;

class BankDeposit
{
private:
    int principal;
    int years;
    double interestRate;
    double returnValue;

public:
    BankDeposit();
    BankDeposit(int p, int y, double r);
    BankDeposit(int p, int y, int r);
    void show();
};
BankDeposit :: BankDeposit(){

}

BankDeposit ::BankDeposit(int p, int y, double r)
{
    principal = p;
    years = y;
    interestRate = r;

    returnValue = principal;

    for (int i = 0; i < y; i++)
    {
        returnValue = returnValue * (1 + interestRate);
    }
}
BankDeposit ::BankDeposit(int p, int y, int r)
{
    principal = p;
    years = y;
    interestRate = double(r)/100;

    returnValue = principal;

    for (int i = 0; i < y; i++)
    {
        returnValue = returnValue * (1 + interestRate);
    }
}
void BankDeposit :: show(){
    cout<<"The principal amount was : "<<principal<<endl;
    cout<<"The duration was "<<years<<" years."<<endl;
    cout<<"The return is "<<returnValue<<endl;
}
int main()
{
    int p,y;
    double r;
    int R;
    cout<<"Enter the value of p y and r:";
    cin>>p>>y>>r;
    BankDeposit dep1(p,y,r);
    cout<<"Enter the value of p y and R:";
    cin>>p>>y>>R;
    BankDeposit dep2(p,y,R);
    // dep1(p,y,r);
    // dep2(p,y,R);
    dep1.show();
    dep2.show();
    BankDeposit dep3;
    dep3.show();
    return 0;
}