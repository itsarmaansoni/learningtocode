// Tue Jun  6 08:48:41 AM IST 2023
// CWH 42
// Exercise: based on Inheritance

// the question description is in the screenshot attached in the same folder

// Here is my solution!!
#include <iostream>
#include <math.h>
using namespace std;

class HybridCalculator
{
protected:
    float inp1, inp2;

public:
    void getInput()
    {
        cout << "Enter Inpur one:";
        cin >> inp1;
        cout << endl;
        cout << "Enter Inpur two:";
        cin >> inp2;
        cout << endl;
    }
};
class SimpleCalculator : public HybridCalculator
{
public:
    float add()
    {
        getInput();
        cout << "The result is :" << inp1 + inp2 << endl;
        return inp1 + inp2;
    }
    float sub()
    {
        getInput();
        cout << "The result is :" << inp1 - inp2 << endl;
        return inp1 - inp2;
    }
    float div()
    {
        getInput();
        cout << "The result is :" << inp1 / inp2 << endl;
        return inp1 / inp2;
    }
    float mul()
    {
        getInput();
        cout << "The result is :" << inp1 * inp2 << endl;
        return inp1 * inp2;
    }
};
class ScientificCalculator : public HybridCalculator
{
public:
    float exp()
    {
        getInput();
        cout << "The result is :" << pow(inp1, inp2) << endl;
        return pow(inp1, inp2);
    }
    float rem()
    {
        getInput();
        cout << "The result is :" << (int)inp1 % (int)inp2 << endl;
        return (int)inp1 % (int)inp2;
    }
};
int main()
{
    ScientificCalculator sfc;
    SimpleCalculator sc;
    sfc.exp();
    sc.div();
}