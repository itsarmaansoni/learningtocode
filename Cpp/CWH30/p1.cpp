//Fri May 19 08:18:15 AM IST 2023
//CWH 30
// Parameterized and default constructors in C++
#include <iostream>

using namespace std;

class Complex
{
private:
    int a,b;
public:
    void printData(void){
        cout<<"The Complex number is "<<a<<"+"<<b<<"i"<<endl;
    }
    Complex(void);
};

Complex :: Complex(void){ // this is a default constructor as it takes no parameter
    a=0;
    b=0;
}

int main(){

    Complex c;
    c.printData();

    return 0;
}