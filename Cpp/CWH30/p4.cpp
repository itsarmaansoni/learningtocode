//Fri May 19 08:18:15 AM IST 2023
//CWH 30
// Parameterized and default constructors in C++
#include <iostream>
#include <math.h>
using namespace std;

class Point
{
private:
    float x,y;

public:
    friend float pointDistCalc(Point a, Point b);
    Point(float a, float b){
        x=a;
        y=b;
    }
    void displayPoint(){
        cout<<"The point is ("<<x<<","<<y<<")"<<endl;
    }
};
float pointDistCalc(Point a, Point b){ // this is my solution function
    float dist=0;
    float xdif = b.x - a.x;
    float ydif = b.y - a.y;
    float xdifsq= xdif*xdif;
    float ydifsq= ydif*ydif;
    dist = sqrt(xdifsq+ydifsq);
    return dist;
}
int main(){
    Point p(1,1);
    Point q(0,0);
    float distance;
    distance = pointDistCalc(p,q);
    cout<<"The distance between points p and q is "<<distance<<" units."<<endl;
    return 0;
}
// plus I also changed the Point class such that,
// the x and y co-ordinates are float, so that they can 
// accept a wider range of values