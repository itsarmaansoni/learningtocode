//Fri May 19 08:18:15 AM IST 2023
//CWH 30
// Parameterized and default constructors in C++
#include <iostream>

using namespace std;

class Complex
{
private:
    int a,b;
public:
    void printData(void){
        cout<<"The Complex number is "<<a<<"+"<<b<<"i"<<endl;
    }
    Complex(int, int);
};

Complex :: Complex(int x, int y){ // this is a parameterized constructor as it takes two int parameter
    a=x;
    b=y;
}

int main(){
    // Implicit call
    Complex c(4,6);

    // Explicit call
    Complex d= Complex(5,9);

    c.printData();
    d.printData();


    return 0;
}