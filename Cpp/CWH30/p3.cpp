//Fri May 19 08:18:15 AM IST 2023
//CWH 30
// Parameterized and default constructors in C++
#include <iostream>

using namespace std;

class Point
{
private:
    int x,y;

public:
    Point(int a, int b){
        x=a;
        y=b;
    }
    void displayPoint(){
        cout<<"The point is ("<<x<<","<<y<<")"<<endl;
    }
};
int main(){

    Point p(3,5);
    Point q(4,6);
    p.displayPoint();
    q.displayPoint();

    return 0;
}
// quiz, write a function that takes two point objects
// and computes the distance between those points
// my solution in p4.cpp