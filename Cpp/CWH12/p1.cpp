//Pointers in C++
//25 April, 2023
//CWH12

#include <iostream>

using namespace std;

int main(){
	// a pointer is a data type which holds the address of other data types
	

	int a=3;
	int * b=&a; // <---- this is a pointer, b is a pointer, it is holding the address of a, &a gives the 
	// address of a, and * is the dereference operator 
	// we can print the address
	
	cout<<"The address of a is "<<b<<endl; // prints the value of b, which is address of a
	cout<<"The address of a is "<<&a<<endl; // &a also gives address of a
	cout<<"The value at a is "<<a<<endl; // prints the value of a
	cout<<"The value at a is "<<*b<<endl; //takes the pointer stored in b, and dereferences it
	// which is the value of a, because b stores pointer of a
	cout<<"The value at a is "<<*&a<<endl; // takes the address of and dereferences it, which is value of a
	//you can also make 
	//
	//
	
	// you can also make pointer to pointer variables
	// which store the address of pointer variables
	int ** c=&b;
	// if you dereference it once, you will just get the value at b, which is address of a, if you dereference it twice you will get the value of a
	// let's try
	cout<<"The value of c is "<<c<<endl; // the value of c is address of b;
	cout<<"The value of *c is "<<*c<<endl; // the dereferenced value of c is value of b
	cout<<"The value of **c is "<<**c<<endl; // the double dereferenced value of c is value of a
	return 0;

}
	
