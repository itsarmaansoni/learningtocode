//Mon May  8 08:07:18 AM IST 2023
//CWH 21
// Classes and access modifiers 
#include <iostream>
using namespace std;
class Employee
{
private:
    int a,b,c;
public:
    int d,e;
    void setdata(int a1, int b1, int c1); // declaration
    void getdata(){
        cout<<"the value of a is "<<a<<endl;
        cout<<"the value of b is "<<b<<endl;
        cout<<"the value of c is "<<c<<endl;
        cout<<"the value of d is "<<d<<endl;
        cout<<"the value of e is "<<e<<endl;
    }
};
// as you can see that we can 
// declare a function in the class and define it later outside the class
// just like we did for the setdata function
// or we can declare or define it at the same time inside the class
// just like we did our getdata() function
void Employee ::setdata(int a1,int b1, int c1){
    a = a1;
    b = b1;
    c = c1;
}
int main(){
    Employee Harry;
    Harry.setdata(14,232,323);
    Harry.getdata();
    // Harry.a=34;
    // Harry.b=3;
    // Harry.c=23;
    // the above lines will throw an error becuase we are
    //  trying to access private data 
    // members
    // but we can modify d and e as they are public data members
    // of class Employee
    Harry.d=19;
    Harry.e=21;
    return 0;
}
