// Fri May 26 09:51:03 AM IST 2023
// CWH 35
//  Destructors in C++

// used to destroy object and free the memory that was allocated to it
// they are not important for simpler and less memory intensive programs
// but they become useful in complex and memory hogging programs

#include <iostream>

using namespace std;
int count = 0;
class Num
{
public:
    Num()
    {
        count++;
        cout << "This is the time when constructor is called for object number" << count << endl;
    }
    ~Num()
    {
        cout << "This is the time when my destructor is called for object number" << count << endl;
        count--;
    }
};
int main()
{

    cout << "We are inside our main function" << endl;
    cout << "Creating first object n1" << endl;
    Num n1;
    {
        cout << "Entering this block" << endl;
        cout << "Creating two more objects" << endl;
        Num n2, n3;
        cout << "Exiting this block" << endl;
    }

    return 0;
}