//Mon Jul  3 08:53:47 AM IST 2023
//CWH 61
//File I/O read/write file in the same program
#include <iostream>
#include <fstream>
#include <string>
using namespace std;
int main(){
    ofstream out("file1.txt");
    cout<<"Enter your name:";
    string name;
    cin>>name;
    out<<"My name is "+name;
    out.close();
    ifstream in("file1.txt");
    cout<<"The first line of file1.txt is:";
    string line;
    getline(in,line);
    cout<<line<<endl;
    in.close();
    //can we have multiple lines read and writen to and from a file?
    //let's see in p2.cpp
    return 0;
}