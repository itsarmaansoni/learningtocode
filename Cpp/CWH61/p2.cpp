//Mon Jul  3 08:53:47 AM IST 2023
//CWH 61
//File I/O read/write file in the same program

// trying to read multiple lines

#include <iostream>
#include <fstream>
#include <string>
using namespace std;
int main(){
    string lines = "line 1\nline 2\nline 3";
    ofstream outToFile("file2.txt");
    outToFile<<lines;
    outToFile.close();
    // okay so writing multiple lines work
    // how to read multiple lines?
    string lineread;
    ifstream inFromFile("file2.txt");
    // do {
    // } while (lineread!="");

    getline(inFromFile,lineread);
    cout<<lineread<<endl;
    getline(inFromFile,lineread);
    cout<<lineread<<endl;
    getline(inFromFile,lineread);
    cout<<lineread<<endl;
    getline(inFromFile,lineread);
    cout<<lineread<<endl;// ah so after this point, it just keeps
    // returning the last line, that's the problem
    // anyway, this is beyond what cwh taught, I'll try and learn more
    // about reading whole files in C++ from some different source

    return 0;
}