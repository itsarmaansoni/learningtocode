// Mon Jun  5 08:22:41 AM IST 2023
// CWH 41
// Multiple inheritance deep dive

#include <iostream>

using namespace std;

// syntax for multiple inheritance
//  class Derived : visibility-mode1 Base1, visibility-mode2 Base2
//  {
//       class body
//  };

class Base1
{
protected:
    int base1Int;

public:
    void set_base1Int(int a)
    {
        base1Int = a;
    }
    void get_base1Int(void)
    {
        cout << "the value of base1Int is :" << base1Int << endl;
    }
};
class Base2
{
protected:
    int base2Int;

public:
    void set_base2Int(int a)
    {
        base2Int = a;
    }
    void get_base2Int(void)
    {
        cout << "the value of base2Int is :" << base2Int << endl;
    }
};
class Derived : public Base1, public Base2
{
public:
    void set(int a, int b)
    {
        set_base1Int(a);
        set_base2Int(b);
    }
    void show()
    {
        get_base1Int();
        get_base2Int();
    }
};
int main()
{
    Derived d;
    d.set(4, 5);
    d.show();
    return 0;
}