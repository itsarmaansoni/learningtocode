// Thu May 11 08:56:39 AM IST 2023
// CWH 24
//  static data members and methods in c++
#include <iostream>

using namespace std;

class Employee
{
private:
    int id;
    static int count;
    // Count is a static data member of employee
public:
    void setData(void)
    {
        cout << "Enter ID of Employee:";
        cin >> id;
        count++;
    }
    void getData(void)
    {
        cout << "Employee id is:" << id << endl;
        cout << "Count is:" << count << endl;
    }
    void static getCount(void){
        cout<<"The value of count is:"<<count<<endl;
    } // static member functions can only access static members 
};
int Employee ::count; // default value is 0, if you want different
// default value then you have to set it here
int main()
{

    Employee Harry, Rohan, Lovish;
    // Harry.id=1;
    // Harry.count=2;
    // cannot do this as Id and count are private
    Harry.setData();
    Harry.getData();
    Employee :: getCount();
    Rohan.setData();
    Rohan.getData();
    Employee :: getCount();
    Lovish.setData();
    Lovish.getData();
    Employee :: getCount();
    return 0;
}