//Wed Jun 14 08:26:46 AM IST 2023
//CWH 48
//Code example: Constructors in Derived class

/*
Case1:
class B: public A{
   // Order of execution of constructor -> first A() then B()
};

Case2:
class A: public B, public C{
    // Order of execution of constructor -> B() then C() and A()
};

Case3:
class A: public B, virtual public C{
    // Order of execution of constructor -> C() then B() and A()
};

*/
#include <iostream>

using namespace std;

class Base1
{
private:
    int data1;
public:
    Base1(int i){
        data1=i;
        cout<<"Base1 class Constructor Called"<<endl;
    }
    void printData(){
        cout<<"The value of data1 is "<<data1<<endl;
    }
};
class Base2
{
private:
    int data2;
public:
    Base2(int i){
        data2=i;
        cout<<"Base2 class Constructor Called"<<endl;
    }
    void printData(){
        cout<<"The value of data2 is "<<data2<<endl;
    }
};
class Derived : public Base1, public Base2
{
private:
    int derived1, derived2;
public:
    Derived(int a, int b, int c, int d) : Base1(a), Base2(b){
        // the order of Base1 and Base2 doesn't matter in the above line
        // it only matters in the class Declaration 
        // and that order is in the order which the constructors run
        derived1=c;
        derived2=d;
        cout<<"Derived class constructor called"<<endl;
    }
    void printData(){
        Base1 :: printData();
        Base2 :: printData();
        cout<<"The value of derived1 is "<<derived1<<endl;
        cout<<"The value of derived2 is "<<derived2<<endl;
    }

};  
int main(){

    //main code
    Derived d(1,2,3,4);
    d.printData();
    return 0;
}