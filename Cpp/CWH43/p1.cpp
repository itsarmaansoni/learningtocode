//Wed Jun  7 09:00:01 AM IST 2023
//CWH 43
// Ambiguitiy resolution in inheritance

#include <iostream>

using namespace std;

class Base1{
    public:
        void greet(){
            cout<<"Hello Everyone!"<<endl;
        }
};
class Base2{
    public:
        void greet(){
            cout<<"Heyyy Everyone!!!!!"<<endl;
        }
};
class Derived : public Base1, public Base2{
    int a;
    public:
        // the below code is for ambiguity resolution
        void greet(){
            Base1 :: greet();
        }
};

int main(){
    // Ambiguity 1
    Derived dobj;
    Base1 b1obj;
    Base2 b2obj;
    b1obj.greet();
    b2obj.greet();
    dobj.greet();
    // Ambiguity 2 is explained in p2.cpp
    return 0;
}