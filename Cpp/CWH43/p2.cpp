//Wed Jun  7 09:00:01 AM IST 2023
//CWH 43
// Ambiguitiy resolution in inheritance


#include <iostream>

using namespace std;

class Base{
    public:
        void say(){
            cout<<"Hello World!"<<endl;
        }
};
class Derived : public Base{
    int b;
    public:
        void say(){
            cout<<"Hello BEAUTIFUL PEOPLE!"<<endl;
        }
};

int main(){
    //Ambiguity 2
    // this isn't an ambiguity that requires resolution
    // this is an ambiguity that resolves itself
    // when you have a base class method that is inherited in the derived class
    // if the derived class has a public method of the same name, then
    // the derived class method will override the base class method
    Derived obj;
    obj.say();
    // removing the derived method will allow you to use the base class method
    return 0;
}