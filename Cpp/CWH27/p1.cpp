// Tue May 16 08:07:27 AM IST 2023
// CWH 27
//  Friend Classes and Member Friend functions in C++
#include <iostream>

using namespace std;
class Complex;
class Calculator
{
public:
    int sum(int a, int b)
    {
        return (a + b);
    }
    int sumRealComplex(Complex o1, Complex o2);
    int sumCompComplex(Complex o1, Complex o2);
};
class Complex
{
private:
    int a, b;

    // Individually declaring functions as friends
    friend int Calculator ::sumRealComplex(Complex com1, Complex com2);
    friend int Calculator ::sumCompComplex(Complex com1, Complex com2);

    //better way of doing it:
    // Declaring the entire Calculator class as a friend
    friend class Calculator;
public:
    void setNum(int n1, int n2)
    {
        a = n1;
        b = n2;
    }
    void printNum(void)
    {
        cout << "The value of complex number is " << a << "+" << b << "i" << endl;
    }
};
int Calculator ::sumRealComplex(Complex o1, Complex o2)
{
    return (o1.a + o2.a);
}
int Calculator ::sumCompComplex(Complex o1, Complex o2)
{
    return (o1.b + o2.b);
}

int main()
{

    Complex o1, o2;
    o1.setNum(1, 4);
    o2.setNum(5, 7);
    Calculator obj;
    int sumOfRealPart = obj.sumRealComplex(o1, o2);
    int sumOfCompPart = obj.sumCompComplex(o1, o2);
    cout << "The value of sum of Real Part is " << sumOfRealPart << endl;
    cout << "The value of sum of Complex Part is " << sumOfCompPart << endl;

    return 0;
}