//Fri Jun 30 08:42:13 AM IST 2023
//CWH 60
//File I/O in C++

#include <iostream>
#include <fstream>

/*
The useful classes for working with files in C++ are:
1. fstreambase
2. ifstream -->derived from fstream base
3. ofstream -->derived from fstream base as well
*/
// In order to work with files in C++, you have to open it
// there are primarily two ways:
// 1. using the constructor
// 2. using the member function open() of the class

using namespace std;

int main(){
    // opening files using constructor and writing it
    ofstream out("write.file");
    string w= "Armaan\n";
    out<<w;
    //opening files using constructor and reading it
    // ifstream in("read.file");
    // string r;
    // // in>>r; // only reads till first space or nextline character
    // // superior method below:
    // getline(in,r); // reads an entire line
    // cout<<r<<endl;
    return 0;
}