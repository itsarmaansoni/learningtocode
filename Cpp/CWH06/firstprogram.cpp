//understanding headerfiles and operators 

//if you ctrl+click on the headerfile, you open it on a new tab in vscode, here you can see the contents of the headerfile
//this header file adds functionality to our program, in this case it gives us access to input and output stream and we are able to use cout function

#include<iostream>
#include"this.h"
//we will understand the statement below in upcoming chapters
using namespace std;

int main(){
	//we could have also used the code below to do the same thing
	//std::cout<<"This is hello world program";
	//but we don't need to do that because we have already put "using namespace std;" statement before the main() function
	cout<<"This is hello world program";
	return 0;
}



// for the notes
// There are two types of header files
// 1.) System header files: It comes with the compiler 
// 2.) User defined header files: It is written by the programmer
// //iostream is a system headerfile
// an example of a user defined header file would be something like #include "this.h", where you'd have to write the contents of this.h, we have created an empty this.h 
// file in the same folder as this program, so this shouldn't give any errors
// if "this.h" is not present in the current directory then the program will throw an error
//
// you can go to cppreference.com to learn more about System header files.
