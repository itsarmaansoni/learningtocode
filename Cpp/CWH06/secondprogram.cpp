// learning about operators
//
//
#include <iostream>


using namespace std;

int main(){
	cout<<"Operators in cpp:"<<endl; //endl is used to jump to a newline, basically like \n
	cout<<"Following are the types of operators in c++"<<endl;
	// Arithmetic operators
	int a = 4, b=5;
	cout<<"The sum of a and b is "<<a+b<<endl; // here + is the operator and it is an arithmetic operator, below are some more common arithmetic operators
	cout<<"The difference of a and b is "<<a-b<<endl;  
	cout<<"The multiple of a and b is "<<a*b<<endl;  
	cout<<"The division of a and b is "<<a/b<<endl;  
	cout<<"The modulo of a and b is "<<a%b<<endl;  
	cout<<"a++ is "<<a++<<endl;  //it updates the value after using the original, yeah it's kinda weird, but it's a postfix operator
	//the value of a is now 5, a++ is basically a=a+1;
	
	cout<<"a-- is "<<a--<<endl; //it updates the value after using the original, yeah it's kinda weird, but it's a postfix operator
	//the value of a is now 4 again, a-- is basically a=a-1; same with --a but it uses the new value instead of original, it's kinda skullf**king
	cout<<"++a is "<<++a<<endl; //it updates the value then uses it, it's a prefix operator 
	cout<<"--a is "<<--a<<endl; //it updates the value then uses it, it's a prefix operator 



	// assignment operators ----> used to assign values to variables, pretty straightforward, "=" is the assignment operator
	int c=53, kd=234;
	char d='d';

	

	//Comparison operator
	//
	//
	// they allow us to compare two values, to check whether they are equal or not, to check which value is bigger and which is smaller
	cout<<"The value of a is "<<a<<endl;
	cout<<"the value of b is "<<b<<endl;

	cout<<"The value of a == b is "<< (a==b) <<endl;// we must not forget to enclose the expression in parenthesis
	cout<<"The value of a != b is "<< (a!=b) <<endl;
	cout<<"The value of a >= b is "<< (a>=b) <<endl;
	cout<<"The value of a <= b is "<< (a<=b) <<endl;
	cout<<"The value of a < b is "<< (a<b) <<endl;
	cout<<"The value of a > b is "<< (a>b) <<endl;
	cout<<endl; // just making things look neat
	// if the output of comarison operator is 1, it means the expression evaluated true, and if its 0, it means expression evaluated false
	



	//Logical operators
	// logical operators and comparision operators are closely related because logical operators also return 1 & 0 for true and false, 
	// they only work with 1 and 0 values as well, i.e. true and false
	// && operator is "and operator"
	// || operator is "or operator"
	// ! operator is "logical not operator"
	// here is the truth table of the && operator 
	// && returns 1 if both values are 1 and returns 0 in all other cases, i.e (0,1),(1,0),(0,0), only gives 1 when (1,1)
	// here is the truth table of the || operator
	// || returns 1 if any one or both inputs are 1, i.e it will return 1 in these cases (1,1),(0,1),(1,0) and return false when (0,0) 
	// ! operator flips the input, if the input is 1, it will return 0 and if the input is 0, it will return 1
	//
	



	// here are logical operators in action
	//
	//
	int v1=1,v2=0; //you can try and change these values to see how it affects the output
	cout<<"The value of v1 is "<<v1<<endl;
	cout<<"The value of v2 is "<<v2<<endl;
	// it should be noted that any value other than 0 is also considered true, that is 22 is true and so is 98, only 0 is false
	cout<<"The value of v1&&v2 is "<< (v1&&v2)<<endl;
	cout<<"The value of v1||v2 is "<< (v1||v2)<<endl;
	cout<<"The value of !v1 is "<< (!v1)<< endl;
	cout<<"The value of !v2 is "<< (!v2)<< endl;
	cout<<endl;
	
	// There are also bitwise operator, but they are not explained in this chapter

	return 0;
}
