// Wed Jun 21 09:23:21 AM IST 2023
// CWH 53
//  this pointer in C++
#include <iostream>
using namespace std;
class A
{
    int a;

public:
    A &setData(int a)
    {
        this->a = a;
        return *this;
    }

    void getData()
    {
        cout << "The value of a is " << a << endl;
    }
};

int main()
{
    A a;
    a.setData(4).getData();
    return 0;
}
