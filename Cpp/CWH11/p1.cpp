// break and continue statements in C++
// 24 April, 2023
// CWH 11
//
//
//
//
//
// break and continue statements are used in loops and switch cases to break out of them or skip one iteration
// respectively if a condition is met

#include <iostream>
using namespace std;
int main(){
	cout<<"demonstrating break at iteration 4"<<endl;
	for (int i=1;i<=10;i++){
		if (i==4){
			break;
			// this breaks the program at the iteration 4, and it doesn't execute
			// the rest of of the loop body after this, so numbers will
			// only be printed upto 3 and we will be out of loop
		}
		cout<<i<<endl;
	}

	cout<<"demonstrating continue at iteration 4"<<endl;
	for (int i=1;i<=10;i++){
		if(i==4){
			continue;
			// this skips iteration number 4, the rest of the loop body isn't executed
			// and we go to updation statement of the loop and then the loop condition is checked
			// and then we are back to loop body, if loop condition is true
		}
		cout<<i<<endl;
	}
		
	return 0;
}
