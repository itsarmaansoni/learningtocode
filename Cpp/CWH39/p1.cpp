//Thu Jun  1 10:03:09 AM IST 2023
//CWH 39
// Protected access modifier in C++
/*
                        Public Derivation       Private Derivation      Protected Derivation
Private members         Not Inherited           Not Inherited           Not Inherited              
Protected members       Protected               Private                 Protected                    
Public members          Public                  Private                 Protected 


the trick to remember this is to 
know that private can't be inherited 
public derivation doesn't change anything, so public is public and protected is protected
private derivation makes everything private, so protected is private and public is private
protected derivation makes everyting protected, so protected is protected and public is protected


what's different about protected?
well to put it simply, protected behaves just like private, it just allows inheritability
*/

#include<iostream>
using namespace std;

class Base{
    protected:
        int a; 
    private:
        int b;

};

class Derived: protected Base{
   
};

int main(){
    Base b;
    Derived d;
    // cout<<d.a; // Will not work since a is protected in both base as well as derived class
    return 0;
}

