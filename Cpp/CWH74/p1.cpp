//Thu Jul 20 07:51:08 AM IST 2023
//CWH 74
// Function Objects Functor objects
#include <iostream>
#include <functional>
#include <algorithm>
using namespace std;
int main(){
    //Function Objects(Functor): Function wrapped in a class so that
    // it is available like an object
    int arr[] = {12,54,77,1,3,5,};
    sort(arr,arr+6);
    for (int i=0;i<6;i++){
        cout<<arr[i]<<endl;
    }
    //also passing a function object
    //for reverse sorting
    sort(arr,arr+6,greater<int>());
    for (int i=0;i<6;i++){
        cout<<arr[i]<<endl;
    }
    return 0;
}

//Completed C++ playlist!s