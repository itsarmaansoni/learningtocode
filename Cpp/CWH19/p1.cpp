//Thu May  4 08:43:10 AM IST 2023
//CWH 19
//Function Overloading 

#include <iostream>
using namespace std;

// function overloading is basically having multiple functions with the same name,
// and differentiating them through number of parameters and type of parameters
int sum (int a, int b){
	return a+b;
}
int sum (int a, int b, int c){
	return a+b+c;
}
// volume of cylinder
float volume (int r,int h){
	return 3.141 * r * r * h;
}
// volume of cube
float volume (int s){
	return s*s*s;
}
//volume of cuboid
float volume (int l, int b, int h){
	return l*b*h;
}

int main(){
	cout<<"The sum of 1,2 and 3 is :"<<sum(1,2,3)<<endl;
	cout<<"The sum of 1 and 2 is :"<<sum(1,2)<<endl;
	cout<<"The volume of cylinder is :"<<volume(3,5)<<endl;
	cout<<"The volume of cylinder is :"<<volume(3,5)<<endl;
	cout<<"The volume of cube is :"<<volume(3)<<endl;
	cout<<"The volume of cuboid is :"<<volume(3,5,4)<<endl;
	return 0;
}
