
// For, While, Do-while loop in C++
// 21 April, 2023
// CWH10

//The solution to the exercise from code with harry,
//task: write the table of 6 using the do while loop
//my solution:


#include <iostream>
using namespace std;
int main(){
	int i=1;
	do {
		cout<<6<<'x'<<i<<'='<<(6*i)<<endl;
		i++;
	}
	while (i<=10);
}
