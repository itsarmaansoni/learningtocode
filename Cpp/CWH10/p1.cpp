// For, While, Do-while loop in C++
// 21 April, 2023
// CWH10

#include<iostream>
using namespace std;
int main(){
	// Loops in C++
	// there are three types of loops in C++
	//
	// 1.) for loop
	// 2.) while loop
	// 3.) do-while loop
	//
	//
	//about For loop
	
	for (int i=1;i<=100;i++){//initialization expression, loop condition, updation expression{
		cout<<i<<endl; //loop body
	}
	// general flow of a for loop, first the initialization statement runs, then loop condition is checked
	// then it enters the loop body and runs the code in there, then it runs the updation statement, then 
	// it checks the condition again, then it runs the code in loop body again, if the condition is false,
	// it exits the loop
	


	//about while loop
	// Syntax of while loop
	// while (condition){
	// 	loop body;
	// }
	
	// flow of while loop, checks loop conditions, if true, executes loop body, checks condition if true, 
	// executes loop body, else exits
	
	// printing 1 to 40 using while loop
	 
	int i=1;
	while (i<=40){
		cout<<i<<endl;
		i++;
	}




	//about do-while loop
	//Syntax of do while loop
	//do {
	//	loop body;
	//}
	//while (condition)
	int f=1;
	do {
		cout<<f<<endl;
		f++;
	}
	while (f<=40);
	// the thing about do while loop is that they run once, even if the condition is false
	


	return 0;
}
// CWH gave an exercise to write the table of 6 using do while loop, my solution is in p2.cpp





