// Tue May  9 09:03:40 AM IST 2023
// CWH 22
// Nesting of member functions in C++

// OOPs - Classes and objects
// C++ --> initially called C with classes
// class is extension of structures (in C)
// features like being able to kkadd function
// and access modifiers

// structures had limitation
// 1.) members are public
// 2.) can't add methods in structures
// classes are structures + more
// classes can have methods and properties
// classes can make few members as private and few as public
// Structures in C++ are typedefed implicitely
// you can declare objects along with class declaration like this
// 		class Harry{
// 		class definition
// 		} harry, lovish, rohan;
// harry.salary = 19 makes no sense if salary is a private property/variable

#include <iostream>
#include <string>
using namespace std;
// NESTING OF MEMBER FUNCTIONS
class binary
{
private:
	string s; // by default a member in class is private, so you don't have to
	// specify private for string s;

	void check(void);

public:
	void read(void);
	void display(void);
	void onescompliment(void);
};
void binary ::read(void)
{
	cout << "Enter a binary number:";
	cin >> s;
	check(); // as you can see, we didn't need to type obj.function, we just typed function();
			 // it automatically works for the current object
}
void binary ::check()
{
	for (int k = 0; k < s.length(); k++)
	{
		if (s.at(k) != '0' && s.at(k) != '1')
		{
			cout << "Incorrect binary format" << endl;
			return;
		}
	}
	cout << "Correct binary format!" << endl;
	return;
}
void binary ::display()
{
	cout << "your binary number is:";
	for (int j = 0; j < s.length(); j++)
	{
		cout << s.at(j);
	}
	cout << endl;
}
void binary ::onescompliment()
{
	for (int j = 0; j < s.length(); j++)
	{
		if (s.at(j) == '0')
		{
			s.at(j) = '1';
		}
		else
		{
			s.at(j) = '0';
		}
	}
	cout << "one's complement: done" << endl;
}
int main()
{
	binary b;
	b.read();
	//b.check(); // this will throw an error because check is a private method
	b.display();
	b.onescompliment();
	b.display();
	// nesting of member functions basically means,
	// that we don't have to type b.check() inside a function of that same class
	// we can just type check()
	// see read();
	return 0;
}