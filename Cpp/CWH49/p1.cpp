// Thu Jun 15 08:52:36 AM IST 2023
// CWH 49
//  Initialization list in Constructors in C++

#include <iostream>
using namespace std;

/*
Syntax for initialization list in constructor:
Constructor (argument-list) : initialization-section
{
    assignment + other code;
}

*/
class Test
{
    int a;
    int b;

public:
    // Test(int i, int j) : a(i), b(j)
    // Test(int i, int j) : a(i), b(i+j)
    // Test(int i, int j) : a(i), b(2*j)
    // Test(int i, int j) : a(i), b(a+j)
    Test(int i, int j) : b(j), a(i+b) //--> RED FLAG!!! THIS WILL be a problem becuase
    // a will be initialized first because it's the first private member that is declared

    {
        cout << "Constructor executed" << endl;
        cout << "Value of a is " << a << endl;
        cout << "Value of b is " << b << endl;
    }
};

int main()
{
    Test t(4, 6);

    return 0;
}
