//Wed May  3 08:54:43 AM IST 2023
//CWH 18
//recursion and recursive functions in Cpp

// recursive functions are functions
// that call themselves and keep calling 
// themselves until a base case is met

// let's look at some code


#include <iostream>
using namespace std;
int factorial(int num){
	if (num==1 || num==0)
	return 1;
	else
	return num*factorial(num-1);
}
int fibonacci(int term){
	if (term<=2)
	return 1;
	else 
	return fibonacci(term-1)+fibonacci(term-2);
}
	
int main(){
	int usernum;
	cout<<"Enter a number to find the factorial of : ";
	cin>>usernum;
	cout<<"The factorial of the given number is :"<<factorial(usernum)<<endl;
	// using a factorial might not be the most efficient approach 
	// but it makes it easier to write logic for some problems
	//
	
	// here is fibonacci term finder using recursion
	int term;
	cout<<"Enter term you want to find:";
	cin>>term;
	cout<<"fibonacci term "<<term<<" is "<<fibonacci(term)<<endl;
	return 0;
}
