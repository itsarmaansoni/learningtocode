
// Constants, Manipulators & Operator Precedence
// 19th April, 2023
// cwh8


//Operator Precedence
#include <iostream>

using namespace std;

int main(){
	int a = 3, b = 4;
	int c = a*5+b;
	// now what do you think the value of
	// c will be?
	// that's where operator precedence comes into
	// play, while it may seem that using the
	// bodmas rule will work, and yes, it may
	// work sometimes, but for evaluating these
	// expression yourself correct all the time
	// you need to learn about operator precedence
	// * / % (left to right)
	// + - (left to right) 
	// (top to bottom)
	// so according to the the precedence above
	// our expression a*5+b will evaluate like this
	// first a will be multiplied with 5
	// which will give 15, then b will be added to
	// that value which is 15, b = 4, so 
	// the final value is 19
	// let's see
	cout<<"The value of c is "<<c<<endl;
	// you can learn more about operator precedence
	// in cppreference.com




	return 0;
}
