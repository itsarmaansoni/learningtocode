// Constants, Manipulators & Operator Precedence
// 19th April, 2023
// cwh8

#include <iostream>
using namespace std;
int main(){
	// The purpose of the code below is to 
	// demonstrate that the variables can be 
	// changed, but what if we don't want that?
	// what if we want them to hold a constant
	// value and not change? for that we 
	// use constants in c++
	int a = 34;
	char c = 'c';
	cout<<"The value of a was "<<a<<endl;
	cout<<"The value of c was "<<c<<endl;
	a = 45;
	c = '4';
	cout<<"The value of a is "<<a<<endl;
	cout<<"The value of c is "<<c<<endl;
	
	//demonstrating constants in c++
	// we use the keyword const to make a 
	// variable constant
	const int d = 3;
	cout<<"The value of d was "<<d<<endl;
	// d = 34;
	// an attempt to change the value of d
	// this line d = 34 throws an error, because 
	// d is now a read-only variable, so I 
	// uncommented that line
	cout<<"The value of d is "<<d<<endl;


	return 0;
}
