// Constants, Manipulators & Operator Precedence
// 19th April, 2023
// cwh8

//In this program we will learn about manipulators
//Manipulators are values that you can insert into
//or extract from iostreams to have special effects.

#include <iostream>
#include <iomanip>

using namespace std;

int main(){
	int a = 3, b = 123, c = 5432;
	//endl is a manipulator that is
	//used to go to next line when 
	//using the output stream, it is present in 
	//iostream header file
	
	cout<<"The value of a is "<<a<<endl;
	cout<<"The value of b is "<<b<<endl;
	cout<<"The value of c is "<<c<<endl;

	//there is also another manipulator which 
	//is present in the header file iomanip
	//it's called setw, and it is used to format
	//integers in such a way that, the zeroth 
	//position of one integer 
	//matches with another
	// here is how it works
	cout<<"The value of a with setw is "<<setw(4)<<a<<endl;
	cout<<"The value of b with setw is "<<setw(4)<<b<<endl;
	cout<<"The value of c with setw is "<<setw(4)<<c<<endl;
	return 0;
	//setw stands for set width
	//it is used to set field width
}

	

