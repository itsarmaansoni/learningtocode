//Tue Jul 18 12:04:22 PM IST 2023
//Code Help - Babbar Lecture 4
//Solving Pattern Questions

// 1234554321
// 1234**4321
// 123****321
// 12******21
// 1********1

// DABANG PATTERN

#include <iostream>
using namespace std;
int main(){
    int n;
    cin>>n;
    for (int i=1;i<=n;i++){
        // pehle seedhe ginti print karwao
        for (int j=1;j<=n-i+1;j++){
            cout<<j<<" ";
        }
        // fir stars wala print karwao
        // star ka formula nikala hai pehle
        //1 -> 0
        //2 -> 2
        //3 -> 4
        //4 -> 6
        //5 -> 8
        // (i-1)*2 <- ye hai hamara stars ka formula
        for (int k=1;k<=(i-1)*2;k++){
            cout<<"*"<<" ";
        }
        // fir ulti ginti print karwao
        for (int j=n-i+1;j>=1;j--){
            cout<<j<<" ";
        }
        //next line me chale jao
        cout<<endl;
    }
}