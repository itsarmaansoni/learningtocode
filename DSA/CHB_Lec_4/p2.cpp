//Mon Jul 17 09:52:09 PM IST 2023
//Code Help - Babbar Lecture 4
//Solving Pattern Questions

// pattern :
// 1
// 2 3
// 3 4 5
// 4 5 6 7
#include <iostream>
using namespace std;
int main(){
    int n;
    cin>>n;
    for (int i=1;i<=n;i++){
        int count =i;
        for (int j=1;j<=i;j++){
            cout<<count<<" ";
            count++;
        }
        cout<<endl;
    }
    return 0;
}