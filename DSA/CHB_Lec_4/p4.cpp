//Tue Jul 18 12:04:22 PM IST 2023
//Code Help - Babbar Lecture 4
//Solving Pattern Questions

//   1
//  121
// 12321
//1234321 

// spaces are n-1 
#include <iostream>
using namespace std;
int main(){
    int n;
    cin>>n;
    for (int i=1;i<=n;i++){
        for (int j=n-i-1;j>=0;j--){
            cout<<" ";
        }
        int j=1;
        for (;j<=i;j++){
            cout<<j;
        }
        j-=2;
        while(j!=0){
            cout<<j;
            j--;
        }
        cout<<endl;
    }
    return 0;
}
 