//Tue Jul 18 01:15:16 PM IST 2023
//Code Help - Babbar Lecture 5
//Bitwise Operators, For Loops, Operator Precedence & Variable Scoping

#include <iostream>
using namespace std;
int main(){
    int a = 4;
    int b = 6;
    cout<<" a&b "<<(a&b)<<endl;
    cout<<" a|b "<<(a|b)<<endl;
    cout<<" ~a "<<(~a)<<endl;
    cout<<" a^b "<<(a^b)<<endl;

    //right shift
    cout<<" a>>1 "<<(a>>1)<<endl;
    //left shift
    cout<<" a<<1 "<<(a<<1)<<endl;

    //post unary operators
    cout<<a++<<endl;
    cout<<a--<<endl;
    //pre unary operators
    cout<<--a<<endl;
    cout<<++a<<endl;

}