//Tue Jul 18 01:15:16 PM IST 2023
//Code Help - Babbar Lecture 5
//Bitwise Operators, For Loops, Operator Precedence & Variable Scoping

#include <iostream>
#include <math.h>
using namespace std;
int isPrime(int n){
    int count = 0;
    for (int i=2;i<=sqrt(n);i++){
        if(n%i==0)
        count++;
    }
    if (count==0){
        return 1;
    }
    else
    return 0;
}
int main(){
    for (int i=0;i<=50;i++){
        if(isPrime(i))
        cout<<i<<" is a prime number"<<endl;
    }
}