//Tue Jul 18 01:15:16 PM IST 2023
//Code Help - Babbar Lecture 5
//Bitwise Operators, For Loops, Operator Precedence & Variable Scoping

#include <iostream>
using namespace std;

int main(){
    int n;
    cin>>n;
    int sum = 0;
    int a=0;
    int b=1;
    if(n<=2){
        cout<<(n-1);
    }
    else{
        for (int i=3;i<=n;i++){
            sum=a+b;
            a=b;
            b=sum;
        }
        cout<<sum;
    }
    return 0;
}