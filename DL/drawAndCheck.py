import cv2
import numpy as np
from tensorflow.keras.models import load_model
import matplotlib.pyplot as plt
import matplotlib
# matplotlib.use('GTK3Cairo')
# Part 1: Draw the digit using OpenCV
def draw(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN or flags == cv2.EVENT_FLAG_LBUTTON:
        cv2.circle(canvas, (x, y), 10, (0, 0, 0), -1)  # Draw a black circle

# Create a blank white canvas
canvas = np.ones((280, 280), dtype="uint8") * 255

cv2.namedWindow("Draw Digit")
cv2.setMouseCallback("Draw Digit", draw)

print("Instructions:")
print("- Draw your digit using the left mouse button.")
print("- Press 's' to save and predict the digit.")
print("- Press 'c' to clear the canvas.")
print("- Press 'q' to quit without saving.")

while True:
    cv2.imshow("Draw Digit", canvas)
    key = cv2.waitKey(1)

    if key == ord('s'):  # Save and exit
        cv2.imwrite("digit.png", canvas)
        break
    elif key == ord('c'):  # Clear canvas
        canvas.fill(255)
    elif key == ord('q'):  # Quit without saving
        break

cv2.destroyAllWindows()

# Part 2: Preprocess the image
try:
    # Load the saved digit
    image = cv2.imread("digit.png", cv2.IMREAD_GRAYSCALE)

    # Resize to 28x28 (MNIST input size)
    image_resized = cv2.resize(image, (28, 28))

    # Normalize pixel values (0-1 range)
    image_normalized = image_resized / 255.0

    # Invert colors (model expects black background, white digit)
    image_inverted = 1 - image_normalized

    # Add batch and channel dimensions (shape: 1, 28, 28, 1)
    input_data = np.expand_dims(image_inverted, axis=(0, -1))

    # Part 3: Load the trained model and make predictions
    print("Loading the trained model...")
    model = load_model("my_mnist_model.h5")  # Replace with your model's filename

    print("Predicting the digit...")
    predictions = model.predict(input_data)
    predicted_digit = np.argmax(predictions)

    # Display the result
    print(f"Predicted digit: {predicted_digit}")

    # Part 4: Visualize the input and prediction
    plt.imshow(image_resized, cmap="gray")
    plt.title(f"Predicted Digit: {predicted_digit}")
    plt.axis("off")
    plt.show()

except Exception as e:
    print(f"An error occurred: {e}")

