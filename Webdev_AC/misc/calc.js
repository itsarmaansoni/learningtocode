
function equationSolver(lol){
    // if(lol.length>3){
    //     return;
    // }
    // else{
    //     switch(lol[1]){
    //         case '+':
    //             return (lol[0]-'0')+(lol[2]-'0');
    //         case '-':
    //             return (lol[0]-'0')-(lol[2]-'0');
    //         case '×':
    //             return (lol[0]-'0')*(lol[2]-'0');
    //         default:
    //             return 0;
    //     }
    // }


    // this FUNCTION HAS TO BE MADE !!!!


    /// chat GPT code
    // Replace '×' with '*' and '÷' with '/' to use JavaScript's built-in operators
    lol = lol.replace(/×/g, '*').replace(/÷/g, '/');
    
    try {
        // Evaluate the expression using JavaScript's eval function
        // eval() can be dangerous if used with untrusted input, but here we assume input is trusted.
        let result = eval(lol);
        return result;
    } catch (error) {
        // If there's an error in evaluation, return an error message
        return "Error in expression";
    }
}
document.addEventListener("DOMContentLoaded", () => {
    const src = document.getElementById("screeneroid");
    // Function to be triggered on button click
    function handleButtonClick(event) {
        if (event.target.id == "equal")
            return ;
        const buttonText = event.target.textContent.trim();
        src.innerText += buttonText;
    }
    function backspace(event){
        src.innerText = src.innerText.slice(0,src.innerText.length-2);
    }
    // Select all buttons with the class 'button'
    const buttons = document.querySelectorAll(".number");
    function equate(event){
        const equation = src.innerText;
        src.innerText=equationSolver(equation);
        // console.log(equation);

    }
    // Add click event listener to each button
    buttons.forEach(button => {
        button.addEventListener("click", handleButtonClick);
    });
    const backarrow = document.getElementById("arrow");
    backarrow.addEventListener("click",backspace);
    const equal = document.getElementById("equal");
    equal.addEventListener("click",equate);
});
