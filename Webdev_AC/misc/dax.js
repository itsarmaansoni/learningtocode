document.addEventListener('DOMContentLoaded', function() {
    const form = document.getElementById('login-form');

    if (form) {
        form.addEventListener('submit', function(event) {
            event.preventDefault(); // Prevent the form from submitting

            // Get input values
            const username = document.getElementById('username').value;
            const password = document.getElementById('password').value;

            // Hardcoded credentials for demo purposes
            const validUsername = 'armaan_soni';
            const validPassword = 'password123';

            // Check if entered credentials match
            if (username === validUsername && password === validPassword) {
                console.log('Login successful!');
                window.location.href = 'index.html';
            } else {
                console.log('Invalid username or password. Please try again.');
                messagecontainer.innerText = 'login failed!';
            }
        });
    } else {
        console.error('Login form not found.');
    }
});
