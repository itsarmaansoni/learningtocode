#!/bin/bash
# 1st May 2023
# while loops in bash scripting



myvar=1
while [ $myvar -le 10 ]
do 
	echo $myvar
	myvar=$(expr $myvar + 1) 
	sleep 0.5
done

