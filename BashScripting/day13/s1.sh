#!/bin/bash
# case statements in bash script
# Tue May 9 11:51:28 AM IST 2023
# learn linux tv 13
read -p "Enter a number from 1-5:" num
case $num in 
    1) echo "you entered one";;
    2) echo "you entered two";;
    3) echo "you entered three";;
    4) echo "you entered four";;
    5) echo "you entered five";;
    *) echo "Invalid choice"
esac

