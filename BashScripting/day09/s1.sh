#!/bin/bash
# learn linux tv day 9
# for loop in bash script

for current_number in 1 2 3 4 5 6
do 
	echo $current_number
	sleep 0.9
done
echo "process completed succesfully"

# what you can also do is 

for current_number in {1..11}
do 
	echo $current_number
	sleep 2
done
echo "process completed succesfully"

for file in logfiles/*.log
do 
	tar -czvf $file.tar.gz $file
done
# this last for loop iterates over every 
# file that ends will .log in the logfiles folder
# it then creates tarballs for those files
