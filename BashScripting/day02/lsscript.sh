ls

# bashscripting, writing our first script
# learn linux tv 2
# 24 April, 2023
# this is the ls script, this only has one command in it, which is ls, and this is a command
# in this script, you have to make it executable with chmod +x lsscript.sh, and saving with .sh extension is optional
# we run the script with ./lsscript.sh
# we can also add more commands to this script and it will run them sequencely 
pwd
# I added the pwd command
# now we will see how to properly write a bash script, in s2.sh
