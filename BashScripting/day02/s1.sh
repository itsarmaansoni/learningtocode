# bashscripting, writing our first script
# learn linux tv 2
# 24 April, 2023
##what is bash?
#bash is a shell, anytime we open a terminal, we are interacting with a shell, 
#we give commands and get output from shell
#some example commands in bash
#ls
#pwd
#cd 
#ls=list storage
#pwd=print working directory
#cd= change directory

# there are tons and tons of commands, and as a sys admin, you will have to use many commands
# that might repeat themselves, that's where automation comes into play, you need to be able to
# make scripts that will execute those commands for you in a shell, that's exactly what bash scripts are for
# on most linux systems the default shell is bash, there are other shells as well, like zsh, and dash and fish
# to know what shell you are on
# you can type this command in the shell 
# echo $SHELL
# if this gives out /bin/bash, then you are using bash shell
# if you aren't using bash, then you can type in 'which bash'
# which is going to give you the directory of the binary, to start using it
# which bash gives
# /usr/bin/bash
# a script is a text file with one or more commands in it, when executed, the commands in that file are executed 
# just as if you entered them yourself in sequence
# we will make a simple hello world script s2.sh
