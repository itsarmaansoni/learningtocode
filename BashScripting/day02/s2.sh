#!/bin/bash

# bashscripting, writing our first script
# learn linux tv 2
# 24 April, 2023
#the first line is called a shebang, or a hashbang, is the very first line of any script and it tells the interpreter what shell to use, even if you are not using a bash shell, if you specify the bash in the shebang, the interpreter will run your script with bash shell, which adds to the reliability of scripts
# it's very important to always use shebangs, even if you don't use extension .sh, 
# we will write a hello world script now
echo "Hello World!"

# we can also add a few more commands

echo "My current working directory is:"
pwd
