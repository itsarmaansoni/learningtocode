#!/bin/bash
# Universal Update Script
# learn linux tv day 8


if [ -d /etc/pacman ]
then 
	sudo pacman -Syu
fi
if [ -d /etc/portage ]
then 
	sudo emerge --sync
	sudo eix-update
	sudo emerge -vuDN @world
fi
if [ -d /etc/apt ]
then 
	sudo apt update 
	sudo apt upgrade -y
fi
