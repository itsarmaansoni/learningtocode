# well today we didn't learn any scripting it was just 
# about where to store your scripts so that they are accessible to the people you want them to be accessible to
# 
# three things to remember:
# you can store your scripts in /usr/local/bin
# you don't need to have the .sh extension when storing the script in /usr/local/bin
# you have to make sure that no one can edit your scripts, you have to chown and chmod them carefully,
# it's always a good idea to chown root:root them
