#!/bin/bash
#learning about if statements in bash scripting 
# 27th April, 2023
# learn linux tv 5

mynum=234

if [ $mynum -eq 200 ] # we could use -ne to check if it's not equal
#gt for greater than 
#lt for less than
then
	echo "The condition is True"
else
	echo "The condition is not true"
fi


num1=3

if [ $(expr $num1 % 2) -eq 0 ]
then 
	echo "num1 is even"
else 
	echo "num1 is odd"
fi

# we can also check whether a file exists or not

if [ -f ~/Documents/learningtocode/C/CWH14/p1.c ] # if it's a directory that you are looking for, you change
# -f to -d
then 
	echo "File exists"
else 
	echo "File doesn't exists"
fi
