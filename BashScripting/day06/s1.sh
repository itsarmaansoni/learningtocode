#!/bin/bash
# exit codes
# how exit codes help us determine if a script was successful or not

# we can check the exit code of the most recent command by echoing the variable '?'

# echo $? is already defined in the environment

# for example if you run "ls -lah /home/"
# and it runs successfully you can echo $? to see that it is set to '0'
# which means the command ran successfully
# but if you run the same command ls on some directory that doesn't exist
# for example "ls -lah /root/shoot/"
# and then echo the value of ?
# then you will see some value other than 0
package=htop
sudo emerge -v $package
echo "The exit code for the package installed is : $?"
# you can change the exit code by typing 'exit any_number'
exit 0
# meaning that the script was successful 
# it also exits the script, so any coommands after that won't ruhn
#
