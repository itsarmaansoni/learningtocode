#!/bin/bash
# learn linux tv 11
# 5th May, 2023
# Data stream 

echo "Enter your name:"
read myname
echo "$myname"
# what you have just seen is an example of 
# standard input, 
# standard input is a data stream
# there is also, standard output and standard error
#
# standard input --> stdin --> 0
# standard output --> stdout --> 1 
# standard error --> stderr --> 2

# stderr is where the errors go
# stdout is where the normal output goes
# you can divert the output of these streams
# the two commands below are for 
# diverting the stdout to somefile
# 
# #somecommand 1> #somefile
# #somecommand > #somefile
#
# diverting the stderr to somefile
# #somecommand 2> #somefile
