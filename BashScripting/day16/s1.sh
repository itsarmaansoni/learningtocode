#!/bin/bash
# Fri May 12 10:38:33 AM IST 2023
# Arguments in bash Script

echo "You entered the argument: $1"
# this is the simplest bash script that makes use of bash arguments
# it doesn't require read and stuff, it just stores the first argument in $1
# the $# stores the number of arguments