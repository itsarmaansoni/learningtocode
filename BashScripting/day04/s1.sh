#!/bin/bash
#day 4 of Bash Scripting: Basic Maths
#Learn Linux TV 4
#26th April, 2023


# to evaluate expression in bash, you have to use the expr command
expr 40 + 30
# will return 70
expr 40 - 30

expr 40 / 30
expr 40 \* 30 # remember that you have to escape out the astericks because it means "everything" in bash

mynum1=32
mynum2=34

expr $mynum1 + $mynum2

mynum3="34"
mynum4="53"
expr $mynum3 + $mynum4
