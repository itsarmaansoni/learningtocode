#!/bin/bash
#Variables in bash Script
#learn linux TV, day 3
#25, April, 2023

#you can also use variables even in the command line, this shows that command line and bash scripts aren't different 
#to declare a variable
name="Armaan"
#as simple as that
# to reference the same variable you always use '$' sign
echo $name
# if you don't use the dollar sign, it will literally echo the string "name"
echo name
# prints 'name' in the terminal
# if you exit out of a terminal, variables are wiped from the memory, when you declare a variable in bash, it is
# tied to that session, once you close that window, the variable is gone, it ceases to exist

myname="Armaan Soni"
myage="19"
echo "Hello, my name is $myname and I am $myage years old!"
# if you use single quotes, it will ignore the variable call, and print literally what's in the string
echo 'Hello, my name is $myname and I am $myage years old!'

# you can also make variables to store the output of commands
files=$(ls)
echo $files 
