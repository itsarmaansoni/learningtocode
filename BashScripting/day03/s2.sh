#!/bin/bash
#Variables in bash Script
#learn linux TV, day 3
#25, April, 2023


# we are gonna make a simple script 
name="Armaan Soni" # wow, something really fucking dumb about bash I learned rn is that it's space sensitive
# meaning that you have to have no space between variable name and the equal to sign, damn... fuck 
now=$(date)
echo "Hello $name"
echo "The current system time and date is $now"
echo "Your username is $USER"
# well you might be wondering, we didn't declare the variable USER, 
# well within your environment, there are many default variables, like $USER and $SHELL and $HOME
# $SHELL contains the current shell
# $USER contains the current user that you are logged in with
# to know what variables are declared in your environment, you can type 'evn' in your terminal

