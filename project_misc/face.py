import threading
import cv2 
import os
from deepface import DeepFace

cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
counter = 0
bhaimatch = False
armaanmatch = False
armflag = 0
lock = threading.Lock()  # Lock for synchronization

reference_img = cv2.imread("/home/life/Documents/learningtocode/project_misc/armaan.jpg")
reference_img2 = cv2.imread("/home/life/Documents/learningtocode/project_misc/bhai.jpg")

if reference_img is None:
    print("Error: armaan image not found or could not be loaded.")
if reference_img2 is None:
    print("Error: bhai image not found or could not be loaded.")

def check_face(frame):
    global bhaimatch, armaanmatch
    try:
        armaan_verified = DeepFace.verify(frame, reference_img.copy())['verified']
        bhai_verified = DeepFace.verify(frame, reference_img2.copy())['verified']
        
        with lock:  # Acquire the lock before updating the flags
            armaanmatch = armaan_verified
            bhaimatch = bhai_verified
    except ValueError: 
        with lock:  # Ensure flags are updated safely
            armaanmatch = False
            bhaimatch = False

while True:
    ret, frame = cap.read()
    if ret:
        if counter % 50 == 0:
            try:
                threading.Thread(target=check_face, args=(frame.copy(),)).start()
            except ValueError:
                pass

        counter += 1

        with lock:  # Acquire the lock before reading the flags
            if armaanmatch:
                if armflag == 0:
                    armflag = 1
                    print('hey wtf')
                    os.system("cd '/home/life/Documents/armbill/' && java -jar dist/armBill.jar")
                cv2.putText(frame, "HELLO ARMAAN!", (20, 450), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 3)
            elif bhaimatch:
                cv2.putText(frame, "HELLO BHAI!", (20, 450), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 3)
            else:
                cv2.putText(frame, "NO HUMAN!", (20, 450), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 255), 3)

        cv2.imshow("video", frame)

    key = cv2.waitKey(1)
    if key == ord("q"):
        break

cap.release()
cv2.destroyAllWindows()
