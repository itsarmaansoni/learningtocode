#include <iostream>
using namespace std;
int main(){
    int nums[] = {8,12};
    int gcd=1;
    int n=1;
    int flag=1;
    int min=INT32_MAX;
    for (auto ele: nums){
        if (min>ele) min = ele;
    }
    while(n<=min){
        int div=1;
        for (int i=0;i<sizeof(nums)/sizeof(nums[0]);i++){
            if (nums[i]%n!=0) {
                div=0;
                break;
            }
        }
        if(div) gcd=n;
        n++;
    }
    cout<<gcd;
}