#include <iostream>
using namespace std;
void printArray(int arr[], int size){
	for (int i=0;i<size;i++) cout<<arr[i]<<" ";
	cout<<endl;
}
void countSort(int arr[], int size,int exp){
	int newarr[size];
	int count[10] = {0};
	for (int i = 0;i<size;i++)
		count[(arr[i]/exp) % 10]++;
	for (int i = 1;i<10;i++){
		count[i] += count[i-1];
	}
	for (int i=size-1;i>=0;i--){
		newarr[count[(arr[i] / exp) % 10] - 1] = arr[i];
		count[(arr[i] / exp) % 10] --;
	}
	for (int i = 0; i< size;i++) 
		arr[i] = newarr[i];
}
void radixSort(int arr[], int size){
	int m = 0;
	for (int i=0;i<size;i++) if (arr[i] > m) m = arr[i];
	for (int exp = 1;m/exp > 0; exp *= 10){
		countSort(arr,size,exp);
	}
}
int main(){
	int arr[] = { 170, 45, 75, 90, 802, 24, 2, 66 };
	int size = sizeof(arr)/sizeof(arr[0]);
	cout<<"old array:";
	printArray(arr,size);
	radixSort(arr,size);		
	cout<<"new array:";
	printArray(arr,size);
	return 0;
}
