#include <stdio.h>
// some random code from a youtube comment
int main()
{
	int r1, c1, r2, c2, t;
	
	printf("Enter the no. of rows of matrix A: ");
	scanf("%d", &r1);
	
	printf("Enter the no. of columns of matrix A: ");
	scanf("%d", &c1);
	
	printf("Enter the no. of rows of matrix B: ");
	scanf("%d", &r2);
	
	printf("Enter the no. of columns of matrix B: ");
	scanf("%d", &c2);

	if(c1 == r2)
	{
		int A[r1][c1], B[r2][c2], C[r1][c2];
		
		for(int i = 0; i < r1; i++)
		{
			for(int j = 0; j < c1; j++)
			{
				printf("Enter the value at (%d, %d) of matrix A: ", i, j);
				scanf("%d", &A[i][j]);
			}
		}
		
		for(int i = 0; i < r2; i++)
		{
			for(int j = 0; j < c2; j++)
			{
				printf("Enter the value at (%d, %d) of matrix B: ", i, j);
				scanf("%d", &B[i][j]);
			}
		}
		
		for(int i = 0; i < r1; i++)
		{
			for(int j = 0; j < c2; j++)
			{
				C[i][j] = 0;
			}
		}
		
		for(int i = 0; i < r1; i++)
		{
			for(int j = 0; j < c2; j++)
			{
				for(int k = 0; k < c1; k++)
				{
					t = A[i][k] * B[k][j];
					C[i][j] += t;
				}
			}
		}
		
		printf("The matrix multiplication of A and B is given below:\n");
		
		printf("   A * B\n= |");
		
		for(int i = 0; i < r1; i++)
		{
			for(int j = 0; j < c2; j++)
			{
				printf(" %d ", C[i][j]);
			}
			printf("|");
			
			if((i + 1) < r1)
			{
				printf("\n  |");
			}
		}
	}
	
	else
	{
		printf("The matrix multiplication of A and B cannot be performed.\n");
	}
	
	return 0;
}
// alright, but it didn't show the first matrices
// I'd give this code a 8/10
// mine is ofcourse 10/10