#include <iostream>
#include <vector>
using namespace std;
#pragma GCC optimize ("O12")
class Solution {
public:
    void cycle (vector<vector<int>>& matrix,int i,int j,int n){
        n=n-1;
        int temp = matrix[i][j];
        matrix[i][j] = matrix[n - j][i];
        matrix[n - j][i] = matrix[n - i][n - j];
        matrix[n - i][n - j] = matrix[j][n - i];
        matrix[j][n - i] = temp;
    }
    void rotate(vector<vector<int>>& matrix) {
        int n=matrix.size();
        for (int i=0;i<n/2;i++){
            for (int j=i;j<n-i-1;j++){
                cycle(matrix,i,j,n);
            }
        }
    }
};
int main(){
    vector<vector<int>> mat = {{1,2,3},{4,5,6},{7,8,9}};
    (new Solution)->rotate(mat);
    for (const auto & row: mat){
        for (const auto & ele: row){
            cout<<ele<<" ";
        }
        cout<<endl;
    }
    return 0;
}