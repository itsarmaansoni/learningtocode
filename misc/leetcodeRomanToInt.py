class Solution:
    def charToVal(self,c: chr) -> int:
        syms=('I','V','X','L','C','D','M')
        vals=(1,5,10,50,100,500,1000)
        ind = syms.index(c)
        return vals[ind]
    def romanToInt(self, s: str) -> int:
        if (s.len()%2!=0):
            s=" "+s
         
