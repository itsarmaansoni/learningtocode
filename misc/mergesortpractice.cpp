#include <iostream>
using namespace std;
class Solution{
    void merge(int arr[],int l,int m,int u){
        int copy1[m-l+1],copy2[u-m];
        for (int i=l;i<=m;i++) copy1[i-l] = arr[i];
        for (int j=m+1;j<=u;j++) copy2[j-(m+1)] = arr[j];
        int i=0,j=0,k=l;
        while(i<=m-l && j<=u-m-1){
            if (copy1[i]<copy2[j]) arr[k++] = copy1[i++];
            else arr[k++] = copy2[j++];
        }
        while(i<=m-l) arr[k++]=copy1[i++];
        while(j<=u-m-1) arr[k++]=copy2[j++];
    }
    public:
    void mergeSort(int arr[],int l,int u){
        // cout<<"hi"<<l<<" "<<u;
        if (u-l<=0){
            return;
        }
        int m = (l+u)/2;
        mergeSort(arr,l,m);
        mergeSort(arr,m+1,u);
        merge(arr,l,m,u);
    }
};
int main(){
    int arr[] = {10,9,8,6,3};
    (new Solution)->mergeSort(arr,0,sizeof(arr)/sizeof(arr[0]) - 1);
    for (int i=0;i<sizeof(arr)/sizeof(arr[0]);i++) cout<<arr[i]<<" ";
    cout<<endl;
}