#include <iostream>
#include <bits/stdc++.h>
#include <chrono>
#include <cstdlib>
using namespace std;

int partition(int arr[],int s,int e){
    int pivot=arr[s];
    int cnt=0;
    for (int i=s+1;i<=e;i++){
        if(arr[i]<=pivot){
            cnt++;
        }
    }
    int p=s+cnt;
    // swap element at s and p
    int temp=arr[p];
    arr[p]=arr[s];
    arr[s]=temp;


    int i=s;
    int j=e;
    while(i<p && j>p){
        while(arr[i]<pivot){
            i++;
        }
        while(arr[j]>pivot){
            j--;
        }
        if(i<p && j>p){
            temp=arr[i];
            arr[i]=arr[j];
            arr[j]=temp;
            i++;
            j--;
        }
    }

    return p;
}
void quickSort(int arr[],int s,int e){
    if(s>=e){
        return;
    }
    //partition
    int p=partition(arr,s,e);
    //quickSort left part
    quickSort(arr,s,p-1);
    //quickSort right part
    quickSort(arr,p+1,e);
}
void insertionSort(int arr[],int arrlen){
    for (int i=1;i<arrlen;i++){
        int temp=arr[i];
        int j=i-1;
        for (;j>=0;j--){
            if(temp>=arr[j]){
                break;
            }
            else{
                arr[j+1]=arr[j];
            }
        }
        arr[j+1]=temp;
    }
}
int main(){
    for (int k = 1;k<=1;k++){
        int lb = -10;
        int ub = 140;
        int choice=0;
        int arrlen=100000;
        int arr[arrlen];
        for (int i = 0; i < arrlen; i++)
            arr[i]=(rand() % (ub - lb + 1));
        // cout<<"before partitioning:\t";
        // for (int i=0;i<arrlen;i++){
        //     cout<<arr[i]<<" ";
        // }
        // cout<<endl<<"after partitioning:\t";
        // partition(arr,0,arrlen-1);
        // for (int i=0;i<arrlen;i++){
        //     cout<<arr[i]<<" ";
        // }

        // cout<<"before sorting:\t";
        // for (int i=0;i<arrlen;i++){
        //     cout<<arr[i]<<" ";
        // }
        // cout<<endl<<"after sorting:\t";
        auto start = chrono::high_resolution_clock::now();
    
        // unsync the I/O of C and C++.
        ios_base::sync_with_stdio(false);
        if(choice)
        insertionSort(arr,arrlen);
        else
        quickSort(arr,0,arrlen-1);
        auto end = chrono::high_resolution_clock::now();
    
        // for (int i=0;i<arrlen;i++){
        //     cout<<arr[i]<<" ";
        // }cout<<endl;
        // Calculating total time taken by the program.
        double time_taken =
        chrono::duration_cast<chrono::nanoseconds>(end - start).count();
    
        time_taken *= 1e-9;
    
        cout << "Time taken by program is : " << fixed
            << time_taken << setprecision(9);
        cout << " sec" << endl;
    }
    return 0;
}