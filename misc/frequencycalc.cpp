#include <iostream>
#include <vector>
using namespace std;
int main() {
    string str = "The sea was a brilliant blue, stretching out endlessly to meet the sky at the distant horizon. Waves gently lapped against the shore, creating a soothing, rhythmic sound that blended perfectly with the warm breeze that carried the faint scent of salt and seaweed. The beach was dotted with small clusters of seashells, their intricate patterns and soft colors glistening under the bright sunlight. A few seagulls soared overhead, their sharp cries echoing through the air as they circled above the water, occasionally diving down to catch a fish. In the distance, a small boat could be seen, lazily drifting along, its white sail billowing in the wind. On the shore, footprints marked the soft sand, evidence of earlier visitors who had long since left, their presence now a distant memory. The sky above was a clear, cloudless blue, with the sun hanging high, casting its warm, golden light over everything below. The atmosphere was peaceful, serene, as if the world had momentarily paused to take a deep breath. The beauty of the scene was almost surreal, as if it had been plucked from the pages of a storybook. Time seemed to slow down, allowing every detail to be appreciated—the way the sunlight danced on the water's surface, the gentle swaying of the palm trees, the quiet murmur of the ocean. It was the kind of day that made you want to forget about everything else and simply exist in the moment, letting the tranquility of the surroundings wash over you like the gentle waves on the shore.";
    vector<int> freq(26, 0);  // Array to store frequency of letters
    // Count letter frequencies
    for (char ch : str) {
        if (isalpha(ch))  // Only consider alphabetic characters
            freq[tolower(ch) - 'a']++;
    }
    // Bubble sort both frequencies and their corresponding indices
    vector<int> index(26);
    for (int i = 0; i < 26; i++) index[i] = i;  // Initialize indices
    for (int i = 0; i < 25; i++) {
        for (int j = 0; j < 25 - i; j++) {
            if (freq[j] > freq[j + 1]) {
                swap(freq[j], freq[j + 1]);    // Swap frequencies
                swap(index[j], index[j + 1]);  // Swap corresponding indices
            }
        }
    }
    // Display letter frequencies
    for (int i = 0; i < 26; i++) {
        cout << (char)(index[i] + 'A') << ": " << freq[i] << endl;
    }
    return 0;
}
