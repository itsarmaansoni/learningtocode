#include <iostream>
#include <random>
#include <windows.h>
#include <conio.h>
#include <vector>
using namespace std;
int ms = 20;

vector<int> xvect;
vector<int> yvect;
vector<int> dirvect;
int fruit_x=3,fruit_y=4;
const int dim_down=40, 
    dim_right=90;
int score = 0;
class direction {
    public:
        // 1 up
        // 0 down
        // 2 left
        // 3 right
        int dir=0;
    direction(){
        dir = 0;
    }
};
direction dirObj;
char gr[dim_down*dim_right+1] = {};
void initializeVectors() {
    xvect.push_back(3);
    yvect.push_back(4);
    dirvect.push_back(0);
}
void dirset(){
    if (_kbhit()){
        char dir  =_getch();
        if (dir == 'w'){
            dirObj.dir=1;
        }
        else if (dir == 's'){
            dirObj.dir=0;
        }
        else if (dir == 'd'){
            dirObj.dir=3;
        }
        else if (dir == 'a'){
            dirObj.dir=2;
        }
        else if (dir == 'k'){
            ms-=1;
        }
        else if (dir == 'j'){
            ms+=1;
        }
    }
}
string InitGrid(){
    for (int i=0;i<dim_down*dim_right+1 ; i++)
    gr[i]=' ';
    gr[dim_down*dim_right]='\0';
    for (int i=0;i<=dim_down;i++){
        for (int j=0;j<=dim_right;j++){
            if (i==0 || i==dim_down || j==0 || j==dim_right){
                gr[i+j]='.';
            }
            else{
                if (i==xvect[xvect.size()-1] && j == yvect[yvect.size()-1]){
                    gr[i+j]='x';
                }
                else if (i==fruit_x && j == fruit_y){
                    gr[i+j]='o';
                }
                else{
                    gr[i+j]=' ';
                }
            }
        }
        gr[i] = '\n';
    }
    return gr;
    
}
void draw(){
    system("CLS");
    // string str;
    // for (int i=0;i<=dim_down;i++){
    //     for (int j=0;j<=dim_right;j++){
    //         if (i==0 || i==dim_down || j==0 || j==dim_right){
    //             str=str+".";
    //         }
    //         else{
    //             if (i==xvect[xvect.size()-1] && j == yvect[yvect.size()-1]){
    //                 str=str+"x";
    //             }
    //             else if (i==fruit_x && j == fruit_y){
    //                 str=str+"O";
    //             }
    //             else{
    //                 str=str+" ";
    //             }
    //         }
    //     }
    //     str=str+"\n";
    // }
    cout<<InitGrid();
    // cout<<str;
    cout<< "\n score : "<<score<< "ms : "<<ms;
}
void engine(){
     switch (dirObj.dir){
        case 0:
            xvect[xvect.size()-1]++;
            break;
        case 1:
            xvect[xvect.size()-1]--;
            break;
        case 2:
            yvect[yvect.size()-1]--;
            break;
        case 3:
            yvect[yvect.size()-1]++;
            break;
     }
     if (xvect[xvect.size()-1] == fruit_x && yvect[yvect.size()-1] == fruit_y){
        score+=10;
        
        fruit_x=rand() % dim_down + 1;
        fruit_y=rand() % dim_right + 1;
     }
    //  if (xvect[xvect.size()-1] == 0 || xvect[xvect.size()-1] == dim_down || yvect[yvect.size()-1] == 0 || yvect[yvect.size()-1] == dim_right){
    //     cout<<"\n               GAME OVER";
    //     exit(0);
    //  }
    if (xvect[xvect.size()-1] == 0){
        xvect[xvect.size()-1]=dim_down-1;
    }
    if (xvect[xvect.size()-1] == dim_down){
        xvect[xvect.size()-1]=1;
    }
    if (yvect[yvect.size()-1] == 0){
        yvect[yvect.size()-1]=dim_right-1;
    }
    if (yvect[yvect.size()-1] == dim_right){
        yvect[yvect.size()-1]=1;
    }
}
int main(){
    initializeVectors();
    xvect.push_back(3);
    yvect.push_back(4);
    dirvect.push_back(0);
    bool gameStop=false;
    while(gameStop!=true){
        
        dirset();
        draw();
        Sleep(ms);
        engine();
        gameStop=true;
    }
    return 0;
}
