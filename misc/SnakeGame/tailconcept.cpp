#include <iostream>
#include <vector>
#include <stdio.h>
#include <windows.h>
#include <conio.h>
#define BOARD_HEIGHT 40
#define BOARD_WIDTH 40
using namespace std;

vector<pair <int,int>> tail;
vector<int> dir;

// down -> 0
// up -> 1
// left -> 2
// right -> 3

char grid[BOARD_HEIGHT][BOARD_WIDTH];
void initString(){
    for (int i=0;i<BOARD_HEIGHT;i++){
        for (int j=0;j<BOARD_WIDTH;j++){
            if (i==0 || i==BOARD_HEIGHT-1 || j==0 || j==BOARD_WIDTH-1){
                grid[i][j]='.';
            }
            else{
                grid[i][j]=' ';
            }
        }
    }
    for (int i=0;i<tail.size();i++){
        grid[tail[i].first][tail[i].second] = '*';
    }
}
void changeHeadDir(char d){
    switch (d){
            case 's':
                dir[0]=0;
                break;
            case 'w':
                dir[0]=1;
                break;
            case 'a':
                dir[0]=2;
                break;
            case 'd':
                dir[0]=3;
                break;
        }
}
void computeString(){
    for (int i=0;i<tail.size();i++){
        int d = dir.at(i);
        switch (d){
            case 0:
                tail[i].first++;
                break;
            case 1:
                tail[i].first--;
                break;
            case 2:
                tail[i].second--;
                break;
            case 3:
                tail[i].second++;
                break;
        }
    }
    for (int i=dir.size()-1;i>=1;i--){
        dir[i]=dir[i-1];
    }
    initString();
}
void tpp(int x,int y,int d){
    tail.push_back(make_pair(x,y));
    dir.push_back(d);
}
void initTail(){
    tpp(4,6,0);
    tpp(4,5,3);
    tpp(4,4,3);
    tpp(4,3,3);
    tpp(4,2,3);
    tpp(4,1,3);
}
void draw(){
    string outbuf="";
    for (int i=0;i<BOARD_HEIGHT;i++){
        for (int j=0;j<BOARD_WIDTH;j++){
            outbuf+=grid[i][j];
        }
        outbuf+="\n";
    }
    cout<<outbuf;
    //debug code
    // cout<<endl;
    // for (int i=0;i<tail.size();i++){
    //     cout<<"x : "<<tail[i].first<<" y : "<<tail[i].second<<" dir : "<<dir[i];
    //     cout<<endl;
    // }
}
int main(){

    int flag = 20;
    initTail();
    initString();
    while(flag){
        system("CLS");
        draw();
        if(kbhit()){
            char ch = getch();
            changeHeadDir(ch);
        }
        computeString();
        Sleep(500);
        // flag--;
    }
    return 0;
}