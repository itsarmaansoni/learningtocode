ourString = ''' this is a multi line string and you are attending the DISHA CLUB's Cryptography :  Escape the room, and we are learning about letter frequency analysis this is a test string, and I am trying to make it sound as normal as possible, so we get the expected letter frequency array lol, just gonna insert more random text here for better analysis, cause you know more the data, clearer the patterns, amirite?
'''

freq_map = {}
for char in ourString.upper():
    if char.isalpha():  # Check if char is a letter
        freq_map[char] = freq_map.get(char, 0) + 1

sorted_freq_map = dict(sorted(freq_map.items(), key=lambda item: item[1]))

for x, y in sorted_freq_map.items():
    print(f"{x}: {y}")
