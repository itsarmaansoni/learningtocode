#include <stdio.h>
#include <stdlib.h>
struct Block{
	int startIndex;
	int maxValue; 	
};
int sequentialSearch(int * arr, int start, int end, int key){
	for (int i=start;i<end;i++){
		if (arr[i] == key) return i;
	}
	return -1;
}
int BinaryBlockSearch(struct Block * IndexTable,int indexTableSize,int key){
	int low = 0, high = indexTableSize-1;
	while(low<=high){
		int mid = (low+high)/2;
		if (key <= IndexTable[mid].maxValue){
			if (mid == 0 || key > IndexTable[mid - 1].maxValue) return mid;	
			high = mid -1;
		}
		else{
			low = mid + 1;
		}
	}	
	return -1;
}
void printArray(int * arr, int arr_size,int indexFlag){
	if (!indexFlag){
		for (int i=0;i<arr_size;i++){
			printf("%d ",arr[i]);
		}
		printf("\n");
		return;
	}
	for (int i=0;i<arr_size;i++){
		printf("%d %d\n",i,arr[i]);
	}
}
void sort(int * arr, int arr_size){
	for (int i=0;i<arr_size-1;i++){
		for (int j=0;j<arr_size-i-1;j++){
			if (arr[j]>arr[j+1]){
				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}	
	}
}
int indexedSequentialSearch(int blockSize, struct Block * IndexTable, int indexTableSize, int * arr, int arr_size, int target){
	int blockId = BinaryBlockSearch(IndexTable,indexTableSize,target);
	if (blockId == -1) return -1;
	int eleId = sequentialSearch(arr,IndexTable[blockId].startIndex,IndexTable[blockId].startIndex+blockSize-1,target);
	if (eleId == -1) return -1;
	return eleId;
	
} 
int main(){
	int arr[] = {5, 10, 10, 15, 20, 20, 25, 30, 30, 35, 40};
	int size = sizeof(arr) / sizeof(arr[0]);
	int blockSize = 3;
	int key = 20;
	sort(arr,size);
	printArray(arr,size,1);
	int indexTableSize = (size/blockSize) + ((size%blockSize)==0?0:1);
	printf("array size: %d\n",size);
	printf("Block size: %d\n",blockSize);
	printf("index Table size: %d\n",indexTableSize);
	struct Block * IndexTable = (struct Block *)malloc(sizeof(struct Block)*indexTableSize);
	for (int i=0,j=0;i<size;i+=blockSize,j++){
		IndexTable[j].startIndex=i;
		IndexTable[j].maxValue=arr[i+blockSize-1];
	}
	IndexTable[indexTableSize-1].maxValue = arr[size-1];
	for (int j=0;j<indexTableSize;j++){
		printf("%d\t%d\t%d\n",j,IndexTable[j].startIndex,IndexTable[j].maxValue);
	}			
	int id = indexedSequentialSearch(blockSize,IndexTable,indexTableSize,arr,size,key);
	printf("element %d found at %d",key,id);	
	return 0;
}
