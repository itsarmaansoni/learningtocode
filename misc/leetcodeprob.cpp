#include <iostream>
#include <string>
using namespace std;
class Solution {
public:
    int charToVal(char c){
        char syms[]={'I','V','X','L','C','D','M'};
        int vals[]={1,5,10,50,100,500,1000};
        int i;
        for (i=0;i<7;i++){
            if (syms[i]==c)
            break;
        }
        return vals[i];
    }
    int romanToInt(string s) {
        int sum =0;
        for (int i=0;i<s.length();i++){
            if(i==s.length()-1){
                sum+=charToVal(s[i]);
                continue;
            }
            if (charToVal(s[i])>=charToVal(s[i+1])){
                sum+=charToVal(s[i]);
            }
            else {
                sum-=charToVal(s[i]);
            }
        }
        return sum;
    }
};
int main(){
    Solution obj;
    cout<<obj.romanToInt("MCMXCIV")<<endl;

}