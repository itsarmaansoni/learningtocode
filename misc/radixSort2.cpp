#include <iostream>
#include <vector>
using namespace std;
void countSort(vector<int>& arr,int exp){
	int freq[10] = {0};
	vector<int> newarr(arr.size());
	for (auto ele : arr) freq[(ele/exp)%10] ++;
	for (int i=1;i<10;i++) freq[i] += freq[i-1];
	for (int i=arr.size()-1;i>=0;i--){
		newarr[freq[(arr[i]/exp)%10] - 1] = arr[i];
		freq[(arr[i]/exp)%10] --;
	}
	for (int i=0;i<arr.size();i++) arr[i] = newarr[i];			
}
void radixSort(vector<int>& arr){
	int m = 0;
	for (auto ele : arr) if (ele>m) m = ele;
	for (int exp = 1; m/exp>0;exp*=10){
		countSort(arr,exp);
	}
}
int main(){
	vector<int> arr = { 170, 45, 75, 90, 802, 24, 2, 66 };	
	cout<<"old array:";
	for (auto ele : arr) cout<<ele<<" ";
	cout<<endl;
	radixSort(arr);
	cout<<"new array:";
	for (auto ele : arr) cout<<ele<<" ";
	cout<<endl;
	return 0;
}
