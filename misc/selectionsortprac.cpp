#include <iostream>
using namespace std;
int main(){
    int arr[] = {1,6,6,34,0,-234,38,45,23,17,14,13};
    int n=sizeof(arr)/sizeof(arr[0]);
    int counter=0;
    for (int i=0;i<n-1;i++){                    
        int minind = i;
        for (int j=i+1;j<n;j++){
            if (arr[minind] > arr[j]) minind=j;
            counter++;
        }
        int temp = arr[i];
        arr[i] = arr[minind];
        arr[minind] = temp;
    }
    cout<<"counter:"<<counter<<endl;
    cout<<"sorted array:";
    for (int i=0;i<n;i++) cout<<arr[i]<<" ";

    return 0;
}