class Solution {
    public int typeOpen(char ch){
        if (ch=='{'||ch=='['||ch=='('){
            if (ch=='{'){
                return 1;
            }
            else if (ch=='['){
                return 2;
            }
            else {
                return 3;
            }
        }
        else {
            return 0;
        }
    }
    public boolean isValid(String s) {
        boolean flag = false;
        StringBuilder sb = new StringBuilder(s);
        while(sb.length()!=0){
            char ch=sb.charAt(0);
            if (typeOpen(ch)==0){
                break;
            }
            else{
                if (typeOpen(ch)==1){
                    sb.deleteCharAt(sb.indexOf("}"));
                }
                else if (typeOpen(ch)==2){
                    sb.deleteCharAt(sb.indexOf("]"));
                }
                else {
                    sb.deleteCharAt(sb.indexOf(")"));
                }
            }
        }
        if (sb.length()==0){
            flag=true;
        }

        return flag;
    }
    public static void main(String [] args){
        Solution obj = new Solution();
        System.out.println(obj.isValid("(){}[[[[[]]]]]"));
        
    }
}