class Person:
    def __init__(self,name:str,age:int,sex:bool,pronoun:tuple()):
        '''Pass in name, age and sex, if sex is male pass true, false if female'''
        self.name = name
        self.age = age
        self.sex = sex 
        self.pronoun = pronoun
        self.state = "doing nothing"
    def showStatus(self):
        print(f"{self.name} is {self.age} year/s old, {self.pronoun[0]} is/are {self.state}")
    def setState(self,state:str):
        self.state=state
    def kill(self):
        self.state="dead"

person1 = Person("Armaan",19,True,("he","him"))
person2 = Person("Madhulika",19,False,("she","her"))
person1.showStatus()
person2.showStatus()
person2.setState("walking")
person2.showStatus()
person1.showStatus()
person1.kill()
person1.showStatus()