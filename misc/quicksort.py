
from timeit import default_timer as timer
def partition(arr:list,s:int,e:int)->int:
    pivot=arr[s]
    count=0
    for i in range(s+1,e+1):
        if(arr[i]<=pivot):
            count+=1
    pivotInd=s+count
    temp=arr[s]
    arr[s]=arr[pivotInd]
    arr[pivotInd]=temp
    i=s
    j=e
    while(i<pivotInd and j>pivotInd):
        while(arr[i]<pivot):
            i+=1
        while(arr[j]>pivot):
            j-=1
        if(i<pivotInd and j>pivotInd):
            temp=arr[i]
            arr[i]=arr[j]
            arr[j]=temp
            i+=1
            j-=1
    return pivotInd

def quickSort(arr:list,s:int,e:int):
    # base case 
    if(s>=e):
        return
    # partitioning
    pivotInd=partition(arr,s,e)
    #left part 
    quickSort(arr,s,pivotInd-1)
    #right part
    quickSort(arr,pivotInd+1,e)

import random
arr = []
len=100000
for i in range (len):
    arr.append(random.randint(-10,140))
# print(arr)
start = timer()
quickSort(arr,0,arr.__len__()-1)
end = timer()
# print(arr)
print(end-start)
