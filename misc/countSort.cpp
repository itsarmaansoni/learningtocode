#include <iostream>
using namespace std;
int main(){
	int arr[] = {6,3,3,3,6,6,1,1,1,1,0,0,0,4,3,5,3,7,2,3,0};
	cout<<"old array:";
	for (auto ele : arr) cout<<ele<<" ";
	cout<<endl;
	int size = sizeof(arr)/sizeof(arr[0]);
	int max = 0;		
	for (auto ele : arr) if (ele>max) max = ele;
	int count[max+1] = {0};
	for (int i=0;i<size;i++){
		count[arr[i]] ++;
	}
	for (int i=1;i<max+1;i++){
		count[i] = count[i-1] + count[i];
	}
	int newarr[size];
	for (int i=size-1;i>=0;i--){
		newarr[count[arr[i]] - 1] = arr[i];
		count[arr[i]] --;
	}
	cout<<"new array:";
	for (int ele : newarr) cout<<ele<<" ";
	cout<<endl;
	return 0;
}
