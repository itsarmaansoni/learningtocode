#include <iostream>
using namespace std;
int main(){
    int n=4;
    int a[n],b[n],c[n+1];
    cout<<"Enter a:";
    for (int i=n-1;i>=0;i--) cin>>a[i];
    cout<<"Enter b:";
    for (int i=n-1;i>=0;i--) cin>>b[i];
    for (int i=0;i<=n;i++) c[i]=0;
    cout<<"a:";
    for (int i=n-1;i>=0;i--) cout<<a[i];
    cout<<endl<<"b:";
    for (int i=n-1;i>=0;i--) cout<<b[i];
    cout<<endl;
    for (int i=0;i<n;i++){
        int sum=a[i]+b[i]+c[i];
        c[i]=sum%2;
        c[i+1]=sum/2;
    }
    cout<<"added binary number:";
    for (int i=n;i>=0;i--) cout<<c[i];
    cout<<endl;
    return 0;
}