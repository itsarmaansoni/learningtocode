#define GRID_HEIGHT 30
#define GRID_WIDTH 30
#include <iostream>
#include <vector>
#include <utility>
#include <unistd.h>

using namespace std;
vector<pair<int,int>> tail_element_position;
vector<int> tail_element_direction;
vector<vector<char>> grid;
void initialize_grid(){	
	for (int i=0;i<GRID_HEIGHT;i++){
		vector<char> temp;
		for (int j=0;j<GRID_WIDTH;j++){
			if(i==0 || i==GRID_HEIGHT-1 || j==0 || j==GRID_WIDTH-1)	
				temp.push_back('.');
			else
				temp.push_back(' ');
		}
		grid.push_back(temp);
	}
}
void tail_ele_push(int x, int y, int d){
	tail_element_position.push_back(make_pair(x,y));
	tail_element_direction.push_back(d);
}
void initialize_tail_elements_and_directions(){
	tail_ele_push(8,8,0);
	tail_ele_push(7,8,0);
	tail_ele_push(6,8,0);
	tail_ele_push(5,8,0);
}
void put_tail_on_grid(){
	if (tail_element_position[0].first>=GRID_HEIGHT-1 || tail_element_position[0].first<=0 || 
		tail_element_position[0].second>=GRID_WIDTH-1 || tail_element_position[0].first<=0){
			system("clear");
			cout<<"\nGAME OVER\n";
			exit(0);
		}
	for (int i=0;i<tail_element_position.size();i++){
		grid[tail_element_position[i].first][tail_element_position[i].second]='x';
	}
}
void print_grid(){
	for (int i=0;i<GRID_HEIGHT;i++){
		for (int j=0;j<GRID_WIDTH;j++){
			cout<<grid[i][j];
		}
		cout<<endl;
	}
}
void clear_old_tail_from_grid(){
	for (int i=0;i<tail_element_position.size();i++){
		grid[tail_element_position[i].first][tail_element_position[i].second]=' ';
	}
}
void update_tail(){
	for (int i=0;i<tail_element_position.size();i++){
        int d = tail_element_direction[i];
        switch (d){
            case 0:
                tail_element_position[i].first++;
                break;
            case 1:
                tail_element_position[i].first--;
                break;
            case 2:
                tail_element_position[i].second--;
                break;
            case 3:
                tail_element_position[i].second++;
                break;
        }
    }
    for (int i=tail_element_direction.size()-1;i>=1;i--){
        tail_element_direction[i]=tail_element_direction[i-1];
    }
	
}
int main(){
	initialize_grid();
	initialize_tail_elements_and_directions();
	put_tail_on_grid();
	int flag=100;
	while(flag){
		system("clear");
		clear_old_tail_from_grid();
		update_tail();
		put_tail_on_grid();
		print_grid();
		usleep(200000);
		flag--;
	}
}
