#include <iostream>
#include <vector>
using namespace std;
class Solution{
	void swap(int& a,int& b){
		int temp = a;
		a = b;
		b = temp;
	}
	int partition(vector<int>& arr, int left, int right){
		int pivot = arr[right];
		int i=left-1;
		for (int j=left;j<=right;j++){
			if(pivot>=arr[j]){
				i++;
				if (j>i) swap(arr[i],arr[j]);
			}
		}
		return i;
	}
	public:
	void quickSort(vector<int>& arr, int left, int right){
		if(left>=right) return ;
		int pivot = partition(arr,left,right);
		quickSort(arr,left,pivot-1);
		quickSort(arr,pivot+1,right);
	}
};
int main(){
	vector<int> arr({6,3,3,0,17,16,42,3,4,5});
	Solution obj;
	obj.quickSort(arr,0,arr.size()-1);
	for (auto ele : arr) cout<<ele<<" ";
	cout<<endl;
	return 0;
}
