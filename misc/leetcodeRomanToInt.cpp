#include <iostream>
#include <string>
using namespace std;
class Solution {
public:
    int charToVal(char x){
        char arr[]={'I','V','X','L','C','D','M'};
        int arrval[]={1,5,10,50,100,500,1000};
        int j;
        for(j=0;j<7;j++){
            if(arr[j]==x){
                break;
            }
        }
        int val=arrval[j];
        return val;
    }
    int romanToInt(string s) {
        int sum =0;
        if (s.length()==1){
            return charToVal(s[0]);
        }
        for (int i=s.length()-1;i>=0;i--){
            if(charToVal(s[i])<=charToVal(s[i-1])){
                sum+=charToVal(s[i]);
            }
            else{
                sum+=charToVal(s[i])-charToVal(s[i-1]);
                i--;
            }
        }
        return sum;
    }
};
int main(){
    Solution obj;
    cout<<obj.romanToInt("MM")<<endl;
    return 0;
}