from manim import *

class SquareToCircle(Scene):
	def construct(self):
		text = Text("change", font_size=200)
		text1 = Text("into", font_size=200)
		circle = Circle()
		square = Square()
		square.flip(RIGHT)
		square.rotate(-3 * TAU / 8)
		circle.set_color(GREEN)
		circle.set_fill(GREEN, opacity=0.8)
		self.add(text)	
		self.wait()
		self.play(Transform(text,text1))

