All the notes will be in a specific order,
the notes are derived from Sumita Arora ISC class 11th Computer Science with
Java and Sumita Arora ISC class 12th Computer Science with Java  

Every chapter will have a subfolder under the learningtocode/Java directory 

So for example, chapter "Indroducing Java" will have the path
"../learningtocode/Java/Introducing_Java/"

In this folder we will name our folders such that our programs our in sequence,
in cronological order, p1.java p2.java .... pN.java and so on...

so paths to first program in "Introducing Java" will look something like:
"../learningtocode/Java/Introducing_Java/p1.java"

Comments in Java programs will be used to explain what's happening in the
code.
