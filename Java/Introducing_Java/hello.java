/* program p1.java */
class hello
{
    public static void main(String [] args){
        System.out.println("Hello World!!");
    }
    // main ends here
}
// HelloWorld is the initial class in the program
// it contains the main function inside of it,
// main is a special function in the initial class
// there can be many classes in a java program, but there will only be one initial class

// main() is a method of class HelloWorld, it is the where the execution of the program begins

// System.out.println("Hello World") // prints the string "Hello World" to the standard output device, which is your monitor

// Comments /*multiline comments */

// // is single line comments, they are ignored by compiler 
