#include <bits/stdc++.h>
using namespace std;
class Solution {
public:
    int dfs(vector<vector<int>> g, int i, int j){
        if (i<0 || i>=g.size() || j<0 || j>=g.size() || g[i][j]!=0)
            return 0;
        g[i][j] = 1;
        return 1 + dfs(g,i+1,j) + dfs(g,i-1,j) + dfs(g,i,j-1) + dfs(g,i,j+1);
    }
    int regionsBySlashes(vector<string>& grid) {
        int n= grid.size();
        int regions=0;
        vector<vector<int>> g(n*3,vector<int>(n*3,0));
        for (int i=0;i<n;i++){
            for (int j=0;j<n;j++){
                cout<<grid[i][j]<<endl;
                if (grid[i][j]=='\\'){
                    // cout<<"heyHI";
                    g[i*3][j*3] = g[i*3+1][j*3+1] = g[i*3+2][j*3+2] = 1;
                }
                if(grid[i][j] == '/'){
                    // cout<<"hey"<<i<<j;
                    g[i*3][j*3+2] = g[i*3+1][j*3+1] = g[i*3+2][j*3] = 1;
                }
            }
        }
        for (int i=0;i<n*3;i++){
            for (int j=0;j<n*3;j++){
                cout<<g[i][j]<<" ";
            }
            cout<<endl;
        }

        for (int i=0;i<n*3;i++){
            for (int j=0;j<n*3;j++){
                regions+= dfs(g,i,j)?1:0;
            }
        }
        return regions;
    }
};
int main(){
    vector<string> grid = {"/\\","\\/"};
    cout<<(new Solution)->regionsBySlashes(grid);
    return 0;
}