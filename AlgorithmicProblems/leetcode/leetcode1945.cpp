#include <iostream>
using namespace std;
class Solution {
    int sumOfDigits(int k){
        int sum = 0;
        while(k>0){
            sum+=(k%10);
            k=k/10;
        }
        return sum;
    }
public:
    int getLucky(string s, int k) {
        int number=0;
        for (int i=0;i<s.length();i++){
            number=number+sumOfDigits(s[i]-'a'+1);
        }
        k=k-1;
        for (int i=1;i<=k;i++){
            number=sumOfDigits(number);
        }
        return number;
    }
};
int main(){
    string s="zbax";
    int k=2;
    cout<<(new Solution)->getLucky(s,k);
}
