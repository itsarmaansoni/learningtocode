#include <iostream>
#include <vector>
using namespace std;
class Solution {
public:
    vector<int> rearrangeArray(vector<int>& nums) {
        vector<int> answer;
        int neg=0,pos=0;
        while(nums[neg]>=0) neg++;
        while(nums[pos]<0)pos++;
        // cout<<"hi";
        // cout<<pos<<" "<<neg<<endl;
        for (int i=0;i<nums.size();i++){
            if(i%2==0){
                answer.push_back(nums[pos]);
                pos++;
                while(nums[pos]<0) pos++;
            }
            else{
                answer.push_back(nums[neg]);
                neg++;
                while(nums[neg]>=0) neg++;
            }
        }
        return answer;
    }
};
int main(){
    vector<int> inp={-1,1};
    vector<int> ans=(new Solution)->rearrangeArray(inp);
    for (const auto & ele: ans){
        cout<<ele<<" ";
    }
    return 0;
}
