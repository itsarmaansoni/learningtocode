#include <vector>
#include <iostream>
#include <map>
#include <climits>
using namespace std;
class Solution {
public:
    long long maximumSubarraySum(vector<int>& nums, int k) {
        int n = nums.size();
        vector<long long int> prefixsum(n);
        prefixsum[0] = nums[0];
        //cout<<"original array:";
        for (const auto k : nums){
            //cout<<k<<" ";
        }
        //cout<<endl;
        //cout<<"Prefix sum array:";
        //cout<<prefixsum[0]<<" ";
        for (int i=1;i<n;i++){
            prefixsum[i] = nums[i] + prefixsum[i-1];
            //cout<<prefixsum[i]<<" ";
        }
        //cout<<endl;
        map<long long int, int> lastSeen;
        bool foundGoodSubArray = false;
        long long int maxSum = LONG_MIN;
        for (int i = 0;i < n; i++){
            if (lastSeen.find(nums[i]) != lastSeen.end()){
                //cout<<"yes";
                if (prefixsum[i] >= prefixsum[lastSeen[nums[i]]]){
                    // don't do anything
                }
                else{
                    lastSeen[nums[i]] = i;
                }
            }
            else{
                lastSeen[nums[i]] = i;
            }
            //cout<<"for i:"<<i<<"\n";
            if (lastSeen.find(nums[i]-k) != lastSeen.end()){
                int whereis = lastSeen[nums[i]-k];
                //cout<<"\t"<<"whereis:"<<whereis<<endl;
                long long sum;
                if (whereis!=0)
                sum = prefixsum[i] - prefixsum[whereis-1];
                else
                sum = prefixsum[i];
                //cout<<"\t"<<"sum:"<<sum<<endl;
                if (sum>maxSum) maxSum = sum;
                foundGoodSubArray = true;
            }
            if (lastSeen.find(nums[i]+k) != lastSeen.end()){
                int whereis = lastSeen[nums[i]+k];
                //cout<<"\t"<<"whereis:"<<whereis<<endl;
                long long sum;
                if (whereis!=0)
                sum = prefixsum[i] - prefixsum[whereis-1];
                else
                sum = prefixsum[i];
                //cout<<"\t"<<"sum:"<<sum<<endl;
                if (sum>maxSum) maxSum = sum;
                foundGoodSubArray = true;
            }
        }
        
        return foundGoodSubArray?maxSum:0;
    }
};
int main(){
    Solution obj;
    vector<int> vect = {3,3,2};
    int k = 1;
    long long answer = obj.maximumSubarraySum(vect,k);
    cout<<answer<<" ";
}


