#include <bits/stdc++.h>
using namespace std;
class Solution {
public:
    int numTeams(vector<int>& rating) {
        int asc,dsc;
        asc=0;
        dsc=0;
        for (int i=0;i<rating.size();i++){
            int rgc,rlc,lgc,llc;
            rgc=rlc=lgc=llc=0;
            for (int j=i-1;j>=0;j--){
                if (rating[i]>rating[j]){
                    cout<< " i: " <<rating[i]<< "   " << " j: " <<rating[j]<< " llc " <<endl;
                    llc++;
                }
                if (rating[i]<rating[j]){
                    cout<< " i: " <<rating[i]<< "   " << " j: " <<rating[j]<< " lgc " <<endl;
                    lgc++;
                }
            }
            for (int k=i+1;k<rating.size();k++){
                if (rating[i]<rating[k]){
                    cout<< " i: " <<rating[i]<< "   " << " k: " <<rating[k]<< " rgc " <<endl;
                    rgc++;
                }
                if (rating[i]>rating[k]){
                    cout<< " i: " <<rating[i]<< "   " << " k: " <<rating[k]<< " rlc " <<endl;
                    rlc++;
                }
            }
            asc += llc*rgc ;
            dsc += rlc*lgc;
        }
        return asc+dsc;
    }
};
int main(){
    vector<int> nums = {2,5,3,4,1}; 
    cout<<(new Solution)->numTeams(nums);
}