#include <iostream>
#include <vector>
#include <algorithm>


using namespace std;

class Solution {
    void helper(vector<int>& nums,vector<vector<int>>& answer,vector<int>& curPerm,vector<int>& mask){
        for (int i=0;i<nums.size();i++){
            if(mask[i]==0){
                mask[i]=1;
                curPerm.push_back(nums[i]);
                helper(nums,answer,curPerm,mask);
            }
            curPerm.pop_back();
            mask[i]=0;
        }
        answer.push_back(curPerm);
        return;
    }
public:
    vector<vector<int>> permuteUnique(vector<int>& nums) {
        vector<vector<int>> answer;
        vector<int> curPerm;
        vector<int> mask(0,nums.size());
        helper(nums,answer,curPerm,mask);
        return answer;
    }
};
int main() {
    Solution sol;
    vector<int> nums = {1, 2, 3};  // Change this to test different cases

    vector<vector<int>> result = sol.permuteUnique(nums);

    // Print the results
    cout << "Permutations: " << endl;
    for (const auto& perm : result) {
        for (int num : perm) {
            cout << num << " ";
        }
        cout << endl;
    }

    return 0;
}
