#include <iostream>
#include <vector>
using namespace std;
class Solution{
    private:
        void helper(int& xorSum,vector<int>& vect
        , int curInd, int n,int currentSum){
            if (curInd==n){
                xorSum+=currentSum;
                return;
            }
            //with ind
            currentSum^=vect[curInd];
            helper(xorSum,vect,curInd+1,n,currentSum);
            //without ind
            currentSum^=vect[curInd];
            helper(xorSum,vect,curInd+1,n,currentSum);
        }
    public:
        int xorSum(vector<int> vect){
            int xorSum=0;
            int curSum=0;
            helper(xorSum,vect,0,vect.size(),curSum);
            return xorSum;
        }
};
int main(){
    // driver code
    vector<int> vect = {1,3};
    int xorSUM = (new Solution)->xorSum(vect);
    cout<<xorSUM;
    return 0;
}