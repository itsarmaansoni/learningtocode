#include <iostream>
using namespace std;

// Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(nullptr) {}
};
class Solution {
public:
    
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        ListNode* one = head;
        ListNode* two = head;
        while(n--){
            two=two->next;
        }
        // cout<<two->val;
        while(two->next){
            two=two->next;
            one=one->next;
        }
        one->next = one->next->next;
        return head;
    }
};
// Function to print linked list
void printList(ListNode* head) {
    while (head) {
        cout << head->val << " -> ";
        head = head->next;
    }
    cout << "nullptr" << endl;
}

// Helper function to create a linked list from an array
ListNode* createList(int arr[], int n) {
    if (n == 0) return nullptr;
    ListNode* head = new ListNode(arr[0]);
    ListNode* current = head;
    for (int i = 1; i < n; i++) {
        current->next = new ListNode(arr[i]);
        current = current->next;
    }
    return head;
}

int main() {
    Solution sol;

    // Test Case 1: Remove 2nd from end
    int arr1[] = {1, 2};
    ListNode* head1 = createList(arr1, 2);
    cout << "Original List: ";
    printList(head1);
    head1 = sol.removeNthFromEnd(head1, 1);
    cout << "Updated List: ";
    printList(head1);

    // // Test Case 2: Remove 1st from end (single element list)
    // int arr2[] = {1};
    // ListNode* head2 = createList(arr2, 1);
    // cout << "Original List: ";
    // printList(head2);
    // head2 = sol.removeNthFromEnd(head2, 1);
    // cout << "Updated List: ";
    // printList(head2);

    // // Test Case 3: Remove 1st from end (multiple element list)
    // int arr3[] = {1, 2};
    // ListNode* head3 = createList(arr3, 2);
    // cout << "Original List: ";
    // printList(head3);
    // head3 = sol.removeNthFromEnd(head3, 1);
    // cout << "Updated List: ";
    // printList(head3);

    return 0;
}
