#include <iostream>
#include <vector>
using namespace std;
class Solution {
public:
    long long findScore(vector<int>& nums) {
        if (nums.size() == 1) return nums[0];
        int markCount = 0;
        long long score = 0;
        vector<bool> markedArr(nums.size(),false);
        while(markCount < nums.size()){
            int MIN, minInd;
            for (int i=nums.size()-1;i>=0;i--){
                if (!markedArr[i]){
                    MIN = nums[i];
                    minInd = i;
                    break;
                }
            }
            for (int i=minInd;i>=0;i--){
                if (MIN>=nums[i] && !markedArr[i]){
                    minInd = i;
                    MIN = nums[i];
                }
            }
            score+=MIN;
            if (minInd == 0){
                markCount+=1;
                markedArr[0] = true;
                if (!markedArr[1]) markCount+=1;
                markedArr[1] = true;
            }
            else if (minInd == nums.size()-1){
                markCount+=1;
                markedArr[nums.size()-1] = true;
                if (!markedArr[nums.size()-2]) markCount+=1;
                markedArr[nums.size()-2] = true;
            }
            else{
                markCount+=1;
                markedArr[minInd] = true;
                if (!markedArr[minInd-1]) markCount+=1;
                markedArr[minInd-1] = true;
                if (!markedArr[minInd+1]) markCount+=1;
                markedArr[minInd+1] = true;
            }
        }
        return score;
    }
};

int main(){
    vector<int> nums({2,1,3,4,5,2});
    cout<<(new Solution)->findScore(nums);
    return 0;
}