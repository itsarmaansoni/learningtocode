
#include <stdio.h>
#include <stdlib.h>

int** largestLocal(int** grid, int gridSize, int* gridColSize, int* returnSize, int** returnColumnSizes);

int main() {
    int gridSize = 4;
    int gridColSize[] = {4, 4, 4, 4};

    int** grid = (int**)malloc(gridSize * sizeof(int*));
    for (int i = 0; i < gridSize; i++) {
        grid[i] = (int*)malloc(gridColSize[i] * sizeof(int));
    }

    // Example grid initialization
    int sampleGrid[4][4] = {
        {9, 9, 8, 1},
        {5, 6, 2, 6},
        {8, 2, 6, 4},
        {6, 2, 2, 2}
    };

    for (int i = 0; i < gridSize; i++) {
        for (int j = 0; j < gridColSize[i]; j++) {
            grid[i][j] = sampleGrid[i][j];
        }
    }

    int returnSize;
    int* returnColumnSizes;
    int** result = largestLocal(grid, gridSize, gridColSize, &returnSize, &returnColumnSizes);

    // Print the result
    printf("Result:\n");
    for (int i = 0; i < returnSize; i++) {
        for (int j = 0; j < (gridColSize[0] - 2); j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // Free allocated memory
    for (int i = 0; i < gridSize; i++) {
        free(grid[i]);
    }
    free(grid);

    for (int i = 0; i < returnSize; i++) {
        free(result[i]);
    }
    free(result);
    free(returnColumnSizes);

    return 0;
}

int** largestLocal(int** grid, int gridSize, int* gridColSize, int* returnSize, int** returnColumnSizes) {
    *returnSize=gridSize-2;
    *returnColumnSizes=(int*)malloc((*returnSize)*sizeof(int));
    for (int i=0;i<*returnSize;i++) (*returnColumnSizes)[i]=gridColSize[i]-2;
    int ** ngrid = (int**)malloc((*returnSize)*sizeof(int*));
    for (int i=0;i<(*returnSize);i++) ngrid[i]=(int*)malloc(sizeof(int)*(*gridColSize-2));
    for (int i=0;i<*returnSize;i++){
        for (int j=0;j<(*returnColumnSizes)[i];j++){
            int max=0;
            for (int k=i;k<=i+2;k++) for (int l=j;l<=j+2;l++) if (grid[k][l]>max) max=grid[k][l];
            ngrid[i][j]=max;
        }
    }
    return ngrid;
}
