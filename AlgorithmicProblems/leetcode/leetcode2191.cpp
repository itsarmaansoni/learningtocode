#include <iostream>
#include <vector>
using namespace std;
class Solution
{
    int reverse(int n){
        if (n>0){
            int r = 0;
            while(n){
                r = n%10 + r * 10;
                n = n/10;
            }
            return r;
        }
        else{
            return 0;
        }
    }
public:
    vector<int> sortJumbled(vector<int> &mapping, vector<int> &nums)
    {
        int n = nums.size();
        for (int i = 0; i < n - 1; i++)
        {
            getc(stdin);
            //cout << "current nums state(i="<<i<<"):";
            for (const auto &ele : nums)
            {
                //cout << ele << " ";
            }
            //cout << endl;
            for (int j = 0; j < n - i - 1; j++)
            {
                int jx = 0;
                int j1x = 0;
                int jy = nums[j];
                int j1y = nums[j + 1];
                do
                {
                    jx = jx * 10 + mapping[jy % 10] ;
                    jy = jy / 10;
                } while(jy);
                jx = reverse(jx);
                do
                {
                    j1x = j1x * 10 + mapping[j1y % 10] ;
                    j1y = j1y / 10;
                } while (j1y);
                j1x = reverse(j1x);
                ////cout<<jx<<" "<<j1x<<endl;
                if (jx != j1x)
                {

                    if (jx > j1x)
                    {
                        cout<<nums[j]<<":"<<jx<<" "<<nums[j+1]<<":"<<j1x<<endl;
                        int temp = nums[j];
                        nums[j] = nums[j + 1];
                        nums[j + 1] = temp;
                        //cout << "\tswapping " << nums[j + 1] << " with " << nums[j] <<" because "<<nums[j+1]<<":"<<j1x<<" and "<<nums[j]<<":"<<jx<< endl;
                        //cout << "\t\tcurrent nums state:";
                        for (const auto &ele : nums)
                        {
                            //cout << ele << " ";
                        }
                        //cout << endl;
                    }
                }

                else
                {
                    // ////cout<<"hey";
                    int arr1[10] = {0};
                    int arr2[10] = {0};
                    jy = nums[j];
                    j1y = nums[j + 1];
                    int set = 0;
                    while (jy)
                    {
                        arr1[jy % 10] = 1;
                        jy /= 10;
                    }
                    while (j1y)
                    {
                        arr2[j1y % 10] = 1;
                        j1y /= 10;
                    }
                    int samemapping = true;
                    for (int i = 0; i < 10; i++)
                    {
                        if (arr1[i] != arr2[i])
                        {
                            samemapping = false;
                            break;
                        }
                    }
                    // ////cout<<samemapping;
                    if (samemapping)
                    {
                        // do nothing
                    }
                    else
                    {
                        cout<<"hi";
                        cout<<nums[j]<<":"<<jx<<" "<<nums[j+1]<<":"<<j1x<<endl;
                        int temp = nums[j];
                        nums[j] = nums[j + 1];
                        nums[j + 1] = temp;
                    }
                }
            }
        }
        return nums;
    }
};
int main()
{
    vector<int> nums = {47799,19021,162535,454,95,51890378,404};
    vector<int> mapping = {7,9,4,1,0,3,8,6,2,5};
    // vector<int> mapping = {8,9,4,0,2,1,3,5,7,6};
    // vector<int> nums = {991,338,38};
    vector<int> answer = (*(new Solution)).sortJumbled(mapping, nums);
    for (const auto &ele : answer)
    {
        cout << ele << " ";
    }
    return 0;
}